package engine.renderer;


public abstract class GenericBuffer<GL>{

	//generic implementation of constructor for subclasses 
	public static interface BufferConstructor<GL, Buffer extends GenericBuffer<GL>>{
		public Buffer construct(GL context, float[] fArray, int floatsPerAttribute);
		public Buffer construct(GL context, int[] iArray);
	}
	
	public abstract void delete(GL gl);
}
