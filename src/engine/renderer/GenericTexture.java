package engine.renderer;

import engine.toolsAWT.BufferTexture;


public abstract class GenericTexture<GL>{

	public abstract void delete(GL gl);
	
	public abstract int getWidth();
	public abstract int getHeight();
	
	/**
	 * Copy the indicated subrect of this texture into the indicated BufferTexture. 
	 * The resulting BufferTexture will have size <i>approximately</i> 
	 * srcWidth x srcHeight pixels.
	 */
	public abstract void getAsBufferTexture(GL gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight);
	
	/**
	 * Copy the indicated subrect of this texture into the indicated BufferTexture.
	 * The subrect will be proportionately downscaled, if necessary, so that its maximum 
	 * dimension does not exceed the target scaled size. 
	 * The resulting BufferTexture will be the rescaled size.
	 */
	public abstract void getAsBufferTexture(GL gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight,
		int targetScaledWidth, int targetScaledHeight);
}
