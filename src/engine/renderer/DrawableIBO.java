package engine.renderer;

import engine.renderer.GenericBuffer.BufferConstructor;

public class DrawableIBO<GL, Buffer extends GenericBuffer<GL>> {
	
	private int triCount;
	private Buffer indexBuffer;
	
	private int edgeCount;
	private Buffer indexBufferWireframe;
	
	//whether wireframe data is available
	protected boolean hasWireframeData;
	
	
	public DrawableIBO(GL context, int[] iArray, int triCount, 
		BufferConstructor<GL, Buffer> bufferConstructor){
		
		this.triCount = triCount;
		indexBuffer = bufferConstructor.construct(context, iArray);
		
		hasWireframeData = false;
		this.edgeCount = 0;
	}
	
	public DrawableIBO(GL context, int[] iArray, int triCount, int[] iArrayWireframe, int edgeCount,
		BufferConstructor<GL, Buffer> bufferConstructor){

		this.triCount = triCount;
		indexBuffer = bufferConstructor.construct(context, iArray);
		
		hasWireframeData = true;
		this.edgeCount = edgeCount;
		indexBufferWireframe = bufferConstructor.construct(context, iArrayWireframe);
	}
	
	
	//get info
	public boolean hasWireframeData(){ return hasWireframeData; }
	
	public int getTriCount(){ return triCount; }
	public int getEdgeCount(){ return edgeCount; }
	
	//get buffer
	public Buffer getWireframeIndexBuffer(){ return indexBufferWireframe; }
	public Buffer getIndexBuffer(){ return indexBuffer; }
	
	//delete buffers
	public void delete(GL gl){
		
		indexBuffer.delete(gl);
		
		if(hasWireframeData){
			indexBufferWireframe.delete(gl);
		}
	}
}
