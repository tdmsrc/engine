package engine.renderer;

import java.util.LinkedList;

import engine.renderer.GenericBuffer.BufferConstructor;
import engine.scene.Scene;
import geometry.math.Camera;

/* GENERIC RENDERER 
 * ---------------------------------------
 * To USE a Renderer:
 * 
 * Create an instance of the Renderer.
 * 
 * Add any Initialize/Resize/Display/DisposeListeners (see below for when they get called)
 * - use ResizeListener interface for changing Camera aspect ratio--not done automatically
 * - Any initialize listener must be added before the Renderer is initialized
 * Can add any e.g. MouseListeners etc to the GUI component using getCanvasObject()
 * 
 * Call Renderer.render() to display, but before doing so,
 * check that it has been initialized using isInitialized()
 * and make sure to set a Scene and Camera using setScene(), setCamera().
 * 
 * ---------------------------------------
 * To MAKE a Renderer:
 * - Make a Buffer class extending GenericBuffer, e.g. ref to an OpenGL VBO
 * - Make a Texture class extended GenericTexture, e.g. ref to an OpenGL texture ID 
 * - Make a Renderer class extending Renderer
 * 
 * In the Renderer class, implement:
 * - render() <- (NB: the Scene and Camera may be null, so check this)
 * - getCanvasObject()
 * - getBufferConstructor()
 * 
 * And make sure that:
 * - before Renderer is finished initializing, call notifyInitializeListeners() 
 * - when the viewport gets resized, call notifyResizeListeners()
 * - when performing a render, first call notifyDisplayListeners()
 * - when disposing the Renderer, call notifyDisposeListeners(), to e.g. free GL stuff
 */

public abstract class Renderer
	<Canvas, GL,
	Buffer extends GenericBuffer<GL>,
	Texture extends GenericTexture<GL>>
{

	//initialize listener interface
	public static interface RendererInitializeListener
		<GL, Buffer extends GenericBuffer<GL>, Texture extends GenericTexture<GL>>{
		public void postInitialize(GL gl, Renderer<?,GL,Buffer,Texture> renderer);
	}
	private LinkedList<RendererInitializeListener<GL,Buffer,Texture>> initializeListeners;
	
	//display listener interface
	public static interface RendererDisplayListener
		<GL, Buffer extends GenericBuffer<GL>, Texture extends GenericTexture<GL>>{
		public void preDisplay(GL gl, Renderer<?,GL,Buffer,Texture> renderer);
	}
	private LinkedList<RendererDisplayListener<GL,Buffer,Texture>> displayListeners;
	
	//dispose listener interface
	public static interface RendererDisposeListener<GL>{
		public void preDispose(GL gl);
	}
	private LinkedList<RendererDisposeListener<GL>> disposeListeners;

	//resize listener interface
	public static interface RendererResizeListener{
		public void viewportResized(int viewportWidth, int viewportHeight);
	}
	private LinkedList<RendererResizeListener> resizeListeners;
	
	//user-specified data used to render
	protected Scene<GL,Buffer,Texture> scene;
	protected Camera camera;
	
	//some options
	//[TODO]
	//private boolean debugMode; 
	
	//set true once renderer is ready to render
	private boolean initialized;
	
	
	public Renderer(){
		initialized = false;
		
		//[TODO]
		//debugMode = false;
		camera = null;
		scene = null;
		
		initializeListeners = new LinkedList<RendererInitializeListener<GL,Buffer,Texture>>();
		displayListeners = new LinkedList<RendererDisplayListener<GL,Buffer,Texture>>();
		disposeListeners = new LinkedList<RendererDisposeListener<GL>>();
		resizeListeners = new LinkedList<RendererResizeListener>();
	}
	
	/**
	 * Set the Scene object that this Renderer will draw each time 
	 * Renderer.render() is called.
	 */
	public void setScene(Scene<GL,Buffer,Texture> scene){ this.scene = scene; }
	
	/**
	 * Set the Camera object that this Renderer will use each time
	 * Renderer.render() is called.
	 */
	public void setCamera(Camera camera){ this.camera = camera; }
	
	//[TODO]
	/**
	 * Determine whether or not DrawOptions.MASK_DEBUG objects should be visible.
	 */
	//public boolean getDebugMode(){ return debugMode; }
	
	/**
	 * Indicate whether or not DrawOptions.MASK_DEBUG objects should be visible.
	 * 
	 * @param debugMode True if debug objects should be visible, false otherwise.
	 */
	//public void setDebugMode(boolean debugMode){ this.debugMode = debugMode; }
	
	/**
	 * Add a RendererInitializeListener.
	 */
	public void addInitializeListener(RendererInitializeListener<GL,Buffer,Texture> initializeListener){
		initializeListeners.add(initializeListener);
	}
	
	/**
	 * Add a RendererDisplayListener.
	 */
	public void addDisplayListener(RendererDisplayListener<GL,Buffer,Texture> displayListener){
		displayListeners.add(displayListener);
	}
	
	/**
	 * Add a RendererDisposeListener.
	 */
	public void addDisposeListener(RendererDisposeListener<GL> disposeListener){
		disposeListeners.add(disposeListener);
	}
	
	/**
	 * Add a RendererResizeListener.
	 */
	public void addResizeListener(RendererResizeListener resizeListener){
		resizeListeners.add(resizeListener);
	}
	
	/**
	 * Determine whether the Renderer has been initialized.
	 * 
	 * @return True if the Renderer has been initialized, false otherwise.
	 */
	public boolean isInitialized(){
		return initialized;
	}
	
	/**
	 * Indicate that the Renderer has been initialized, and is ready to render.
	 * Call the postInitialize method for the RendererListeners.
	 */
	protected void notifyInitializeListeners(GL gl){
		for(RendererInitializeListener<GL,Buffer,Texture> initializeListener : initializeListeners){
			initializeListener.postInitialize(gl, this);
		}
		
		initialized = true;
	}
	
	/**
	 * Notify RendererResizeListeners of a viewport resize.
	 */
	protected void notifyResizeListeners(int viewportWidth, int viewportHeight){
		for(RendererResizeListener resizeListener : resizeListeners){
			resizeListener.viewportResized(viewportWidth, viewportHeight);
		}
	}

	/**
	 * Notify RendererDisplayListeners that a display is occuring.
	 */
	protected void notifyDisplayListeners(GL gl){
		for(RendererDisplayListener<GL,Buffer,Texture> displayListener : displayListeners){
			displayListener.preDisplay(gl, this);
		}
	}
	
	/**
	 * Notify RendererDisposeListeners that the Renderer will be disposed.
	 */
	protected void notifyDisposeListeners(GL gl){
		for(RendererDisposeListener<GL> disposeListener : disposeListeners){
			disposeListener.preDispose(gl);
		}
	}
	
	/**
	 * Renders using the Scene and Camera specified using the constructor
	 * or using the methods setScene and setCamera.
	 * 
	 * @see Renderer.setScene, Renderer.setCamera
	 */
	public abstract void render();
		
	/**
	 * Get the canvas onto which this Renderer draws.
	 * 
	 * @return Any type of object, e.g. a GUI component.
	 */
	public abstract Canvas getCanvasObject();
	
	/**
	 * Get a BufferConstructor which can be used to create the 
	 * type of GenericBuffer that this Renderer uses.
	 */
	public abstract BufferConstructor<GL,Buffer> getBufferConstructor();
}
