package engine.renderer;

import engine.renderer.GenericBuffer.BufferConstructor;
import engine.tools.SerializedMesh;


/**
 * Essentially the same as {@link SerializedMesh}, but with data stored in 
 * {@link Buffer} objects rather than arrays.
 */
public class Drawable<GL, Buffer extends GenericBuffer<GL>>{

	//wireframe
	protected int edgeCount;
	protected Buffer vertexWireframeBuffer;
	
	//solid
	protected int triCount;
	protected Buffer vertexPositionBuffer, vertexTexBuffer, vertexNormalBuffer;
	protected Buffer vertexTangentSBuffer, vertexTangentTBuffer;
	
	//whether tangent data, wireframe data is available
	protected boolean hasTangentData, hasWireframeData;
	
	
	/**
	 * Create a {@link Drawable} using the geometry of a {@link SerializedMesh}.
	 * 
	 * @param bufferConstructor Specifies how to create a {@link Buffer} from a float array
	 */
	public Drawable(GL context, SerializedMesh smesh, 
		BufferConstructor<GL, Buffer> bufferConstructor){
		
		triCount = smesh.triCount;
		vertexPositionBuffer = bufferConstructor.construct(context, smesh.position, smesh.positionDimension);
		vertexTexBuffer = bufferConstructor.construct(context, smesh.tex, 2);
		vertexNormalBuffer = bufferConstructor.construct(context, smesh.normal, 3);
		
		hasTangentData = smesh.hasTangentData;
		if(hasTangentData){
			vertexTangentSBuffer = bufferConstructor.construct(context, smesh.tangentS, 3);
			vertexTangentTBuffer = bufferConstructor.construct(context, smesh.tangentT, 3);
		}
		
		hasWireframeData = smesh.hasWireframeData;
		if(hasWireframeData){
			edgeCount = smesh.edgeCount;
			vertexWireframeBuffer = bufferConstructor.construct(context, smesh.positionWireframe, smesh.positionDimension);
		}
		else{ edgeCount = 0; }
	}
	
	
	//get info
	public boolean hasWireframeData(){ return hasWireframeData; }
	public boolean hasTangentData(){ return hasTangentData; }
	
	public int getTriCount(){ return triCount; }
	public int getEdgeCount(){ return edgeCount; }
	
	//get buffers
	public Buffer getWireframeBuffer(){ return vertexWireframeBuffer; }
	public Buffer getPositionBuffer(){ return vertexPositionBuffer; }
	public Buffer getTexBuffer(){ return vertexTexBuffer; }
	public Buffer getNormalBuffer(){ return vertexNormalBuffer; }
	public Buffer getTangentSBuffer(){ return vertexTangentSBuffer; }
	public Buffer getTangentTBuffer(){ return vertexTangentTBuffer; }
	
	//delete buffers
	public void delete(GL gl){
		
		vertexPositionBuffer.delete(gl);
		vertexTexBuffer.delete(gl);
		vertexNormalBuffer.delete(gl);
		
		if(hasTangentData){ 
			vertexTangentSBuffer.delete(gl);
			vertexTangentTBuffer.delete(gl);
		}
		
		if(hasWireframeData){
			vertexWireframeBuffer.delete(gl);
		}
	}
}
