package engine.renderer;


public class Material<Texture extends GenericTexture<?>>{
	
	//material name
	private String name;
	
	//texture(s) and texture data flags
	private Texture texColor, texBump;
	
	private boolean hasAlphaMask; //mask by alpha threshhold 0.1 in texColor
	private boolean hasTexBump; 
	private boolean hasReliefData; //assumed to be alpha of texBump
	
	private float reliefMappingHeight;
	
	//lighting properties
	private float diffuseCoefficient, specularCoefficient, specularExponent;
	
	
	private Material(String name){
		
		this.name = name;
		
		//default values
		reliefMappingHeight = 0.04f;
		
		diffuseCoefficient = 1.0f;
		specularCoefficient = 0.2f;
		specularExponent = 8.0f;
	}
	
	public Material(String name, Texture texColor, boolean hasAlphaMask){
		this(name);
		
		this.texColor = texColor;
		this.hasAlphaMask = hasAlphaMask;
		
		hasTexBump = false;
		hasReliefData = false;
	}
	
	public Material(String name, Texture texColor, boolean hasAlphaMask, Texture texBump, boolean hasReliefData){
		this(name);
		
		this.texColor = texColor;
		this.hasAlphaMask = hasAlphaMask;
		
		this.texBump = texBump;
		hasTexBump = true;
		this.hasReliefData = hasReliefData;
	}
	
	
	public String getName(){ return name; }
	public void setName(String name){ this.name = name; }
	
	
	//textures
	public Texture getTexColor(){ return texColor; }
	public Texture getTexBump(){ return texBump; }
	
	public void setTexColor(Texture texColor){ this.texColor = texColor; }
	public void setTexBump(Texture texBump){ this.texBump = texBump; }
	
	public boolean hasAlphaMask(){ return hasAlphaMask; }
	public void setHasAlphaMask(boolean hasAlphaMask){ this.hasAlphaMask = hasAlphaMask; }
	
	public boolean hasTexBump(){ return hasTexBump; }
	public void setHasTexBump(boolean hasTexBump){ this.hasTexBump = hasTexBump; }
	
	public boolean hasReliefData(){ return hasReliefData; }
	public void setHasReliefData(boolean hasReliefData){ this.hasReliefData = hasReliefData; }
	
	//relief mapping height
	public float getReliefMappingHeight(){ return reliefMappingHeight; }
	public void setReliefMappingHeight(float reliefMappingHeight){ this.reliefMappingHeight = reliefMappingHeight; }
	
	//lighting coefficients
	public float getDiffuseCoefficient(){ return diffuseCoefficient; }
	public float getSpecularCoefficient(){ return specularCoefficient; }
	public float getSpecularExponent(){ return specularExponent; }
	
	public void setDiffuseCoefficient(float diffuseCoefficient){ this.diffuseCoefficient = diffuseCoefficient; }
	public void setSpecularCoefficient(float specularCoefficient){ this.specularCoefficient = specularCoefficient; }
	public void setSpecularExponent(float specularExponent){ this.specularExponent = specularExponent; }
}
