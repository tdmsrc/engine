package engine.tools;

import engine.collision.BoundingBox;
import engine.collision.ColliderGroup;
import engine.collision.PickListener;
import engine.renderer.Drawable;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.renderer.Material;
import engine.renderer.GenericBuffer.BufferConstructor;
import engine.scene.DrawOptions;
import engine.scene.SceneObject;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData;
import geometry.rawgeometry.ObjFileLoader;

/**
 * Given an ObjData and a Material, this class will spawn SceneObjects and Colliders.
 * Useful if multiple instances of a particular object will be created.
 */
public class SceneObjectFactory<GL,
	Buffer extends GenericBuffer<GL>,
	Texture extends GenericTexture<GL>>{

	private long nextInstanceLabel; //used to automatically generate instance names
	private String name;
	
	private ObjData objData;
	private Drawable<GL,Buffer> drawable;
	private BoundingBox boundingBox;
	private Material<Texture> material;
	
	/**
	 * Create {@link SceneObjectFactory} from {@link ObjData} and {@link Material}.
	 * 
	 * @see {@link ObjFileLoader#readObjFileVertices}
	 */
	public SceneObjectFactory(GL gl, String name,
		ObjData objData, Material<Texture> material,
		BufferConstructor<GL,Buffer> bufferConstructor){
		
		nextInstanceLabel = 0;
		this.name = name;
		
		this.objData = objData;
		boundingBox = BoundingBox.createFromObjData(objData, null);
		drawable = new Drawable<GL,Buffer>(gl, SerializedMesh.getFromObjData(objData, objData.hasTex, true), bufferConstructor);
		
		this.material = material;
	}
	
	public String getName(){ return name; }
	
	public ObjData getObjData(){ return objData; }
	public Drawable<GL,Buffer> getDrawable(){ return drawable; }
	public Material<Texture> getMaterial(){ return material; }
	
	/**
	 * Create a {@link SceneObject} using this {@link SceneObjectFactory}'s {@link Drawable} and {@link Material}.
	 * 
	 * @param name The name of the SceneObject to be created.
	 * @return A SceneObject.
	 */
	public SceneObject<GL,Buffer,Texture> spawnInstance(){
		
		return spawnInstance(name + " " + nextInstanceLabel++);
	}
	
	/**
	 * Create a {@link SceneObject} using this {@link SceneObjectFactory}'s {@link Drawable} and {@link Material}.
	 * 
	 * @param name The name of the SceneObject to be created.
	 * @return A SceneObject.
	 */
	public SceneObject<GL,Buffer,Texture> spawnInstance(String name){
		
		return new SceneObject<GL,Buffer,Texture>(name, drawable, boundingBox, new DrawOptions(), material);
	}
	
	/**
	 * Create a BoundingBox to be used for SceneObjects spawned by this factory.
	 */
	public BoundingBox spawnBoundingBox(PickListener<Vector3d> pickListener){
		return BoundingBox.createFromObjData(objData, pickListener);
	}
	
	/**
	 * Create a Collider to be used for SceneObjects spawned by this factory.
	 */
	public ColliderGroup spawnColliderTriangles(PickListener<Vector3d> pickListener){
		return ColliderGroup.createTriangleBoxFromObjData(objData, pickListener);
	}
	
	/**
	 * Create a Collider to be used for SceneObjects spawned by this factory.
	 */
	public ColliderGroup spawnColliderEdges(PickListener<Vector3d> pickListener){
		//[TODO]
		return ColliderGroup.createTriangleBoxFromObjData(objData, pickListener);
	}
}
