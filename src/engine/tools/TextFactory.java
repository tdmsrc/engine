package engine.tools;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import engine.collision.BoundingBox;
import engine.renderer.Drawable;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.renderer.Material;
import engine.renderer.GenericBuffer.BufferConstructor;
import engine.scene.DrawOptions;
import engine.scene.SceneObject;
import geometry.common.MessageOutput;
import geometry.math.Vector3d;
import glfont.GLFontData;
import glfont.GLFontData.Metric;

/* 
 * To be used with output produced by the GLFont project
 */

public class TextFactory<GL, 
	Buffer extends GenericBuffer<GL>, 
	Texture extends GenericTexture<GL>>
{
	
	protected static final int VERTEX_DIMENSION = 2;
	
	protected static final float SCALE_POS = 1.0f/256.0f;
	protected static final float SCALE_TEX = 1.0f/256.0f;
	protected static final boolean WRAP_ONLY_ON_SPACE = true;
	
	protected static final float LEADING = 1.0f; //space between descent of one line to ascent of next line
	
	protected static class Caret{
		
		public float caretX, caretY;
		public int posIndex, texIndex, normalIndex, wireframeIndex;
		
		public Caret(float x0, float y0){
			caretX = x0; caretY = y0;
			posIndex = 0; texIndex = 0; normalIndex = 0; wireframeIndex = 0;
		}
	}
	
	/**
	 * Indicates whether the text should be above, below, or centered at the origin. 
	 */
	public enum TextVerticalAlignment{
		/** The text will be above the origin; i.e., the bottom of the text is at the origin. */
		ABOVE{ @Override public float getCenterY(float minY, float maxY){ return minY; }}, 
		/** The text will be below the origin; i.e., the top of the text is at the origin. */
		BELOW{ @Override public float getCenterY(float minY, float maxY){ return maxY; }},
		/** The text will be centered vertically at the origin. */
		CENTER{ @Override public float getCenterY(float minY, float maxY){ return (minY+maxY)/2.0f; }};
		
		public abstract float getCenterY(float minY, float maxY);
	}
	
	/**
	 * Indicates whether the text should be to the left, to the right, or centered at the origin.
	 */
	public enum TextHorizontalAlignment{
		/** The text will be to the left of the origin; i.e., the right edge of the text is at the origin. */
		LEFT{ @Override public float getCenterX(float minX, float maxX){ return maxX; }},
		/** The text will be to the right of the origin; i.e., the left edge of the text is at the origin. */
		RIGHT{ @Override public float getCenterX(float minX, float maxX){ return minX; }},
		/** The text will be centered horizontally at the origin. */
		CENTER{ @Override public float getCenterX(float minX, float maxX){ return (minX+maxX)/2.0f; }};
		
		public abstract float getCenterX(float minX, float maxX);
	}
	
	private Material<Texture> fontMaterial;
	int fontTextureHeight;
	float scaleTexX, scaleTexY;
	boolean flipTexY;
	
	private GLFontData glFontData;
	private BufferConstructor<GL,Buffer> bufferConstructor;
	
	private boolean constructWireframeData;
	
	private TextHorizontalAlignment horizontalAlignment;
	private TextVerticalAlignment verticalAlignment;
	
	
	public TextFactory(Texture glFontTexture, InputStream glFontDAT, 
		boolean constructWireframeData, 
		TextHorizontalAlignment horizontalAlignment, TextVerticalAlignment verticalAlignment,
		BufferConstructor<GL,Buffer> bufferConstructor){
		
		fontMaterial = new Material<Texture>("Font Material", glFontTexture, true);
		//fontMaterial.setSpecularCoefficient(0.0f);
		
		fontTextureHeight = glFontTexture.getHeight();
		scaleTexX = 1.0f / (float)glFontTexture.getWidth();
		scaleTexY = 1.0f / (float)fontTextureHeight;
		flipTexY = true;
		
		this.bufferConstructor = bufferConstructor;
		
		this.horizontalAlignment = horizontalAlignment;
		this.verticalAlignment = verticalAlignment;
		
		this.constructWireframeData = constructWireframeData;
		
		try{ glFontData = GLFontData.readFromFile(glFontDAT); }
        catch(Exception e){
        	MessageOutput.printError(e.getMessage());
        }
	}
	
	
	//=================================================
	// SCENE OBJECT CREATION
	//=================================================
	
	/**
	 * Creates a new SerializedMesh with arrays initialized to the necessary size.
	 * <br><br>
	 * It is possible the arrays will be larger than necessary, since charCount may
	 * include spaces or characters for which there are no metrics; these will not 
	 * contribute to the SerializedMesh data.
	 */
	protected SerializedMesh getNewSerializedMesh(int charCount){
		
		SerializedMesh smesh = new SerializedMesh(false, constructWireframeData, VERTEX_DIMENSION);

		smesh.triCount = charCount*2;
		smesh.position = new float[charCount*6*VERTEX_DIMENSION];
		smesh.tex = new float[charCount*6*2];
		smesh.normal = new float[charCount*6*3];
		
		if(constructWireframeData){
			smesh.edgeCount = charCount*4;
			smesh.positionWireframe = new float[charCount*8*VERTEX_DIMENSION];
		}
		
		return smesh;
	}
	
	//make sure box bounds billboarded text, since text is often billboarded
	//also, subtract a lineStep from minY to ensure the stuff below baseline is included
	//(minY is computed so that it is at the baseline of the lowest line)
	protected BoundingBox getBoundingBox(
		float offsetMinX, float offsetMaxX, 
		float offsetMinY, float offsetMaxY, float lineStep){

		//"radius" is max(-offsetMinX, offsetMaxX)
		float boundingBoxRadius = -offsetMinX;  
		if(offsetMaxX > boundingBoxRadius){ boundingBoxRadius = offsetMaxX; }
		
		return new BoundingBox(
			new Vector3d(-boundingBoxRadius, offsetMinY-lineStep, -boundingBoxRadius), 
			new Vector3d(boundingBoxRadius, offsetMaxY, boundingBoxRadius),
			null);
	}
	
	protected SceneObject<GL,Buffer,Texture> createSceneObject(GL gl, 
		String name, SerializedMesh textSmesh, BoundingBox boundingBox){
		
		Drawable<GL,Buffer> textDrawable = 
			new Drawable<GL,Buffer>(gl, textSmesh, bufferConstructor);
		
		return new SceneObject<GL,Buffer,Texture>(name, 
			textDrawable, boundingBox, new DrawOptions(), fontMaterial);
	}
	
	
	public SceneObject<GL,Buffer,Texture> getTextObjectNoWrap(GL gl, 
		String name, String text, int maxStrLen){
	
		//get max(text.length, maxStrLen)
		int charCount = text.length();
		if(maxStrLen < charCount){ charCount = maxStrLen; }
		
		//allocate arrays for serialized mesh
		SerializedMesh textSmesh = getNewSerializedMesh(charCount);
		
		//get geometry of this text object (bounds have min_y at baseline of lowest line)
		float lineStep = (glFontData.getLineHeight() + LEADING)*SCALE_POS;
		
		float minX = 0.0f, maxX = getLineWidth(text, 0, charCount);
		float minY = 0.0f, maxY = lineStep;
		
		//set up offset depending on alignment
		float centerX = horizontalAlignment.getCenterX(minX, maxX);
		float centerY = verticalAlignment.getCenterY(minY, maxY);

		//put the chars
		Caret caret = new Caret(0,0);
		putLine(caret, textSmesh, text, 0, charCount, -centerX, -centerY);
		
		//return a SceneObject
		return createSceneObject(gl, name, textSmesh, 
			getBoundingBox(minX-centerX, maxX-centerX, minY-centerY, maxY-centerY, lineStep));
	}
	
	public SceneObject<GL,Buffer,Texture> getTextObjectWrap(GL gl, 
		String name, String text, int maxStrLen, float wrapWidth){ 
		
		//get max(text.length, maxStrLen)
		int charCount = text.length();
		if(maxStrLen < charCount){ charCount = maxStrLen; }
		
		//allocate arrays for serialized mesh
		SerializedMesh textSmesh = getNewSerializedMesh(charCount);
		
		//wrap the string and get line step
		List<String> lines = wrapText(text, wrapWidth);
		
		//get geometry of this text object (bounds have min_y at baseline of lowest line)
		float lineStep = (glFontData.getLineHeight() + LEADING)*SCALE_POS;
		
		float minX = 0.0f, maxX = wrapWidth;
		float minY = -(lines.size()-1)*lineStep, maxY = lineStep;
		
		//set up offset depending on alignment
		float centerX = horizontalAlignment.getCenterX(minX, maxX);
		float centerY = verticalAlignment.getCenterY(minY, maxY);
		
		//put the chars
		Caret caret = new Caret(0,0);
		for(String line : lines){
			//put wrapped line of text
			putLine(caret, textSmesh, line, 0, line.length(), -centerX, -centerY);
			//set raster position to new line
			caret.caretX = 0;
			caret.caretY -= lineStep;
		}
		
		//return a SceneObject
		return createSceneObject(gl, name, textSmesh, 
			getBoundingBox(minX-centerX, maxX-centerX, minY-centerY, maxY-centerY, lineStep));
	}
	
	
	//=================================================
	// METHODS FOR CONSTRUCTING MESH
	//=================================================
	
	private static int putFloats(float[] buffer, int index, float ... x){
		
		for(int i=0; i<x.length; i++){ buffer[index+i] = x[i]; }
		return x.length;
	}
	
	/**
	 * Put a character into the SerializedMesh at the position and
	 * array indices indicated by Caret; update Caret.
	 */
	protected void putChar(Caret caret, SerializedMesh smesh, EnumMap<Metric, Integer> charMetrics, float offsetX, float offsetY){

		//vertex data at caret position
		float v_x0 = caret.caretX;
		float v_x1 = v_x0 + SCALE_POS*charMetrics.get(Metric.WIDTH);
		float v_y0 = caret.caretY;
		float v_y1 = v_y0 + SCALE_POS*charMetrics.get(Metric.HEIGHT);
		
		//contribute char metrics OFFSET_X
		float xPadding = charMetrics.get(Metric.OFFSET_X);
		v_x0 += xPadding*SCALE_POS; v_x1 += xPadding*SCALE_POS;
		
		//contribute char metrics OFFSET_Y
		float yPadding = charMetrics.get(Metric.OFFSET_Y);
		v_y0 -= yPadding*SCALE_POS; v_y1 -= yPadding*SCALE_POS;
		
		//contribute other offset
		v_x0 += offsetX; v_x1 += offsetX;
		v_y0 += offsetY; v_y1 += offsetY;

		caret.posIndex += putFloats(smesh.position, caret.posIndex, 
			v_x0, v_y0,	 v_x1, v_y0,  v_x1, v_y1,
			v_x0, v_y0,  v_x1, v_y1,  v_x0, v_y1);

		if(constructWireframeData){
			caret.wireframeIndex += putFloats(smesh.positionWireframe, caret.wireframeIndex, 
				v_x0, v_y0,  v_x1, v_y0,
				v_x1, v_y0,  v_x1, v_y1,
				v_x1, v_y1,  v_x0, v_y1,
				v_x0, v_y1,  v_x0, v_y0);
		}
		
		//texture coords
		float t_x0 = scaleTexX * charMetrics.get(Metric.TEXTURE_X);
		float t_x1 = scaleTexX * (charMetrics.get(Metric.TEXTURE_X) + charMetrics.get(Metric.WIDTH));
		
		float t_y0, t_y1;
		if(flipTexY){
			t_y0 = scaleTexY * (fontTextureHeight - charMetrics.get(Metric.TEXTURE_Y));
			t_y1 = scaleTexY * (fontTextureHeight - (charMetrics.get(Metric.TEXTURE_Y) - charMetrics.get(Metric.HEIGHT)));
		}else{
			t_y0 = scaleTexY * charMetrics.get(Metric.TEXTURE_Y);
			t_y1 = scaleTexY * (charMetrics.get(Metric.TEXTURE_Y) - charMetrics.get(Metric.HEIGHT));
		}

		caret.texIndex += putFloats(smesh.tex, caret.texIndex, 
			t_x0, t_y0,  t_x1, t_y0,  t_x1, t_y1,
			t_x0, t_y0,  t_x1, t_y1,  t_x0, t_y1);

		//normals
		//[TODO] this is a waste of space...
		caret.normalIndex += putFloats(smesh.normal, caret.normalIndex, 
			0,0,1,  0,0,1,  0,0,1,
			0,0,1,  0,0,1,  0,0,1);

		//update caret
		caret.caretX += SCALE_POS * charMetrics.get(Metric.WIDTH);
	}
	
	/**
	 * Uses putChar on each character in the range of the string [startPos, maxPos).
	 */
	protected void putLine(Caret caret, SerializedMesh smesh, String text, int startPos, int maxPos, float offsetX, float offsetY){

		for(int i=startPos; i<maxPos; i++){
			//get current char
			char c = text.charAt(i);
			//check if current char is a space
			if(c == ' '){
				caret.caretX += glFontData.getSpaceWidth()*SCALE_POS;
				continue;
			}
			//get char metrics
			EnumMap<Metric, Integer> charMetrics = glFontData.getCharMetrics((int)c);			
			if(charMetrics == null){ continue; }
			//put char
			putChar(caret, smesh, charMetrics, offsetX, offsetY);
		}
	}
	
	
	//=================================================
	// METRICS METHODS
	//=================================================
	
	/**
	 * Get the width of the specified char according to glFontData.
	 * Returns 0 if the char has no metrics.
	 */
	protected float getScaledCharWidth(char c){
		
		//check if current char is a space
		if(c == ' '){
			return glFontData.getSpaceWidth()*SCALE_POS;
		}
		
		//get char metrics
		EnumMap<Metric, Integer> charMetrics = glFontData.getCharMetrics((int)c);			
		if(charMetrics == null){ return 0; }
		return charMetrics.get(Metric.WIDTH)*SCALE_POS;
	}
	
	/**
	 * Traverse the string in indicated range, adding up char widths.
	 */
	protected float getLineWidth(String text, int startPos, int maxPos){
		
		float totalWidth = 0; 
		for(int i=startPos; i<maxPos; i++){
			totalWidth += getScaledCharWidth(text.charAt(i));
		}
		
		return totalWidth;
	}
	
	/**
	 * Traverse the string in indicated range, adding up char widths,
	 * until the textWrapWidth is exceeded.  Returns the first char NOT on the line
	 * or, if WRAP_ONLY_ON_SPACE, then the char after the last space on the line (if one exists).
	 */
	protected int getLineEnd(String text, int startPos, int maxPos, float wrapWidth){
		
		boolean foundSpace = false;
		int lastSpace = startPos;
		
		float totalWidth = 0;
		for(int i=startPos; i<maxPos; i++){
			
			//get current char
			char c = text.charAt(i);
			if(c == ' '){ lastSpace = i; foundSpace = true; }
			
			//add width
			totalWidth += getScaledCharWidth(c);
			
			//either the overflow character or the char after lastSpace, depending on wrap options
			if(totalWidth > wrapWidth){
				if(WRAP_ONLY_ON_SPACE && foundSpace){ return Math.min(lastSpace+1, maxPos); }
				else{ return i; } 
			}
		}
		
		return maxPos;
	}
	

	/**
	 * Splits "text" into multiple lines, none of which has width exceeding wrapWidth.
	 * Only allows line breaks at spaces if WRAP_ONLY_ON_SPACE is true.
	 */
	protected List<String> wrapText(String text, float wrapWidth){
		
		//prepare list of strings
		ArrayList<String> textLines = new ArrayList<String>();
		
		//get string length
		int charCount = text.length();
		
		//keep applying getLineEnd to wrap
		int startPos = 0;
		int maxPos = getLineEnd(text, startPos, charCount, wrapWidth);
		textLines.add(text.substring(startPos, maxPos));

		while(maxPos < charCount){
			//find next line's limits
			startPos = maxPos;
			maxPos = getLineEnd(text, startPos, charCount, wrapWidth);
			//store string
			textLines.add(text.substring(startPos, maxPos));
		}
		
		//return list of strings
		return textLines;
	}
}
