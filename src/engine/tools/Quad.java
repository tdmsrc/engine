package engine.tools;

import engine.renderer.GenericBuffer;
import engine.renderer.GenericBuffer.BufferConstructor;

//single quad encoded in buffer of floats

public class Quad<GL, Buffer extends GenericBuffer<GL>>{

	private static final int TRI_COUNT = 2;
	private Buffer vertexPositionBuffer, vertexTexBuffer;


	public Quad(GL gl, BufferConstructor<GL,Buffer> bufferConstructor){
		
		vertexPositionBuffer = bufferConstructor.construct(gl, new float[]{
			-1.0f,-1.0f,	 1.0f,-1.0f,	 1.0f, 1.0f, 
			 1.0f, 1.0f,	-1.0f, 1.0f,	-1.0f,-1.0f
		}, 2);
		
		vertexTexBuffer = bufferConstructor.construct(gl, new float[]{
			0.0f, 0.0f,		1.0f, 0.0f,		1.0f, 1.0f,	
			1.0f, 1.0f,		0.0f, 1.0f,		0.0f, 0.0f
		}, 2);
	}
	
	public int getTriCount(){ return TRI_COUNT; }
	
	//bind buffers
	public Buffer getPositionBuffer(){ 
		return vertexPositionBuffer;
	}
	
	public Buffer getTexBuffer(){ 
		return vertexTexBuffer;
	}
}
