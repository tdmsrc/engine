package engine.tools;

import java.util.Collection;

import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.polygon.triangulate.Triangle2d;
import geometry.polyhedron.components.EmbeddedEdge;
import geometry.polyhedron.components.EmbeddedVertex;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFaceTriangulated;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.rawgeometry.ObjData;
import geometry.rawgeometry.ObjData.VertexData;


/**
 * Represents geometry in a way that is easy to use with OpenGL.
 * The geometry is assumed to consist of triangles, and vertex attributes
 * are serialized in float arrays.  For example:
 * <ul>
 * <li>{@link #position} consists of repeating 9-float sequences specifying triangle position: 
 * <pre>{@code x0 y0 z0 x1 y1 z1 x2 y2 z2}</pre>
 * <li>{@link #tex} consists of repeating 6-float sequences specifying triangle texture coords:
 * <pre>{@code u0 v0 u1 v1 u2 v2}</pre>
 * </ul>
 * Optionally includes edge data (for wireframe rendering) and tangent vector data (for bumpmapping).
 * Creation:
 * <ul>
 * <li>From a Polyhedron: {@link #getFromPolyhedronFaces(Collection)}
 * <li>From {@link ObjData}: 
 *   <ul>
 *   <li>{@link #getFromObjData(ObjData)}
 *   <li>{@link #getFromObjData(ObjData, boolean, boolean)}
 *   </ul>
 * </ul>
 */
public class SerializedMesh{

	//solid (i.e., data for GL_TRIANGLES)
	public int triCount;
	public int positionDimension;
	public float[] position;
	public float[] tex;
	public float[] normal;
	
	//used for bumpmapping
	public boolean hasTangentData;
	public float[] tangentS;
	public float[] tangentT;
	
	//wireframe (i.e., data for GL_LINES)
	public boolean hasWireframeData;
	public int edgeCount;
	public float[] positionWireframe;
	
	
	public SerializedMesh(boolean hasTangentData, boolean hasWireframeData, int positionDimension){
		this.hasTangentData = hasTangentData;
		this.hasWireframeData = hasWireframeData;
		
		this.positionDimension = positionDimension;
	}
	
	/**
	 * Bound all the positions of this SerializedMesh by a box,
	 * subtract the center of that box from every position, 
	 * then return a BoundingBox containing all the new positions.
	 * <br><br>
	 * Assumes positionDimension <= 3.  If positionDimension < 3, 
	 * interprets the missing components as being 0.
	 * <br><br>
	 * Assumes the position array has length a multiple of positionDimension,
	 * and that it is populated by the components of positions.
	 * 
	 * @return A BoundingBox fitting the positions in this SerializedMesh,
	 * which have been shifted so the center of the box is 0.
	 */
	/*public BoundingBox offsetToOrigin(){
		
		int n = position.length / positionDimension;
		
		float[] min = new float[positionDimension];
		float[] max = new float[positionDimension];
		
		int pi = 0;
		//set min and max to 0th position
		
		for(int i=1; i<n; i++){
			//compare each component of ith position with min and max
			for(int k=0; k<positionDimension; k++){
				
			}
		}
		
		//offset all the points
	}*/
	
	private static int putFloats(float[] buffer, int startIndex, Vector2d v){
		buffer[startIndex  ] = v.getX();
		buffer[startIndex+1] = v.getY();
		return 2;
	}
	
	private static int putFloats(float[] buffer, int startIndex, Vector3d v){
		buffer[startIndex  ] = v.getX();
		buffer[startIndex+1] = v.getY();
		buffer[startIndex+2] = v.getZ();
		return 3;
	}
	
	
	//============================================
	// CREATE FROM OBJDATA
	//============================================
	
	/**
	 * Construct a SerializedMesh from an ObjData.
	 * 
	 * Assumes that faces are flat and convex, and in CCW
	 * order with respect to outward-pointing normal.
	 * 
	 * Ignores faces with fewer than 3 vertices for solid data,
	 * but includes them for wireframe data.
	 * 
	 * @param objData An {@link ObjData} with geometry to be serialized.
	 * 
	 * @return A {@link SerializedMesh}
	 * @see {@link SerializedMesh#getFromObjData(ObjData, boolean, boolean)}
	 */
	public static SerializedMesh getFromObjData(ObjData objData){
		
		SerializedMesh smesh = new SerializedMesh(true, true, 3);
		
		putSolidObjData(smesh, objData);
		putTangentObjData(smesh, objData);
		putWireframeObjData(smesh, objData);
		
		return smesh;
	}
	
	/**
	 * Same as {@link SerializedMesh#getFromObjData(ObjData)} but with options
	 * to skip writing tangent and/or wireframe data.
	 * 
	 * @param objData An {@link ObjData} with geometry to be serialized.
	 * 
	 * @return A {@link SerializedMesh}
	 */
	public static SerializedMesh getFromObjData(ObjData objData, boolean writeTangentData, boolean writeWireframeData){
		
		SerializedMesh smesh = new SerializedMesh(writeTangentData, writeWireframeData, 3);
		
		putSolidObjData(smesh, objData);
		if(writeTangentData){ putTangentObjData(smesh, objData); }
		if(writeWireframeData){ putWireframeObjData(smesh, objData); }
		
		return smesh;
	}
	
	private static void putSolidObjData(SerializedMesh smesh, ObjData objData){
		
		//get tricount and allocate data
		smesh.triCount = 0;
		for(VertexData[] vd : objData.faces){
			if(vd.length < 3){ continue; }
			smesh.triCount += vd.length-2;
		}
		
		smesh.position = new float[9*smesh.triCount];	int kp = 0;
		smesh.tex = new float[6*smesh.triCount];		int kt = 0;
		smesh.normal = new float[9*smesh.triCount];		int kn = 0;
		
		//put data
		for(VertexData[] vd : objData.faces){
			if(vd.length < 3){ continue; }

			//put triangle 0 i (i+1) data, for 1 <= i < n-1
			for(int i=1; i<vd.length-1; i++){
				kp += putFloats(smesh.position, kp, objData.position[vd[0  ].posIndex]);
				kp += putFloats(smesh.position, kp, objData.position[vd[i  ].posIndex]);
				kp += putFloats(smesh.position, kp, objData.position[vd[i+1].posIndex]);
				
				if(objData.hasNormal){
					kn += putFloats(smesh.normal, kn, objData.normal[vd[0  ].normalIndex]);
					kn += putFloats(smesh.normal, kn, objData.normal[vd[i  ].normalIndex]);
					kn += putFloats(smesh.normal, kn, objData.normal[vd[i+1].normalIndex]);
				}else{
					smesh.normal[kn++] = 1; smesh.normal[kn++] = 0; smesh.normal[kn++] = 0;
					smesh.normal[kn++] = 1; smesh.normal[kn++] = 0; smesh.normal[kn++] = 0;
					smesh.normal[kn++] = 1; smesh.normal[kn++] = 0; smesh.normal[kn++] = 0;
				}

				if(objData.hasTex){
					kt += putFloats(smesh.tex, kt, objData.tex[vd[0  ].texIndex]);
					kt += putFloats(smesh.tex, kt, objData.tex[vd[i  ].texIndex]);
					kt += putFloats(smesh.tex, kt, objData.tex[vd[i+1].texIndex]);	
				}else{
					smesh.tex[kt++] = 1; smesh.tex[kt++] = 0;
					smesh.tex[kt++] = 1; smesh.tex[kt++] = 0;
					smesh.tex[kt++] = 1; smesh.tex[kt++] = 0;
				}
			}
		}
	}
	
	private static void putTangentObjData(SerializedMesh smesh, ObjData objData){
		
		//Assumes putSolidObjData has already been called, so triCount has been found.
		
		smesh.tangentS = new float[9*smesh.triCount];	int kts = 0;
		smesh.tangentT = new float[9*smesh.triCount];	int ktt = 0;
		
		//put data
		for(VertexData[] vd : objData.faces){
			if(vd.length < 3){ continue; }

			//find tangents
            //(tangentS is the vector which, when travelled along, would increase
            // the S tex coord, but not the T; similarly for tangentT)
            Vector3d tangentS, tangentT;
            
            //tangent vectors, not aligned to texture
            Vector3d tAmbA = new Vector3d(objData.position[vd[1].posIndex]); tAmbA.subtract(objData.position[vd[0].posIndex]);
            Vector3d tAmbB = new Vector3d(objData.position[vd[2].posIndex]); tAmbB.subtract(objData.position[vd[0].posIndex]);
            //texture space tangent vectors, corresponding to tA and tB
            Vector2d tTexA = new Vector2d(objData.tex[vd[1].texIndex]); tTexA.subtract(objData.tex[vd[0].texIndex]);
            Vector2d tTexB = new Vector2d(objData.tex[vd[2].texIndex]); tTexB.subtract(objData.tex[vd[0].texIndex]);
            
            //if M is the linear trans taking [u][v] \mapsto u*tTexA+v*tTexB,
            //then we want e.g. M^{-1} [1][0] for tangentS; this will be in the
            //(tAmbA, tAmbB) basis, so take the appropriate linear combination
            float invDet = 1.0f / (tTexA.getX()*tTexB.getY() - tTexA.getY()*tTexB.getX());
            tangentS = Vector3d.linearCombination(tTexB.getY(), tAmbA,  -tTexA.getY(), tAmbB);
            tangentT = Vector3d.linearCombination(-tTexB.getX(), tAmbA, tTexA.getX(),  tAmbB);
            tangentS.scale(invDet);
            tangentT.scale(invDet);
			
			//put triangle 0 i (i+1) data, for 1 <= i < n-1
			for(int i=1; i<vd.length-1; i++){
				
				kts += putFloats(smesh.tangentS, kts, tangentS);
				kts += putFloats(smesh.tangentS, kts, tangentS);
				kts += putFloats(smesh.tangentS, kts, tangentS);
				
				ktt += putFloats(smesh.tangentT, ktt, tangentT);
				ktt += putFloats(smesh.tangentT, ktt, tangentT);
				ktt += putFloats(smesh.tangentT, ktt, tangentT);
			}
		}
	}
	
	private static void putWireframeObjData(SerializedMesh smesh, ObjData objData){
		
		//get edge count and allocate wireframe data
		smesh.edgeCount = 0;
		for(VertexData[] vd : objData.faces){
			smesh.edgeCount += vd.length;
		}
		smesh.positionWireframe = new float[6*smesh.edgeCount];	int kpw = 0;
		
		//put data
		for(VertexData[] vd : objData.faces){
			
			VertexData start = vd[0], end;
			for(int i=1; i<vd.length; i++){
				end = vd[i];
				kpw += putFloats(smesh.positionWireframe, kpw, objData.position[start.posIndex]);
				kpw += putFloats(smesh.positionWireframe, kpw, objData.position[end.posIndex]);
				start = end;
			}
			end = vd[0];
			kpw += putFloats(smesh.positionWireframe, kpw, objData.position[start.posIndex]);
			kpw += putFloats(smesh.positionWireframe, kpw, objData.position[end.posIndex]);
		}
	}
	
	
	//============================================
	// CREATE FROM POLYHEDRON
	//============================================
	
	/**
	 * Construct a SerializedMesh from a {@link Collection} of {@link PolyhedronFaceTriangulated} objects.
	 * 
	 * @return A {@link SerializedMesh}
	 */
	public static
		<PolyVertex extends PolyhedronVertex<PolyVertex, PolyEdge, PolyFace>,
		 PolyEdge extends PolyhedronEdge<PolyVertex, PolyEdge, PolyFace>,
		 PolyFace extends PolyhedronFaceTriangulated<PolyVertex, PolyEdge, PolyFace>>
	
		SerializedMesh getFromPolyhedronFaces(Collection<PolyFace> faces){
		
		SerializedMesh smesh = new SerializedMesh(true, true, 3);

		putSolidPolyhedronData(smesh, faces);
		putTangentPolyhedronData(smesh, faces);
		putWireframePolyhedronData(smesh, faces);
		
		return smesh;
	}
		
	private static 
		<PolyVertex extends PolyhedronVertex<PolyVertex, PolyEdge, PolyFace>,
		 PolyEdge extends PolyhedronEdge<PolyVertex, PolyEdge, PolyFace>,
		 PolyFace extends PolyhedronFaceTriangulated<PolyVertex, PolyEdge, PolyFace>>
	
		void putSolidPolyhedronData(SerializedMesh smesh, Collection<PolyFace> faces){
		
		//get tricount and allocate data
		smesh.triCount = 0;
		for(PolyFace face : faces){
			smesh.triCount += face.getTriangles().size();
		}
		
		smesh.position = new float[9*smesh.triCount];	int kp = 0;
		smesh.tex = new float[6*smesh.triCount];		int kt = 0;
		smesh.normal = new float[9*smesh.triCount];		int kn = 0;
		
		//put data
		for(PolyFace face : faces){
			
			Vector3d normal = face.getPlane().getNormal();
			
			for(Triangle2d<EmbeddedVertex<PolyVertex,PolyEdge>> tri : face.getTriangles()){
			for(int i=0; i<3; i++){
				EmbeddedVertex<PolyVertex,PolyEdge> ev = tri.getVertex(i);
				
				/*Vector3d normal = new Vector3d(0,0,0);
				for(DebugFace fv : ev.getGlobalVertex().getIncidentFaces()){
					normal.add(fv.getPlane().getNormal());
				}
				normal.normalize();*/

				kp += putFloats(smesh.position, kp, ev.getGlobalVertex().getPosition());
				
				//scaled texture data
				Vector2d pos2d = ev.getPosition().copy();
				pos2d.scale(0.2f);
				kt += putFloats(smesh.tex, kt, pos2d);
				
				kn += putFloats(smesh.normal, kn, normal);
			}}
		}	
	}
	
	private static 
		<PolyVertex extends PolyhedronVertex<PolyVertex, PolyEdge, PolyFace>,
		 PolyEdge extends PolyhedronEdge<PolyVertex, PolyEdge, PolyFace>,
		 PolyFace extends PolyhedronFaceTriangulated<PolyVertex, PolyEdge, PolyFace>>
	
		void putTangentPolyhedronData(SerializedMesh smesh, Collection<PolyFace> faces){

		//Assumes putSolidPolyhedronData has already been called, so triCount has been found.
		
		smesh.tangentS = new float[9*smesh.triCount];	int kts = 0;
		smesh.tangentT = new float[9*smesh.triCount];	int ktt = 0;
		
		//put data
		for(PolyFace face : faces){
			Vector3d tangentS = face.getPlane().getXAxisVector();
			Vector3d tangentT = face.getPlane().getYAxisVector();
			
			int nfacetris = face.getTriangles().size();
			for(int t=0; t<nfacetris; t++){
			for(int i=0; i<3; i++){
				kts += putFloats(smesh.tangentS, kts, tangentS);
				ktt += putFloats(smesh.tangentT, ktt, tangentT);
			}}
		}
	}
	
	private static 
		<PolyVertex extends PolyhedronVertex<PolyVertex, PolyEdge, PolyFace>,
		 PolyEdge extends PolyhedronEdge<PolyVertex, PolyEdge, PolyFace>,
		 PolyFace extends PolyhedronFaceTriangulated<PolyVertex, PolyEdge, PolyFace>>
	
		void putWireframePolyhedronData(SerializedMesh smesh, Collection<PolyFace> faces){

		//get edge count and allocate data
		smesh.edgeCount = 0;
		for(PolyFace face : faces){
			smesh.edgeCount += face.getEdges().size();
		}
		smesh.positionWireframe = new float[6*smesh.edgeCount];	int ke = 0;
		
		//put data
		for(PolyFace face : faces){
		for(EmbeddedEdge<PolyVertex,PolyEdge> edge : face.getEdges()){
			
			Vector3d v0 = edge.getInitialVertex().getGlobalVertex().getPosition();
			Vector3d v1 = edge.getFinalVertex().getGlobalVertex().getPosition();
			
			ke += putFloats(smesh.positionWireframe, ke, v0);
			ke += putFloats(smesh.positionWireframe, ke, v1);
		}}
	}
}
