package engine.toolsAWT;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import geometry.common.MessageOutput;
import geometry.math.Vector4d;

/**
 * A 32 bpp image stored in a ByteBuffer in BGRA format.
 */
public class BufferTexture{
	
	private static final int PACK_ALIGNMENT_R_OFFSET = 2;
	private static final int PACK_ALIGNMENT_G_OFFSET = 1;
	private static final int PACK_ALIGNMENT_B_OFFSET = 0;
	private static final int PACK_ALIGNMENT_A_OFFSET = 3;
	
	private ByteBuffer data;
	private int bufferPixelCapacity;
	
	private int width, height;
	
	/**
	 * Create a {@link BufferTexture} with the specified pixel capacity.
	 * 
	 * @param bufferPixelCapacity The number of pixels allocated for this BufferTexture.
	 */
	public BufferTexture(int bufferPixelCapacity){
		
		this.width = 0;
		this.height = 0;
		
		this.bufferPixelCapacity = bufferPixelCapacity;
		allocate();
	}
	
	/**
	 * Ensure that the ByteBuffer can store bufferPixelCapacity pixels.
	 */
	private void allocate(){
		data = ByteBuffer.allocate(4 * bufferPixelCapacity);
	}
	
	/**
	 * Get the pixel width of the image currently stored in this Buffer.
	 */
	public int getWidth(){ return width; }
	
	/**
	 * Get the pixel height of the image currently stored in this Buffer.
	 */
	public int getHeight(){ return height; }
	
	/**
	 * Get the pixel capacity of the ByteBuffer that has been allocated.
	 */
	public int getBufferPixelCapacity(){ return bufferPixelCapacity; }
	
	/**
	 * Returns the ByteBuffer that can be written to to store an image
	 * in this BufferTexture.  If the ByteBuffer is not big enough
	 * to store the image, it will be re-allocated.
	 * 
	 * @param width The width of the image that will be stored in the buffer.
	 * @param height The height of the image that will be stored in the buffer.
	 * @return The ByteBuffer that can be used to store the image.
	 */
	public ByteBuffer getBufferForWriting(int width, int height){
		
		this.width = width;
		this.height = height;
		int requiredPixelCapacity = width*height;
		
		if(requiredPixelCapacity > bufferPixelCapacity){
			MessageOutput.printWarning("Re-allocating BufferTexture; pixel capacity too small (cur: " + bufferPixelCapacity + ", req: " + requiredPixelCapacity + ")");
			bufferPixelCapacity = requiredPixelCapacity;
			allocate();
		}
		
		return data; 	
	}
	
	/**
	 * Get the byte offset for the first byte of the pixel with offset
	 * (x,y) of the image currently stored in this BufferTexture.
	 * 
	 * @param x The pixel x coordinate.
	 * @param y The pixel y coordinate.
	 */
	private int getBufferPos(int x, int y){
		return 4*(y*width+x);
	}
	
	/**
	 * Returns the R component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getR(int x, int y){
		int v = (int)data.get(getBufferPos(x,y)+PACK_ALIGNMENT_R_OFFSET);
		return (v < 0) ? (v + 256) : v;
	}
	
	/**
	 * Returns the G component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getG(int x, int y){
		int v = (int)data.get(getBufferPos(x,y)+PACK_ALIGNMENT_G_OFFSET);
		return (v < 0) ? (v + 256) : v;
	}
	
	/**
	 * Returns the B component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getB(int x, int y){
		int v = (int)data.get(getBufferPos(x,y)+PACK_ALIGNMENT_B_OFFSET);
		return (v < 0) ? (v + 256) : v;
	}
	
	/**
	 * Returns the A component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getA(int x, int y){
		int v = (int)data.get(getBufferPos(x,y)+PACK_ALIGNMENT_A_OFFSET);
		return (v < 0) ? (v + 256) : v;
	}
	
	/**
	 * Returns the RGBA color of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public void getColorv(Vector4d target, int x, int y){
		
		int bufPos = getBufferPos(x,y);
		int r = (int)data.get(bufPos+PACK_ALIGNMENT_R_OFFSET); if(r < 0){ r += 256; }
		int g = (int)data.get(bufPos+PACK_ALIGNMENT_G_OFFSET); if(g < 0){ g += 256; }
		int b = (int)data.get(bufPos+PACK_ALIGNMENT_B_OFFSET); if(b < 0){ b += 256; }
		int a = (int)data.get(bufPos+PACK_ALIGNMENT_A_OFFSET); if(a < 0){ a += 256; }
		
		target.setX(r); target.setY(g); target.setZ(b); target.setW(a);
	}
	
	/**
	 * Returns a {@link Color} holding the color of the pixel with offset (x,y)
	 * of the image currently stored in this BufferTexture.
	 */
	public Color getColor(int x, int y){
		
		int bufPos = getBufferPos(x,y);
		int r = (int)data.get(bufPos+PACK_ALIGNMENT_R_OFFSET); if(r < 0){ r += 256; }
		int g = (int)data.get(bufPos+PACK_ALIGNMENT_G_OFFSET); if(g < 0){ g += 256; }
		int b = (int)data.get(bufPos+PACK_ALIGNMENT_B_OFFSET); if(b < 0){ b += 256; }
		int a = (int)data.get(bufPos+PACK_ALIGNMENT_A_OFFSET); if(a < 0){ a += 256; }
		
		return new Color(r,g,b,a);
	}
	
	//===========================================
	// BUFFERED IMAGE CONVERSION
	//===========================================
	
	/*private BufferedImage rescale(BufferedImage srcImg, float scaleX, float scaleY){
		
		int sw = (int)(scaleX*(float)srcImg.getWidth());
		int sh = (int)(scaleY*(float)srcImg.getHeight()); 
		
		//paint new image
		BufferedImage newImg = new BufferedImage(sw, sh, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = newImg.createGraphics(); 
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(srcImg, 0, 0, sw, sh, null);
		g.dispose();
		return newImg;
	}
	
	private BufferedImage rescaleNice(BufferedImage srcImg, float scaleX, float scaleY){
		
		BufferedImage intermediateImg = srcImg;
		
		//keep rescaling, but never by a factor less than 0.5 in either direction
		while((scaleX < 0.5f) || (scaleY < 0.5f)){
			float tempScaleX = 1.0f, tempScaleY = 1.0f;
			if(scaleX < 0.5f){ tempScaleX = 0.5f; scaleX *= 2.0f; }
			if(scaleY < 0.5f){ tempScaleY = 0.5f; scaleY *= 2.0f; }
			
			intermediateImg = rescale(intermediateImg, tempScaleX, tempScaleY);
		}
		
		//rescale the rest of the way (if necessary)
		if((scaleX < 1.0f) || (scaleY < 1.0f)){
			intermediateImg = rescale(intermediateImg, scaleX, scaleY);
		}
		
		//return
		return intermediateImg;
	}
	
	private BufferedImage rescaleNice(BufferedImage srcImg, int maxWidth, int maxHeight){
		
		//find rescaling factor (biggest s still satisfying bounds)
		float sx = (width <= maxWidth) ? 1.0f : (float)maxWidth/(float)width; 
		float sy = (height <= maxHeight) ? 1.0f : (float)maxHeight/(float)height;
		float s = (sx < sy) ? sx : sy;
		
		return rescaleNice(srcImg, s, s);
	}*/
	
	/**
	 * Create a new {@link BufferedImage} using the data from the image 
	 * currently stored in this BufferTexture.  It will be resized
	 * so that the width and height do not exceed the specified values.
	 */
	public BufferedImage getAsBufferedImage(){ //int maxWidth, int maxHeight){
		
		//this can probably be done more efficiently (see e.g. Raster.createWritableRaster)
		BufferedImage bufImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		//Vector4d cv = new Vector4d();
		
		for(int i=0; i<width; i++){
		for(int j=0; j<height; j++){
			Color c = getColor(i,j);
			bufImg.setRGB(i, height-1-j, c.getRGB());
			//getColorv(cv, i,j);
			//Color c = new Color((int)cv.getX(), (int)cv.getY(), (int)cv.getZ());
			//bufImg.setRGB(i, j, c.getRGB());
		}}
		
		return bufImg; //rescaleNice(bufImg, maxWidth, maxHeight);
	}
}
