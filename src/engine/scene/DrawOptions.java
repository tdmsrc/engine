package engine.scene;

import geometry.math.Vector3d;

//standard options for all objects

/**
 * Options for each {@link SceneObject}.  
 * Boolean options are specified as bitmasks:
 * <ul>
 * <li>DrawOptions.MASK_SOLID_VISIBLE
 * <li>DrawOptions.MASK_CASTS_SHADOWS
 * <li>DrawOptions.MASK_WIREFRAME_VISIBLE
 * <li>DrawOptions.MASK_TRANSLUCENT
 * <li>DrawOptions.MASK_EMISSIVE
 * <li>DrawOptions.MASK_LIGHTING
 * <li>DrawOptions.MASK_COLLIDER
 * <li>DrawOptions.MASK_PICKABLE
 * <li>DrawOptions.MASK_DEBUG
 * </ul>
 * Objects handled during a {@link Scene} traversal are filtered by these options using a bitmask.
 * Set or query these options via {@link #setFlags(int, boolean)} and {@link #checkFlags(int, int)}.
 */
public class DrawOptions{
	
	//[TODO] should be moved out of *draw* options and into a *scene* options thing
	public static final int 
		MASK_SOLID_VISIBLE		= 1 << 0, //draw in z-prepass and light passes
		MASK_CASTS_SHADOWS		= 1 << 1, //draw in shadow maps
		MASK_WIREFRAME_VISIBLE	= 1 << 2, //draw wireframe
		MASK_TRANSLUCENT		= 1 << 3, //use alpha texture values as translucency
		MASK_EMISSIVE			= 1 << 4, //use emissive light properties
		MASK_LIGHTING			= 1 << 5, //lit by non-emissive light
		MASK_COLLIDER			= 1 << 6, //[TODO]can collide with this object
		MASK_PICKABLE			= 1 << 7, //[TODO]can pick this object
		MASK_DEBUG				= 1 << 8; //[TODO]used only for debugging/editing/etc
	
	//bitmask indicating flags defined above
	private int optionsBitmask;
	//translucency options
	private float opacity;
	//wireframe options
	private Vector3d wireframeColor;
	//emissive lighting options
	private Vector3d emissiveColor;
	private float emissiveCoefficient;
	
	
	public DrawOptions(){
		
		//set defaults
		optionsBitmask = MASK_SOLID_VISIBLE | MASK_CASTS_SHADOWS | MASK_LIGHTING;
		
		opacity = 1.0f;
		
		wireframeColor = new Vector3d(1.0f, 1.0f, 1.0f);
		
		emissiveColor = new Vector3d(1.0f, 1.0f, 1.0f);
		emissiveCoefficient = 1.0f;
	}
	
	/**
	 * Example: Ensure that SOLID_VISIBLE is enabled and TRANSLUCENT is disabled:
	 * <pre>{@code 
	 * checkFlags(MASK_SOLID_VISIBLE | MASK_TRANSLUCENT, MASK_SOLID_VISIBLE);
	 * }</pre>
	 * 
	 * @return True iff <ul><li>each option in both queryBitmask and valueBitmask is enabled<li>each option
	 * in queryBitmask but not in valueBitmask is disabled</ul>
	 */
	public boolean checkFlags(int queryBitmask, int valueBitmask){ return ((optionsBitmask & queryBitmask) == valueBitmask); }
	
	/**
	 * Example: Enable both SOLID_VISIBLE and CASTS_SHADOWS:
	 * <pre>{@code
	 * setFlags(MASK_SOLID_VISIBLE | MASK_CASTS_SHADOWS, true)
	 * }</pre>
	 */
	public void setFlags(int flagsBitmask, boolean value){
		if(value){ optionsBitmask |= flagsBitmask; }else{ optionsBitmask &= ~flagsBitmask; }
	}
	
	public Vector3d getWireframeColor(){ return wireframeColor; }
	public void setWireframeColor(Vector3d wireframeColor){ this.wireframeColor = wireframeColor; }
	
	public float getOpacity(){ return opacity; }
	public void setOpacity(float opacity){ this.opacity = opacity; }
	
	public Vector3d getEmissiveColor(){ return emissiveColor; }
	public void setEmissiveColor(Vector3d emissiveColor){ this.emissiveColor = emissiveColor; }
	
	public float getEmissiveCoefficient(){ return emissiveCoefficient; }
	public void setEmissiveCoefficient(float emissiveCoefficient){ this.emissiveCoefficient = emissiveCoefficient; }
}
