package engine.scene;



public abstract class SceneEntity{

	private String name;
	
	public SceneEntity(String name){
		setName(name);
	}
	
	public String getName(){ return name; } 
	public void setName(String name){
		this.name = name;
	}
}
