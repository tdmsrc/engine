package engine.scene;

import java.util.EnumMap;

import engine.renderer.GenericBuffer;
import engine.renderer.GenericBuffer.BufferConstructor;
import geometry.math.Vector3d;
import engine.renderer.GenericTexture;


public class Skybox<GL, 
	Buffer extends GenericBuffer<GL>, 
	Texture extends GenericTexture<GL>>{

	private static final int FACE_TRI_COUNT = 2;
	
	public static enum SkyboxFace{
		SKYBOX_FACE_FRONT,	SKYBOX_FACE_BACK, 
		SKYBOX_FACE_LEFT,	SKYBOX_FACE_RIGHT, 
		SKYBOX_FACE_TOP,	SKYBOX_FACE_BOTTOM
	}
	
	private Buffer vertexTexBuffer;
	
	private EnumMap<SkyboxFace,Buffer> vertexPositionBuffers;
	private EnumMap<SkyboxFace,Texture> skyboxTextures;
	
	
	public Skybox(GL gl, BufferConstructor<GL,Buffer> bufferConstructor, 
		boolean flipTexturesVertically,
		Texture texFront,	Texture texBack, 
		Texture texLeft,	Texture texRight, 
		Texture texTop,		Texture texBottom){
		
		skyboxTextures = new EnumMap<SkyboxFace,Texture>(SkyboxFace.class);
		
		skyboxTextures.put(SkyboxFace.SKYBOX_FACE_FRONT,	texFront);
		skyboxTextures.put(SkyboxFace.SKYBOX_FACE_BACK,		texBack);
		skyboxTextures.put(SkyboxFace.SKYBOX_FACE_LEFT,		texLeft);
		skyboxTextures.put(SkyboxFace.SKYBOX_FACE_RIGHT,	texRight);
		skyboxTextures.put(SkyboxFace.SKYBOX_FACE_TOP,		texTop);
		skyboxTextures.put(SkyboxFace.SKYBOX_FACE_BOTTOM,	texBottom);
		
		populateFloatBuffers(gl, bufferConstructor, flipTexturesVertically);
		
		initializeSun(gl, bufferConstructor);
	}
	
	/**
	 * Get the number of triangles in any of the vertex position buffers returned by either
	 * {@link #getPositionBuffer(SkyboxFace)} or {@link #getSunPositionBuffer()}.
	 */
	public int getTriCount(){ return FACE_TRI_COUNT; }
	
	public Texture getTexture(SkyboxFace face){ return skyboxTextures.get(face); }
	
	public Buffer getTexBuffer(){ return vertexTexBuffer; }
	public Buffer getPositionBuffer(SkyboxFace face){ return vertexPositionBuffers.get(face); }
	
	
	//======================================
	// SUN (LIGHT SCATTERING SOURCE)
	//======================================
	
	private boolean hasSun;
	private Texture textureSun;
	private Vector3d sunDirection;
	private Buffer vertexPositionBufferSun;
	
	private void initializeSun(GL gl, BufferConstructor<GL,Buffer> bufferConstructor){
		
		hasSun = false;
		textureSun = null;
		
		sunDirection = new Vector3d(0,1,0); 
		vertexPositionBufferSun = bufferConstructor.construct(gl, new float[]{ 
			 0.2f, 1.0f,-0.2f,	 0.2f, 1.0f, 0.2f,	-0.2f, 1.0f, 0.2f,
			-0.2f, 1.0f, 0.2f,	-0.2f, 1.0f,-0.2f,	 0.2f, 1.0f,-0.2f
		}, 3);
	}
	
	public boolean hasSun(){ return hasSun; }
	public Texture getSunTexture(){ return textureSun; }
	public Buffer getSunPositionBuffer(){ return vertexPositionBufferSun; }
	public Vector3d getSunDirection(){ return sunDirection; }
	
	public void setHasSun(boolean hasSun){ this.hasSun = hasSun; }
	public void setSunTexture(Texture textureSun){ this.textureSun = textureSun; }
	
	/**
	 * Sets the direction and size of the sun quad.  
	 * Updates the vertex position buffer accordingly. 
	 * 
	 * @param gl 
	 * @param sunDirection Direction of the sun in object coordinates.
	 * @param sunSize Side length of the sun quad.  A length of 2 corresponds to the size
	 * of one face of the skybox cube. 
	 */
	public void setSunDirection(GL gl, BufferConstructor<GL,Buffer> bufferConstructor, Vector3d sunDirection, float sunSize){
		this.sunDirection.setEqualTo(sunDirection);
		
		//update position buffer
		Vector3d n = sunDirection.copy(); n.normalize();
		Vector3d u = sunDirection.getOrthogonalVector(); 
		Vector3d v = u.cross(n);
		
		u.scale(0.1f);
		v.scale(0.1f);
		
		Vector3d p0 = n.copy(); p0.subtract(u); p0.subtract(v);
		Vector3d p1 = n.copy(); p1.add(u); p1.subtract(v);
		Vector3d p2 = n.copy(); p2.add(u); p2.add(v);
		Vector3d p3 = n.copy(); p3.subtract(u); p3.add(v);
		
		//vertexPositionBufferSun.delete(gl);
		vertexPositionBufferSun = bufferConstructor.construct(gl, new float[]{ 
			p0.getX(),p0.getY(),p0.getZ(),  p1.getX(),p1.getY(),p1.getZ(),  p2.getX(),p2.getY(),p2.getZ(),
			p2.getX(),p2.getY(),p2.getZ(),  p3.getX(),p3.getY(),p3.getZ(),  p0.getX(),p0.getY(),p0.getZ()
		}, 3);
	}
	
	//======================================
	// METHODS TO GENERATE FACE GEOMETRY
	//======================================
	
	private void populateFloatBuffers(GL gl, BufferConstructor<GL,Buffer> bufferConstructor, boolean flipTexturesVertically){
		
		vertexTexBuffer = bufferConstructor.construct(gl, new float[]{
			0.0f, flipTexturesVertically ? 1.0f : 0.0f,
			1.0f, flipTexturesVertically ? 1.0f : 0.0f,
			1.0f, flipTexturesVertically ? 0.0f : 1.0f,
			
			1.0f, flipTexturesVertically ? 0.0f : 1.0f,
			0.0f, flipTexturesVertically ? 0.0f : 1.0f,
			0.0f, flipTexturesVertically ? 1.0f : 0.0f,
		}, 2);
		
		vertexPositionBuffers = new EnumMap<SkyboxFace,Buffer>(SkyboxFace.class);
		
		//F: --- +-- ++- -+-
		vertexPositionBuffers.put(SkyboxFace.SKYBOX_FACE_FRONT, bufferConstructor.construct(gl, new float[]{ 
			-1.0f,-1.0f,-1.0f,	 1.0f,-1.0f,-1.0f,	 1.0f, 1.0f,-1.0f,
			 1.0f, 1.0f,-1.0f,	-1.0f, 1.0f,-1.0f,	-1.0f,-1.0f,-1.0f
		}, 3));
		
		//B: +-+ --+ -++ +++
		vertexPositionBuffers.put(SkyboxFace.SKYBOX_FACE_BACK, bufferConstructor.construct(gl, new float[]{ 
			 1.0f,-1.0f, 1.0f,	-1.0f,-1.0f, 1.0f,	-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,	 1.0f, 1.0f, 1.0f,	 1.0f,-1.0f, 1.0f
		}, 3));
		
		//L: --+ --- -+- -++
		vertexPositionBuffers.put(SkyboxFace.SKYBOX_FACE_LEFT, bufferConstructor.construct(gl, new float[]{ 
			-1.0f,-1.0f, 1.0f,	-1.0f,-1.0f,-1.0f,	-1.0f, 1.0f,-1.0f,
			-1.0f, 1.0f,-1.0f,	-1.0f, 1.0f, 1.0f,	-1.0f,-1.0f, 1.0f
		}, 3));
		
		//R: +-- +-+ +++ ++-
		vertexPositionBuffers.put(SkyboxFace.SKYBOX_FACE_RIGHT, bufferConstructor.construct(gl, new float[]{ 
			 1.0f,-1.0f,-1.0f,	 1.0f,-1.0f, 1.0f,	 1.0f, 1.0f, 1.0f,
			 1.0f, 1.0f, 1.0f,	 1.0f, 1.0f,-1.0f,	 1.0f,-1.0f,-1.0f
		}, 3));
		
		//U: ++- +++ -++ -+-
		vertexPositionBuffers.put(SkyboxFace.SKYBOX_FACE_TOP, bufferConstructor.construct(gl, new float[]{ 
			 1.0f, 1.0f,-1.0f,	 1.0f, 1.0f, 1.0f,	-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,	-1.0f, 1.0f,-1.0f,	 1.0f, 1.0f,-1.0f
		}, 3));

		//D: --- --+ +-+ +-- 
		vertexPositionBuffers.put(SkyboxFace.SKYBOX_FACE_BOTTOM, bufferConstructor.construct(gl, new float[]{ 
			-1.0f,-1.0f,-1.0f,	-1.0f,-1.0f, 1.0f,	 1.0f,-1.0f, 1.0f,
			 1.0f,-1.0f, 1.0f,	 1.0f,-1.0f,-1.0f,	-1.0f,-1.0f,-1.0f
		}, 3));
	}
}
