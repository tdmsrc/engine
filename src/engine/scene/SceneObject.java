package engine.scene;

import engine.collision.BoundingBox;
import engine.collision.Collider;
import engine.collision.CollisionData;
import engine.collision.Ellipsoid;
import engine.collision.PickData;
import engine.renderer.Drawable;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.renderer.Material;
import geometry.math.ObjectTransformation3d;
import geometry.math.PhysicalState3d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;
import geometry.spacepartition.Culler.BoxContainment;

//a DrawableObject and ObjectTransformation3d pair
//the matrix represents transform from object's coords -> world coords
//note that a single drawable object can be used in many worldobjects

public class SceneObject<GL, 
	Buffer extends GenericBuffer<GL>, 
	Texture extends GenericTexture<GL>> 

	extends SceneEntity
{
	
	//SceneOctree
	private SceneOctree<GL,Buffer,Texture> owningNode;
	private int owningNodeIndex; //<- position in owningNode array, for fast removal
	
	//object data, bounding box, and collider
	private Drawable<GL,Buffer> drawable;
	private BoundingBox boundingBox; //need getReverseContainment for placement in the scene octree 
	private Collider<Vector3d> collider; //for collision
	
	//draw options
	private DrawOptions drawOptions;
	private Material<Texture> material;
	
	//transformation
	private boolean isTransformed; 
	private boolean isBillboard;
	private ObjectTransformation3d objTrans;
	
	
	public SceneObject(String name, 
		Drawable<GL,Buffer> drawable, BoundingBox boundingBox, 
		DrawOptions drawOptions, Material<Texture> material){
		super(name);
		
		owningNode = null;
		owningNodeIndex = 0;
		
		this.drawable = drawable;
		this.boundingBox = boundingBox;
		collider = null;
		
		this.drawOptions = drawOptions;
		this.material = material;
		isBillboard = false;
		
		isTransformed = false;
		objTrans = new ObjectTransformation3d();
	}
	
	//getters
	public Drawable<GL,Buffer> getDrawable(){ return drawable; }
	public DrawOptions getDrawOptions(){ return drawOptions; }
	
	//material
	public void setMaterial(Material<Texture> material){ this.material = material; }
	public Material<Texture> getMaterial(){ return material; }
	
	//transformation
	public boolean isTransformed(){ return isTransformed; }
	public void setIsTransformed(boolean isTransformed){ this.isTransformed = isTransformed; }
	
	/**
	 * Get the transformation applied to this object.  
	 * <br><br>
	 * Note that if the transformation is modified, a call to {@link #updateTreePosition()}
	 * may be needed.
	 */
	public ObjectTransformation3d getObjectTransformation(){ return objTrans; } 
	
	/**
	 * Set the object's transformation.  This can be used to set the 
	 * transformation to a {@link PhysicalState3d} object.
	 * To <i>modify</i> the existing transformation, use {@link #getObjectTransformation()} 
	 * and then use the methods of {@link ObjectTransformation3d}.
	 * <br><br>
	 * Note that if the transformation is modified, a call to {@link #updateTreePosition()}
	 * may be needed.
	 */
	public void setObjectTransformation(ObjectTransformation3d objTrans){ this.objTrans = objTrans; }
	
	//billboard
	public boolean isBillboard(){ return isBillboard; }
	public void setBillboard(boolean isBillboard){ this.isBillboard = isBillboard; }
	
	//bounding box and collider
	public void setBoundingBox(BoundingBox boundingBox){ this.boundingBox = boundingBox; }
	public BoundingBox getBoundingBox(){ return boundingBox; } 
	
	public void setCollider(Collider<Vector3d> collider){ this.collider = collider; }

	
	//===========================================
	// OCTREE NODE PLACEMENT/UPDATING
	//===========================================
	
	public boolean isContainedCompletely(Box<Vector3d> box){
		
		return (isTransformed ?
			boundingBox.getReverseContainment(box, objTrans) :
			box.getBoxContainment(boundingBox)) == BoxContainment.CONTAINMENT_COMPLETE;
	}
	
	public SceneOctree<GL,Buffer,Texture> getOwningNode(){ return owningNode; }	
	
	public void setOwningNode(SceneOctree<GL,Buffer,Texture> owningNode, int owningNodeIndex){ 
		this.owningNode = owningNode;
		this.owningNodeIndex = owningNodeIndex;
	}
	
	//public float getOctreePlacementBadness(){
	//	return owningNode.getCullBounds().computeVolume() - boundingBox.computeVolume();
	//}
	
	public void removeFromScene(){
		if(owningNode == null){ return; }
		
		owningNode.remove(owningNodeIndex);
		this.owningNode = null;
	}
	
	public void updateTreePosition(){
		if(owningNode == null){ return; }
		
		//start with newNode = owningNode
		SceneOctree<GL,Buffer,Texture> newNode = owningNode;
		
		//remove from current node
		owningNode.remove(owningNodeIndex);
		owningNode = null;
		
		//keep ascending tree, starting at original owningNode, until object is contained
		while(!isContainedCompletely(newNode.getCullBounds())){	
			newNode = newNode.getParent();
			if(newNode == null){ return; } //[TODO] out of tree bounds
		}
		
		//descend from newNode into containing children until no longer possible
		newNode.addObject(this);
	}
	
	
	//===========================================
	// COLLISION AND PICKING
	//===========================================
	/**
	 * Calls the {@link Collider#pick} method on the {@link Collider} associated to this object,
	 * either providing the object's {@link SceneObjectTransformation} or not, depending on whether
	 * or not the object is marked as transformed.
	 */
	public boolean pick(PickData<Vector3d> pickData, 
		Vector3d p, Vector3d ray, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
			
		return isTransformed ?
			collider.pick(pickData, objTrans, p, ray, maxPickDistance, rayCuller) :
			collider.pick(pickData, p, ray, maxPickDistance, rayCuller);
	}
	
	/**
	 * Calls the {@link Collider#collide} method on the {@link Collider} associated to this object,
	 * either providing the object's {@link SceneObjectTransformation} or not, depending on whether
	 * or not the object is marked as transformed.
	 */
	public boolean collide(CollisionData<Vector3d> collisionData,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		return isTransformed ?
			collider.collide(collisionData, objTrans, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds) :
			collider.collide(collisionData, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
	}
}
