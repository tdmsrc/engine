package engine.scene;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.renderer.GenericBuffer.BufferConstructor;
import geometry.common.MessageOutput;
import geometry.math.Vector3d;
import geometry.polyhedron.components.EmbeddedVertex;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.polyhedron.components.PolyhedronFaceTriangulated;
import geometry.polyhedron.components.PolyhedronVertex;
import geometry.spacepartition.Box;
import geometry.spacepartition.OctreeChild;
import geometry.spacepartition.OctreeNodeCullable;

//octree with lazy creation of nodes:
//when an object is added, keep checking if it can be put into a child node, and if that
//node doesn't exist, create it and continue checking until a max depth/nodebounds is reached.

public class SceneOctree<GL,
	Buffer extends GenericBuffer<GL>,
	Texture extends GenericTexture<GL>>
	
	extends OctreeNodeCullable<SceneOctree<GL,Buffer,Texture>,Vector3d>{
	
	private int depth, maxDepth;
	private Box<Vector3d> nodeBounds;
	
	//children
	private boolean isLeaf;
	private SceneOctree<GL,Buffer,Texture> parent;
	private EnumMap<OctreeChild,SceneOctree<GL,Buffer,Texture>> children;
	
	//node contents
	private ArrayList<SceneObject<GL,Buffer,Texture>> objects;
	
	
	public SceneOctree(int depth, int maxDepth, Box<Vector3d> nodeBounds){
		
		this.depth = depth;
		this.maxDepth = maxDepth;
		
		this.nodeBounds = nodeBounds;
		
		//initialize as leaf
		isLeaf = true;
		parent = null;
		children = null;
		
		//prepare contents
		objects = new ArrayList<SceneObject<GL,Buffer,Texture>>();
	}
	
	//[TODO]
	public String debugBounds(){
		return nodeBounds.getMin() + " - " + nodeBounds.getMax();
	}
	
	//==========================================
	// BASIC METHODS, OCTREE NODE INTERFACE
	//==========================================

	@Override
	protected SceneOctree<GL,Buffer,Texture> getThisNode(){ return this; }
	
	@Override
	protected SceneOctree<GL,Buffer,Texture> getParent(){ return parent; }

	@Override
	protected boolean isLeaf(){ return isLeaf; }

	@Override
	protected SceneOctree<GL,Buffer,Texture> getChild(OctreeChild child){
		return children.get(child);
	}
	
	@Override
	protected Box<Vector3d> getCullBounds(){ return nodeBounds; }

	public List<SceneObject<GL,Buffer,Texture>> getObjects(){
		return objects;
	}
	
	//==========================================
	// OBJECT ARRAY DELETE/INSERT
	//==========================================
	
	//add to this
	private void add(SceneObject<GL,Buffer,Texture> object){
		
		object.setOwningNode(this, objects.size());
		objects.add(object);
	}
	
	private void addAll(Collection<SceneObject<GL,Buffer,Texture>> objects){
		
		for(SceneObject<GL,Buffer,Texture> object : objects){ add(object); }
	}
	
	//remove by putting last entry in ith slot, then removing last entry
	//(this avoids "shifting to the left")
	protected void remove(int i){
		
		//get last object and index
		int n = objects.size()-1;
		SceneObject<GL,Buffer,Texture> lastObj = objects.get(n);
		
		//put last object into ith slot
		lastObj.setOwningNode(this, i);
		objects.set(i, lastObj);
		
		//remove last object in list
		objects.remove(n);
	}
	
	//==========================================
	// ADD METHODS
	//==========================================
	
	//add object by descending through node bounding boxes
	public void addObject(SceneObject<GL,Buffer,Texture> object){ 
		
		//if this is a leaf at the max depth, just add to this list of objects
		if(depth >= maxDepth){
			add(object);
			return;
		}
		
		//otherwise, check children
		for(OctreeChild child : OctreeChild.values()){
			
			//get bounds for child
			Box<Vector3d> childBounds = (isLeaf || (children.get(child) == null)) ? 
				child.getSubBox(nodeBounds) : children.get(child).nodeBounds;
			
			//check containment of bounding box in child, applying transformation if necessary	
			if(object.isContainedCompletely(childBounds)){
				
				//if this is a leaf, make it not a leaf
				if(isLeaf){
					isLeaf = false;
					children = new EnumMap<OctreeChild,SceneOctree<GL,Buffer,Texture>>(OctreeChild.class);
				}
				
				//if there is no child in this slot, make one
				SceneOctree<GL,Buffer,Texture> childNode = children.get(child);
				if(childNode == null){
					childNode = new SceneOctree<GL,Buffer,Texture>(depth+1, maxDepth, childBounds);
					childNode.parent = this;
					children.put(child, childNode);
					MessageOutput.printDebug("Created octree node at depth " + (depth+1));
				}
				
				//recurse
				childNode.addObject(object);
				return;
			}
		}
		
		//at this point, object does not fit in any child, so add to this node's objects
		add(object);
	}
	
	
	/**
	 * Specific implementations of Polyhedron may have properties that affect the
	 * creation of a corresponding SceneObject; this is an interface that specifies
	 * how to create a SceneObject collection from a PolyhedronFaceTriangulated collection.
	 */
	public static interface FacesToObjectsMethod<GL,
		Buffer extends GenericBuffer<GL>,
		Texture extends GenericTexture<GL>,
		PolyVertex extends PolyhedronVertex<PolyVertex, PolyEdge, PolyFace>,
		PolyEdge extends PolyhedronEdge<PolyVertex, PolyEdge, PolyFace>,
		PolyFace extends PolyhedronFaceTriangulated<PolyVertex, PolyEdge, PolyFace>>{
		
		List<SceneObject<GL,Buffer,Texture>> createSceneObjects(GL gl, 
			List<PolyFace> faces, BufferConstructor<GL,Buffer> bufferConstructor);
	}
	
	/**
	 * Add a PolyhedronFaceTriangulated collection to this SceneOctree using the 
	 * specified {@link FacesToObjectsMethod}.
	 * <br>
	 * Note that the List "faces" will be modified by this method.
	 */
	public 
		<PolyVertex extends PolyhedronVertex<PolyVertex, PolyEdge, PolyFace>,
		 PolyEdge extends PolyhedronEdge<PolyVertex, PolyEdge, PolyFace>,
		 PolyFace extends PolyhedronFaceTriangulated<PolyVertex, PolyEdge, PolyFace>>
	
		void addPolyhedronFaces(GL gl, 
			List<PolyFace> faces, BufferConstructor<GL,Buffer> bufferConstructor,
			FacesToObjectsMethod<GL,Buffer,Texture,PolyVertex,PolyEdge,PolyFace> method){
		
		if(depth >= maxDepth){
			//faces cannot descend any further in the tree, so combine them and add to objects
			if(!faces.isEmpty()){
				addAll(method.createSceneObjects(gl, faces, bufferConstructor));
			}
			return;
		}
		
		//descent sublist of faces to children
		for(OctreeChild child : OctreeChild.values()){
			
			//get bounds for child
			Box<Vector3d> childBounds = (isLeaf || (children.get(child) == null)) ? 
				child.getSubBox(nodeBounds) : children.get(child).nodeBounds;
			
			//make a list of faces to descend to child
			List<PolyFace> childFaces = new LinkedList<PolyFace>();
			
			//loop through list of remaining faces; if a face is contained in the child
			//add to child list and remove from list of remaining faces
			ListIterator<PolyFace> it = faces.listIterator();
			while(it.hasNext()){
				PolyFace face = it.next();
				
				boolean contained = true;
				for(EmbeddedVertex<PolyVertex,PolyEdge> embVert : face.getVertices()){
					Vector3d p = embVert.getGlobalVertex().getPosition();
					if(!childBounds.containsPoint(p, 0.0f)){
						contained = false; 
						break;
					}
				}
				
				if(contained){
					it.remove();
					childFaces.add(face);
				}
			}
			
			//if there were contained faces, pass them on to the child
			if(!childFaces.isEmpty()){
				//if this is a leaf, make it not a leaf
				if(isLeaf){
					isLeaf = false;
					children = new EnumMap<OctreeChild,SceneOctree<GL,Buffer,Texture>>(OctreeChild.class);
				}
				//if there is no child in this slot, make one
				SceneOctree<GL,Buffer,Texture> childNode = children.get(child);
				if(childNode == null){
					childNode = new SceneOctree<GL,Buffer,Texture>(depth+1, maxDepth, childBounds);
					childNode.parent = this;
					children.put(child, childNode);
					MessageOutput.printDebug("Created octree node at depth " + (depth+1));
				}
				//pass faces to child
				childNode.addPolyhedronFaces(gl, childFaces, bufferConstructor, method);
			}
		}
		
		//"faces" now consists only of faces that don't fit in a child node
		//combine them using "method" and add them to the list of node objects
		if(!faces.isEmpty()){
			addAll(method.createSceneObjects(gl, faces, bufferConstructor));
		}
	}
}
