package engine.scene;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import engine.collision.CollisionEllipsoid;
import engine.collision.PickData;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;
import geometry.spacepartition.OctreeChild;
import geometry.spacepartition.OctreeNodeTraversal;
import geometry.spacepartition.QuadtreeChild;


public class Scene<GL,
	Buffer extends GenericBuffer<GL>,
	Texture extends GenericTexture<GL>>{
	
	//CollisionEllipsoids that interact with this scene; stored so that
	//when adding or removing a terrain, its ColliderTerrain list can be updated
	private HashSet<CollisionEllipsoid<GL>> collisionEllipsoids;
	
	//lights and terrains in this scene
	private LinkedList<Light> lights;
	private LinkedList<Terrain<GL,Buffer,Texture>> terrains;
	
	//octree of scene objects, along with dimensions of the root box
	private Box<Vector3d> octreeBounds;
	private Vector3d octreeDimensions;
	private SceneOctree<GL,Buffer,Texture> tree;
	
	//skybox
	private boolean hasSkybox;
	private Skybox<GL,Buffer,Texture> skybox;
	
	//renderer effects
	private SceneEffects fx;
	
	
	//temp containers for holding children in front-to-back order when doing cameraTraversal or pick
	private OctreeChild[] octreeChildOrder = new OctreeChild[8];
	private QuadtreeChild[] quadtreeChildOrder = new QuadtreeChild[4];
	
	
	public Scene(Vector3d octreeBoundsMin, Vector3d octreeBoundsMax){
		
		//prepare collision ellipsoid container
		collisionEllipsoids = new HashSet<CollisionEllipsoid<GL>>();
		
		//prepare light and terrain containers 
		lights = new LinkedList<Light>();
		terrains = new LinkedList<Terrain<GL,Buffer,Texture>>();
		
		//prepare object octree
		octreeBounds = new Box<Vector3d>(octreeBoundsMin, octreeBoundsMax);
		octreeDimensions = octreeBounds.getDimensions();
		tree = new SceneOctree<GL,Buffer,Texture>(0, 6, octreeBounds);
		
		//default skybox: none
		hasSkybox = false;
		
		//renderer effects
		fx = new SceneEffects();
	}
		
	public SceneOctree<GL,Buffer,Texture> getOctree(){ return tree; }
	
	public List<Terrain<GL,Buffer,Texture>> getTerrains(){ return terrains; }
	
	//[TODO] get lights visible ONLY from the specified camera?
	public LinkedList<Light> getLights(Camera camera){ return lights; }
	
	public SceneEffects getFX(){ return fx; }
	
	//SKYBOX
	//------------------------------------
	public void setSkybox(Skybox<GL,Buffer,Texture> skybox){
		this.skybox = skybox;
		hasSkybox = true;
	}
	
	public boolean hasSkybox(){
		return hasSkybox;
	}
	
	public void removeSkybox(){
		this.skybox = null;
		hasSkybox = false;
	}
	
	public Skybox<GL,Buffer,Texture> getSkybox(){
		return skybox;
	}
	
	//ADDERS
	//------------------------------------
	public void addObject(SceneObject<GL,Buffer,Texture> object){ 
		tree.addObject(object);
	}
	
	public void addLight(Light light){
		lights.add(light);
	}
	
	public void addTerrain(Terrain<GL,Buffer,Texture> terrain){
		terrains.add(terrain);
		
		//update any CollisionEllipsoids that refer to this scene
		for(CollisionEllipsoid<GL> collisionEllipsoid : collisionEllipsoids){
			collisionEllipsoid.updateTerrains();
		}
	}

	public void addCollisionEllipsoid(CollisionEllipsoid<GL> collisionEllipsoid){
		collisionEllipsoids.add(collisionEllipsoid);
	}
	
	public void removeCollisionEllipsoid(CollisionEllipsoid<GL> collisionEllipsoid){
		collisionEllipsoids.remove(collisionEllipsoid);
	}

	
	//========================================================
	// GEOMETRY TRAVERSAL FROM CAMERA
	//========================================================
	
	//traversal with camera as culler
	public void cameraTraversal(SceneTraversal<GL,Buffer,Texture> sceneTraversal, 
		Camera camera, int drawOptionsQueryBitmask, int drawOptionsValueBitmask){
		
		//sort octree children front-to-back, and project forward vector onto terrain plane
		Vector3d f = camera.getRotation().getForward();
		OctreeChild.sort(f, octreeDimensions, octreeChildOrder);
		Vector2d forward2d = new Vector2d(f.getX(), -f.getZ());
		
		//perform traversal of scene objects
		sceneTraversal.specifySceneObjectFilter(drawOptionsQueryBitmask, drawOptionsValueBitmask);
		tree.traverse(sceneTraversal, false, octreeChildOrder, camera);
		
		//perform traversal of terrains
		for(Terrain<GL,Buffer,Texture> terrain : terrains){
			if(!terrain.getDrawOptions().checkFlags(drawOptionsQueryBitmask, drawOptionsValueBitmask)){ continue; }
			
			//sort children for front-to-back traversal
			QuadtreeChild.sort(forward2d, terrain.getMetrics().getTerrainPosBounds().getDimensions(), quadtreeChildOrder);
			
			sceneTraversal.initializeTerrainTraversal(terrain, camera.getPosition(), terrain.getTileData());
			terrain.getQuadtree().traverse(sceneTraversal, true, quadtreeChildOrder, camera);
			sceneTraversal.finalizeTerrainTraversal(terrain);
		}
	}
	
	//just like cameraTraversal, but a light is also specified; traversal culled by light volume
	public void cameraLightPassTraversal(SceneTraversal<GL,Buffer,Texture> sceneTraversal, 
		Camera camera, Light light,//Culler<Vector3d> additionalCuller,
		int drawOptionsQueryBitmask, int drawOptionsValueBitmask){
		
		//sort octree children front-to-back, and project forward vector onto terrain plane
		Vector3d f = camera.getRotation().getForward();
		OctreeChild.sort(f, octreeDimensions, octreeChildOrder);
		Vector2d forward2d = new Vector2d(f.getX(), -f.getZ());
		
		//perform traversal of scene objects
		sceneTraversal.specifySceneObjectFilter(drawOptionsQueryBitmask, drawOptionsValueBitmask);
		tree.traverse(sceneTraversal, false, octreeChildOrder, camera, light.getCuller());
		
		//perform traversal of terrains
		for(Terrain<GL,Buffer,Texture> terrain : terrains){
			if(!terrain.getDrawOptions().checkFlags(drawOptionsQueryBitmask, drawOptionsValueBitmask)){ continue; }
				
			//sort children for front-to-back traversal
			QuadtreeChild.sort(forward2d, terrain.getMetrics().getTerrainPosBounds().getDimensions(), quadtreeChildOrder);
			
			sceneTraversal.initializeTerrainTraversal(terrain, camera.getPosition(), terrain.getTileData());
			terrain.getQuadtree().traverse(sceneTraversal, true, quadtreeChildOrder, camera, light.getCuller());
			sceneTraversal.finalizeTerrainTraversal(terrain);
		}
	}
	
	
	//========================================================
	// PICKING
	//========================================================
	
	public PickData<Vector3d> pick(GL gl, final Vector3d p, final Vector3d normalizedRay, float maxPickDistance){
		
		//initialize a PickData
		final PickData<Vector3d> pickData = new PickData<Vector3d>(maxPickDistance);
		
		//create a CullerEdge by which to cull the scene octree and terrain quadtrees
		Vector3d q = normalizedRay.copy();
		q.scale(maxPickDistance); q.add(p);
		final CullerEdge<Vector3d> rayCuller = new CullerEdge<Vector3d>(p, q);
		
		//sort octree children front-to-back
		OctreeChild.sort(normalizedRay, octreeDimensions, octreeChildOrder);
		
		//check scene objects
		tree.traverse(
			new OctreeNodeTraversal<SceneOctree<GL,Buffer,Texture>>(){
				@Override
				public void handleOctreeNode(SceneOctree<GL,Buffer,Texture> node){
					for(SceneObject<?,?,?> sceneObject : node.getObjects()){
						if(!sceneObject.getDrawOptions().checkFlags(DrawOptions.MASK_PICKABLE, DrawOptions.MASK_PICKABLE)){ continue; }
						
						//check for pick, and if there is one, shorten the culler
						boolean result = sceneObject.pick(pickData, p, normalizedRay, pickData.getPickDistance(), rayCuller);
						if(result){ rayCuller.setFinalPoint(pickData.getPickPosition()); }
					}
				}
			}, false, octreeChildOrder, rayCuller);
		
		//check terrains
		for(Terrain<GL,Buffer,Texture> terrain : terrains){
			if(!terrain.getDrawOptions().checkFlags(DrawOptions.MASK_PICKABLE, DrawOptions.MASK_PICKABLE)){ continue; }

			//no need to manually shorten the culler upon finding a pick, here, since
			//terrain.pick will do this as it traverses the terrain quadtree.
			terrain.pick(gl, pickData, p, normalizedRay, pickData.getPickDistance(), rayCuller);
		}
		
		//return pick data
		return pickData;
	}
}
