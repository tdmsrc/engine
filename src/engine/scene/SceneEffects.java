package engine.scene;

import geometry.math.Vector3d;


/**
 * Represents options for global rendering effects.
 * It is up to the renderer to implement or ignore the effects.
 */
public class SceneEffects {
	
	private float fogDensity;
	private Vector3d fogColor;
	
	private boolean ambientLight;
	private Vector3d ambientColor;
	private float ambientCoefficient;
	
	private boolean colorAdjustEnabled;
	private float colorAdjustBrightness, colorAdjustContrast, colorAdjustSaturation;
	
	private boolean hsvAdjustEnabled;
	private float hsvAdjustHue, hsvAdjustSaturation, hsvAdjustValue;
	
	private boolean blurEnabled;
	private float blurOpacity;
	
	private boolean depthOfFieldEnabled;
	private float depthOfFieldFocusDistance, depthOfFieldFocusRange;
	
	
	public SceneEffects(){
		
		//set some reasonable default values
		fogDensity = 0.001f;
		fogColor = new Vector3d(0.7f, 0.8f, 1.0f);
		
		ambientLight = true;
		ambientColor = new Vector3d(1.0f, 1.0f, 1.0f);
		ambientCoefficient = 0.2f;
		
		colorAdjustEnabled = false;
		colorAdjustBrightness = 0.0f; //added to rgb components
		colorAdjustContrast = 1.0f; //lerp with 0 = (lum,lum,lum), 1 = initial color
		colorAdjustSaturation = 1.0f; //lerp with 0 = grey, 1 = initial color
		
		hsvAdjustEnabled = false;
		hsvAdjustHue = 0.0f;
		hsvAdjustSaturation = 1.0f;
		hsvAdjustValue = 1.0f;
		
		blurEnabled = true;
		blurOpacity = 0.5f;
		
		depthOfFieldEnabled = false;
		depthOfFieldFocusDistance = 6.0f;
		depthOfFieldFocusRange = 10.0f;
	}
	
	//fog
	public float getFogDensity(){ return fogDensity; }
	public Vector3d getFogColor(){ return fogColor; }
	
	public void setFogDensity(float fogDensity){ this.fogDensity = fogDensity; }
	public void setFogColor(Vector3d fogColor){ this.fogColor = fogColor; }
	
	//ambient lighting
	public boolean hasAmbientLight(){ return ambientLight; }
	public float getAmbientCoefficient(){ return ambientCoefficient; }
	public Vector3d getAmbientColor(){ return ambientColor; }
	
	public void setHasAmbientLight(boolean ambientLight){ this.ambientLight = ambientLight; }
	public void setAmbientCoefficient(float ambientCoefficient){ this.ambientCoefficient = ambientCoefficient; }
	public void setAmbientColor(Vector3d ambientColor){ this.ambientColor = ambientColor; }
	
	//color adjust
	public boolean getColorAdjustEnabled(){ return colorAdjustEnabled; }
	public float getColorAdjustBrightness(){ return colorAdjustBrightness; }
	public float getColorAdjustContrast(){ return colorAdjustContrast; }
	public float getColorAdjustSaturation(){ return colorAdjustSaturation; }
	
	public void setColorAdjustEnabled(boolean colorAdjustEnabled){ this.colorAdjustEnabled = colorAdjustEnabled; }
	public void setColorAdjustBrightness(float colorAdjustBrightness){ this.colorAdjustBrightness = colorAdjustBrightness; }
	public void setColorAdjustContrast(float colorAdjustContrast){ this.colorAdjustContrast = colorAdjustContrast; }
	public void setColorAdjustSaturation(float colorAdjustSaturation){ this.colorAdjustSaturation = colorAdjustSaturation; }
	
	//HSV adjust
	public boolean getHSVAdjustEnabled(){ return hsvAdjustEnabled; }
	public float getHSVAdjustHue(){ return hsvAdjustHue; }
	public float getHSVAdjustSaturation(){ return hsvAdjustSaturation; }
	public float getHSVAdjustValue(){ return hsvAdjustValue; }
	
	public void setHSVAdjustEnabled(boolean hsvAdjustEnabled){ this.hsvAdjustEnabled = hsvAdjustEnabled; }
	public void setHSVAdjustHue(float hsvAdjustHue){ this.hsvAdjustHue = hsvAdjustHue; }
	public void setHSVAdjustSaturation(float hsvAdjustSaturation){ this.hsvAdjustSaturation = hsvAdjustSaturation; }
	public void setHSVAdjustValue(float hsvAdjustValue){ this.hsvAdjustValue = hsvAdjustValue; }
	
	//blur
	public boolean getBlurEnabled(){ return blurEnabled; }
	public float getBlurOpacity(){ return blurOpacity; }
	
	public void setBlurEnabled(boolean blurEnabled){ this.blurEnabled = blurEnabled; }
	public void setBlurOpacity(float blurOpacity){ this.blurOpacity = blurOpacity; }
	
	//depth of field
	public boolean getDepthOfFieldEnabled(){ return depthOfFieldEnabled; }
	public float getDepthOfFieldFocusDistance(){ return depthOfFieldFocusDistance; }
	public float getDepthOfFieldFocusRange(){ return depthOfFieldFocusRange; }
	
	public void setDepthOfFieldEnabled(boolean depthOfFieldEnabled){ this.depthOfFieldEnabled = depthOfFieldEnabled; }
	public void setDepthOfFieldFocusDistance(float depthOfFieldFocusDistance){ this.depthOfFieldFocusDistance = depthOfFieldFocusDistance; }
	public void setDepthOfFieldFocusRange(float depthOfFieldFocusRange){ this.depthOfFieldFocusRange = depthOfFieldFocusRange; }
	
}
