package engine.scene;

import geometry.math.Camera;
import geometry.math.Vector3d;
import geometry.spacepartition.Culler;
import geometry.spacepartition.CullerNoCull;


public class Light extends SceneEntity{
	
	/**
	 * Indicates a shadow type; one of:
	 * <ul>
	 * <li>{@link #SHADOW_NONE}
	 * <li>{@link #SHADOW_DIRECTIONAL}
	 * <li>{@link #SHADOW_OMNI}
	 * </ul>
	 */
	public static enum ShadowType{
		SHADOW_NONE, SHADOW_DIRECTIONAL, SHADOW_OMNI
	}
	
	private Camera camera;
	
	private Vector3d color;
	private ShadowType shadowType;
	
	//final color scaled by 1 / attenuation.dot(1,d,d^2), d = dist from light
	private Vector3d attenuation;
	
	private Culler<Vector3d> culler;
	
	
	public Light(String name){
		super(name);
		
		this.camera = new Camera(90.0f, 1.0f, 0.1f, 200.0f);
		
		this.color = new Vector3d(1.0f, 1.0f, 1.0f);
		this.attenuation = new Vector3d(1.0f, 0.0f, 0.0f);
		this.shadowType = ShadowType.SHADOW_NONE;
		
		this.culler = new CullerNoCull<Vector3d>();
	}
	
	//getters and setters
	public Camera getCamera(){ return camera; }
	
	public Vector3d getColor(){ return color; }
	public void setColor(Vector3d color){ this.color = color; }
	
	public ShadowType getShadowType(){ return shadowType; }
	public void setShadowType(ShadowType shadowType){ this.shadowType = shadowType; }
	
	/**
	 * Get the light attenuation: The final frag color should be scaled by
	 * 1 / attenuation.dot(1,d,d^2), where d is the distance of the frag from the light.
	 */
	public Vector3d getAttenuation(){ return attenuation; }
	
	/**
	 * Set the light attenuation: The final frag color should be scaled by 
	 * 1 / attenuation.dot(1,d,d^2), where d is the distance of the frag from the light.
	 */
	public void setAttenuation(Vector3d attenuation){ this.attenuation = attenuation; }
	
	//culler
	public Culler<Vector3d> getCuller(){ return culler; }
	public void setCuller(Culler<Vector3d> culler){ this.culler = culler; }
}
