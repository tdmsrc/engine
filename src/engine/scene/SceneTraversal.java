package engine.scene;

import engine.renderer.DrawableIBO;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.terrain.Terrain;
import engine.terrain.TerrainQuadtree;
import engine.terrain.TerrainTileData;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.OctreeNodeTraversal;
import geometry.spacepartition.QuadtreeNodeTraversal;

//this is e.g. created by a Renderer to pass to the Scene
//when doing a single rendering pass.

public abstract class SceneTraversal<GL,
	Buffer extends GenericBuffer<GL>,
	Texture extends GenericTexture<GL>>

	implements 
		QuadtreeNodeTraversal<TerrainQuadtree>, 
		OctreeNodeTraversal<SceneOctree<GL,Buffer,Texture>>{
	
	
	private int drawOptionsQueryBitmask = 0, drawOptionsValueBitmask = 0;
	
	//object drawing methods
	public abstract void actionObject(SceneObject<GL,Buffer,Texture> sceneObject);
	
	//terrain drawing methods
	protected abstract void actionTerrainBegin(Terrain<GL,Buffer,Texture> terrain);
	protected abstract void actionTerrainTile(DrawableIBO<GL,Buffer> tileIBO, Box<Vector2d> tileLerp);	
	protected abstract void actionTerrainEnd(Terrain<GL,Buffer,Texture> terrain);
	
	//=============================
	// OCTREE TRAVERSAL
	//=============================

	public void specifySceneObjectFilter(int drawOptionsQueryBitmask, int drawOptionsValueBitmask){
		this.drawOptionsQueryBitmask = drawOptionsQueryBitmask;
		this.drawOptionsValueBitmask = drawOptionsValueBitmask;
	}
	
	@Override
	public void handleOctreeNode(SceneOctree<GL,Buffer,Texture> node){
		
		for(SceneObject<GL,Buffer,Texture> sceneObject : node.getObjects()){
			if(!sceneObject.getDrawOptions().checkFlags(drawOptionsQueryBitmask, drawOptionsValueBitmask)){ continue; }
			actionObject(sceneObject);
		}
	}
	
	//=============================
	// TERRAIN TRAVERSAL
	//=============================
	
	private Vector3d lodPosition;
	private TerrainTileData<GL,Buffer> tileData;
	
	public void initializeTerrainTraversal(
		Terrain<GL,Buffer,Texture> terrain, 
		Vector3d lodPosition, TerrainTileData<GL,Buffer> tileData){
		
		this.lodPosition = lodPosition;
		this.tileData = tileData;
		
		actionTerrainBegin(terrain);
	}
	
	@Override
	public void handleQuadtreeNode(TerrainQuadtree node){
		
		actionTerrainTile(node.getIBO(lodPosition, tileData), node.getTileLerp());
	}
	
	public void finalizeTerrainTraversal(Terrain<GL,Buffer,Texture> terrain){
		
		actionTerrainEnd(terrain);
		
		lodPosition = null;
		tileData = null;
	}
}