package engine.collision;

import java.util.Collection;
import java.util.LinkedList;

import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.scene.DrawOptions;
import engine.scene.Scene;
import engine.scene.SceneObject;
import engine.scene.SceneOctree;
import engine.terrain.ColliderTerrain;
import engine.terrain.Terrain;
import geometry.common.MessageOutput;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.OctreeChild;
import geometry.spacepartition.OctreeNodeTraversal;

//this represents a moving ellipsoid, which can interact with a Collider

public abstract class CollisionEllipsoid<GL> extends Ellipsoid<Vector3d>{

	//"FINE TUNING" PARAMETERS FOR COLLISION
	//----------------------------------------------------
	//if ellipsoid has already moved inside e.g. a plane, movement needs to be negative;
	//if this is 0, no tolerance for being infinitesimally through a wall (NEW_CENTER_OFFSET helps this, though)
	//if this is too negative, could pop through the back side of a wall (magnitude should be < smallest ellipsoid diameter) 
	protected static final float MINIMUM_COLLISION_DISTANCE = -0.01f;
	//very small offset from collision point along the normal to ellipsoid at the collision point
	//if this is 0, will run into the same object repeatedly during moveRecurse, causing "sticking"
	//if this is too large, will cause noticable vibration from bouncing off the floor
	protected static final float NEW_CENTER_OFFSET = 0.0001f;
	//----------------------------------------------------
	
	//if the velocity of the ellipsoid is less than this, do not do any movement at all
	protected static final float MIN_VELOCITY = 0.002f;
	//recurse at most this many times; prevents infinite loops if "sticking" occurs; 
	//NEW_CENTER_EPS should prevent sticking so this is unexpected; warning printed if reached
	protected static final int MAX_MOVES = 50;
	
	
	protected float mass; //in kg
	protected Vector3d velocity; //in units/sec
	
	//things with which to collide
	private boolean hasScene;	
	private Scene<GL,?,?> scene;
	private Collection<ColliderTerrain<GL>> terrainColliders;

	
	public CollisionEllipsoid(Vector3d radii, float mass, Vector3d center, Vector3d velocity){
		super(radii, center);
		
		this.mass = mass;
		this.velocity = velocity;
		hasScene = false;
	}
	
	public float getMass(){ return mass; }
	public void setMass(float mass){ this.mass = mass; }
	
	public Vector3d getVelocity(){ return velocity; }
	public void setVelocity(Vector3d velocity){ this.velocity = velocity; }
	
	public void setScene(Scene<GL,?,?> scene){
		
		if(hasScene){ this.scene.removeCollisionEllipsoid(this); }
		this.scene = scene;
		hasScene = true;
		
		scene.addCollisionEllipsoid(this);
		updateTerrains();
	}
	
	public void updateTerrains(){
		
		this.terrainColliders = new LinkedList<ColliderTerrain<GL>>();
		
		for(Terrain<GL,?,? extends GenericTexture<GL>> terrain : scene.getTerrains()){
			if(!terrain.getDrawOptions().checkFlags(DrawOptions.MASK_COLLIDER, DrawOptions.MASK_COLLIDER)){ continue; }
			
			terrainColliders.add(new ColliderTerrain<GL>(terrain));
		}
	}
	
	//======================================
	// movement with collision detection
	//======================================
	
	protected abstract Vector3d collisionResponse(CollisionData<Vector3d> collisionData, Vector3d movement);
	
	public void moveWithCollision(GL gl, long dt){ //dt = number of milliseconds that have passed
		
		Vector3d movement = velocity.copy();
		movement.scale((float)dt / 1000.0f);
		
		if(!hasScene){ move(movement); return; } 
		moveRecurse(gl, movement, MAX_MOVES);
	}
	
	private void moveRecurse(GL gl, Vector3d movement, int movesRemaining){
	
		//report warning if recursion limit is reached
		if(movesRemaining == 0){
			MessageOutput.printWarning("Possible infinite loop during collision test");
		}
		//do nothing if exceeded recursion limit or velocity is too low
		if((movesRemaining == 0) || (velocity.length() < MIN_VELOCITY)){ return; }
		
		//get structures to pass to colliders
		CollisionData<Vector3d> collisionData = new CollisionData<Vector3d>();
		Box<Vector3d> movingEllipsoidBounds = getBoundingBox(movement);
		
		float minimumMovementMultiple = MINIMUM_COLLISION_DISTANCE / movement.length();
		Vector3d movementr = movement.copy();
		movementr.divideComponentWise(getRadii());
		
		//SceneObject collisions
		collideSceneObjects(scene, collisionData, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
		
		//Terrain collisions
		for(ColliderTerrain<GL> terrainCollider : terrainColliders){
			terrainCollider.update(gl, movingEllipsoidBounds);
			terrainCollider.collide(collisionData, this, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
		}
		
		//if there was no collision, simply move
		if(!collisionData.checkCollisionOccurred()){
			move(movement);
			return;
		}
		
		//perform collision response and recurse
		moveRecurse(gl, collisionResponse(collisionData, movement), movesRemaining-1); 
	}
	
	private <Buffer extends GenericBuffer<GL>, Texture extends GenericTexture<GL>>
		void collideSceneObjects(Scene<GL,Buffer,Texture> scene, 
			final CollisionData<Vector3d> collisionData,
			final Vector3d movement, final Vector3d movementr, final float minimumMovementMultiple, final Box<Vector3d> movingEllipsoidBounds){
	
		final CollisionEllipsoid<GL> ce = this;
		
		//traverse scene's octree
		scene.getOctree().traverse(
			new OctreeNodeTraversal<SceneOctree<GL,Buffer,Texture>>(){
				@Override
				public void handleOctreeNode(SceneOctree<GL,Buffer,Texture> node){
					for(SceneObject<?,?,?> sceneObject : node.getObjects()){
						if(!sceneObject.getDrawOptions().checkFlags(DrawOptions.MASK_COLLIDER, DrawOptions.MASK_COLLIDER)){ continue; }
						
						sceneObject.collide(collisionData, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
					}
				}
			}, false, OctreeChild.values(), movingEllipsoidBounds);
	}
}
