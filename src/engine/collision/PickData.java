package engine.collision;

import geometry.math.FixedDimensionalVector;

/**
 * This class holds information about a pick.  It is maintained during
 * the process of checking for a pick and returned as the result of 
 * checking for a pick (i.e., by Scene.pick).
 * <br><br>
 * The information is whether or not a pick has occurred, the distance of
 * the closest pick, the position of the closest pick, and the Collider which
 * reported the closest pick (the latter to are null if no pick has occurred so far).
 * <br><br>
 * An instance of this class is passed to all relevant objects
 * implementing the {@link Pickable} or {@link PickableTerrain} interface,
 * and each such object updates this information, if necessary, by calling 
 * {@link PickData#reportPick} in its {@link Pickable#pick} method.
 */
public class PickData<Vector extends FixedDimensionalVector<Vector>>{
	
	private boolean pickOccurred;
	private float pickDistance;
	private Vector pickPosition;
	private Collider<Vector> pickObject;
	
	
	public PickData(float maxPickDistance){
		reset(maxPickDistance);
	}
	
	/**
	 * Reset the information stored in this {@link PickData} to the defaults.
	 */
	public void reset(float maxPickDistance){
		pickOccurred = false;
		pickDistance = maxPickDistance;
		pickPosition = null;
		pickObject = null;
	}
	
	/**
	 * Indicate that a pick occurred with the specified data.  The information
	 * in this {@link PickData} will only be updated if this is the first pick that
	 * has occurred for this PickData, or if the pickDistance is less than that
	 * stored in this PickData.
	 * 
	 * @param pickDistance The distance from the pick ray's initial point that 
	 * the pick occurred.
	 * @param pickPosition The position at which the pick occurred.
	 * 
	 * @return Returns true if the PickData was updated as a result of calling 
	 * reportPick, false otherwise.
	 */
	public boolean reportPick(float pickDistance, Vector pickPosition, Collider<Vector> pickObject){
		if(!pickOccurred || (pickDistance < this.pickDistance)){
			pickOccurred = true;
			this.pickDistance = pickDistance;
			this.pickPosition = pickPosition;
			this.pickObject = pickObject;
			return true;
		}
		return false;
	}
	
	/**
	 * Indicates whether a pick has occurred.  Initially false.
	 */
	public boolean checkPickOccurred(){ return pickOccurred; }
	
	/**
	 * Get the distance of the closest pick encountered so far.  If no pick has
	 * occurred, this returns "maxPickDistance" as defined in the constructor.
	 */
	public float getPickDistance(){ return pickDistance; }
	
	/**
	 * Get the position of the closest pick encountered so far.  
	 * If no pick has occurred, this will be null.
	 */
	public Vector getPickPosition(){ return pickPosition; }
	
	/**
	 * Get the Collider which reported the closest pick encountered so far.  
	 * If no pick has occurred, this will be null.
	 */
	public Collider<Vector> getPickObject(){ return pickObject; }
}