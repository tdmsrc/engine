package engine.collision;

import geometry.math.FixedDimensionalVector;

public interface PickListener<Vector extends FixedDimensionalVector<Vector>>{
	
	public void pickOccurred(Vector pickPosition);
}
