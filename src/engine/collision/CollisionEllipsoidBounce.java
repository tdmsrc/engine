package engine.collision;

import geometry.math.Vector3d;

public class CollisionEllipsoidBounce<GL> extends CollisionEllipsoid<GL>{

	public CollisionEllipsoidBounce(Vector3d radii, float mass, Vector3d center, Vector3d velocity){
		super(radii, mass, center, velocity);
	}
	
	@Override
	protected Vector3d collisionResponse(CollisionData<Vector3d> collisionData, Vector3d movement){
		
		//get new center
		float d = collisionData.getCollisionMovementMultiple();
		Vector3d newCenter = movement.copy(); newCenter.scale(d); newCenter.add(getCenter());
		
		//get normal to tangent plane at intersection point
		Vector3d intRay = collisionData.getCollisionPoint().copy(); intRay.subtract(newCenter);
		Vector3d n = getTangentPlaneNormal(intRay);
		
		//bounce remaining movement off tangent plane
		Vector3d bounceMovement = Vector3d.linearCombination(1.0f, movement, -2*movement.dot(n), n);
		bounceMovement.scale(1.0f-d);
		
		//bounce velocity off tangent plane
		Vector3d bounceVelocity = Vector3d.linearCombination(1.0f, velocity, -2*velocity.dot(n), n);
		bounceVelocity.scale(0.8f);
		setVelocity(bounceVelocity);
		
		//float NEW_CENTER_EPS away from contact point
		Vector3d newCenterAdjust = n.copy(); newCenterAdjust.flip();
		newCenterAdjust.scale(NEW_CENTER_OFFSET);
		newCenter.add(newCenterAdjust);
		
		//set new state and recurse
		setCenter(newCenter);
		return bounceMovement;
	}
}
