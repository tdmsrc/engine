package engine.collision;

import engine.scene.Scene;
import geometry.math.FixedDimensionalVector;
import geometry.math.ObjectTransformation3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;

/**
 * Implements pick and collide.
 * 
 * Collide is used by {@link CollisionEllipsoid#moveWithCollision}, 
 * and pick is used by {@link Scene#pick}.
 */
public interface Collider<Vector extends FixedDimensionalVector<Vector>> 
	extends PickListener<Vector>{
	
	/**
	 * Updates the {@link PickData} "pickData", if necessary, with the closest 
	 * intersection of:
	 * <ul>
	 * <li> The geometric object represented by this class, after having been transformed by 
	 * objTransformation,
	 * <li> The "pick ray", which is the directed edge which starts at "p", has direction 
	 * "normalizedRay", and has length "maxPickDistance".
	 * </ul>
	 * The "closest" intersection means the intersection nearest the point "p".
	 * <br><br> 
	 * The data held by the {@link CullerEdge} "rayCuller" is redundant; it 
	 * can be assumed that the initial point of that edge is equal to p, and that
	 * the final point of that edge is equal to p+maxPickDistance*normalizedRay. 
	 * <br><br>
	 * The {@link CullerEdge} "rayCuller" may be modified as a result of calling this
	 * method; in particular, its final point may be set to pickData.getPickPosition().
	 * 
	 * @param pickData The information about the closest pick so far.
	 * @param objTransformation The transformation to be applied to this object.
	 * @param p The initial point of the pick ray.
	 * @param normalizedRay The direction of the pick ray, as a normalized vector.
	 * @param maxPickDistance The length of the pick ray.
	 * @param rayCuller The {@link CullerEdge} holding the pick ray.
	 * 
	 * @return Returns true if a pick occurred, false otherwise.
	 */
	public boolean pick(PickData<Vector> pickData, ObjectTransformation3d objTransformation,
		Vector p, Vector normalizedRay, float maxPickDistance, CullerEdge<Vector> rayCuller);
	
	public boolean pick(PickData<Vector> pickData,
		Vector p, Vector normalizedRay, float maxPickDistance, CullerEdge<Vector> rayCuller);
	
	//true or false depending on whether this particular "collide" call produced a collision
	//"movementr" is "movement" divided componentwise by ellipsoid radii
	
	//[TODO] write nice javadoc for the rest of these methods
	public boolean collide(CollisionData<Vector> collisionData,
		Ellipsoid<Vector> ce, Vector movement, Vector movementr, float minimumMovementMultiple, Box<Vector> movingEllipsoidBounds);
	
	public boolean collide(CollisionData<Vector> collisionData, ObjectTransformation3d objTransformation,
		Ellipsoid<Vector> ce, Vector movement, Vector movementr, float minimumMovementMultiple, Box<Vector> movingEllipsoidBounds);
}
