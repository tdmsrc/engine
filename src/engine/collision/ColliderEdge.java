package engine.collision;

import geometry.math.ObjectTransformation3d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;


public class ColliderEdge implements Collider<Vector3d>{

	//action to use when pick occurs
	private PickListener<Vector3d> pickListener;
	
	//edge data
	Vector3d p,e; //edge is p->q, where q = p+e
	float elensq; //e.lengthSquared()
	float collisionRadius; //distance from edge to count as a collision
	
	
	public ColliderEdge(Vector3d p, Vector3d q, float collisionRadius, PickListener<Vector3d> pickListener){
		this.pickListener = pickListener;
		
		this.p = p;
		e = q.copy(); e.subtract(p);
		elensq = e.lengthSquared();
		this.collisionRadius = collisionRadius;
	}
	
	@Override
	public void pickOccurred(Vector3d pickPosition){
		pickListener.pickOccurred(pickPosition);
	}
	
	//==================================================
	// PICKING
	//==================================================
	
	@Override
	public boolean pick(PickData<Vector3d> pickData,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		return pick(pickData, null, false, p, normalizedRay, maxPickDistance, rayCuller);
	}
	
	@Override
	public boolean pick(PickData<Vector3d> pickData, ObjectTransformation3d objTransformation,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		return pick(pickData, objTransformation, true, p, normalizedRay, maxPickDistance, rayCuller);
	}
	
	/**
	 * Encompasses both transformed and non-transformed versions of {@link Collider#pick}
	 */
	private boolean pick(PickData<Vector3d> pickData, 
		ObjectTransformation3d objTransformation, boolean isTransformed,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		//by some math we can show the following: 
		//let l(s) = a + s*e, l'(t) = b + t*r, dsq(s,t) = ||l(s)-l'(t)||^2
		//let u = a-b, A = [-u-][-e-][-r-], M = AA^T
		//then the minimum of dsq(s,t) occurs with value dsq = det(M)/minor(M,1,1)
		//at s = minor(M,2,1)/minor(M,1,1), t = minor(M,3,1)/minor(M,1,1)
		//and: det(M) = det(A)^2 = |u.(exr)|^2, minor(M,1,1) = |exr|^2, 
		//minor(M,2,1) = (e.r)(u.r)-|r|^2(u.e), minor(M,3,1) = |e|^2(u.r)-(e.r)(u.e)
		
		//get transformed edge
		Vector3d pt = isTransformed ? objTransformation.applyToPoint(this.p) : this.p;
		Vector3d et = isTransformed ? objTransformation.applyToVector(this.e) : this.e;
		
		//compute lower bound for dist from edge to ray
		Vector3d exr = et.cross(normalizedRay);
		float m11 = exr.lengthSquared(), exrlen = (float)Math.sqrt(m11);
		
		Vector3d u = pt.copy(); u.subtract(p);
		float detA = u.dot(exr); if(detA < 0){ detA = -detA; }
		
		float d = detA/exrlen;
		if(d > collisionRadius){ return false; } 
		
		//compute some dot products
		float eDotr = et.dot(normalizedRay), eDotu = et.dot(u), uDotr = u.dot(normalizedRay);
		
		//compute s (should be in [0,1])
		float m21 = eDotr*uDotr-eDotu;
		float s = m21/m11;
		if((s < 0) || (s > 1)){ return false; }
		
		//compute t (should be in [0,maxPickDistance])
		float m31 = elensq*uDotr - eDotr*eDotu;		
		float t = m31/m11;
		if((t < 0) || (t > maxPickDistance)){ return false; }
		
		//report pick, if still here
		Vector3d x = normalizedRay.copy(); x.scale(t); x.add(p);
		pickData.reportPick(d, x, this);
		return true;
	}
	
	//==================================================
	// COLLISION (do nothing)
	//==================================================
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		return false;
	}
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData, ObjectTransformation3d objTransformation,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		return false;
	}
}
