package engine.collision;

import engine.collision.Ellipsoid.EllipsoidPlaneCollisionData;
import engine.collision.Ellipsoid.SphereLineCollisionData;
import engine.collision.Ellipsoid.SpherePointCollisionData;
import geometry.math.ObjectTransformation3d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;

public class ColliderTriangle implements Collider<Vector3d>{
	
	//normal and CCW vertices (CCW when normal pointing at you)
	private Vector3d n, v[];
	
	//precomputed things (see pointInExtrudedTriangle comments)
	private Vector3d en1, en2;
	private float d0, d1, d2;
	
	//action to use when pick occurs
	private PickListener<Vector3d> pickListener;
	
	
	public ColliderTriangle(Vector3d v0, Vector3d v1, Vector3d v2, PickListener<Vector3d> pickListener){
		this.pickListener = pickListener;
		
		//store vertices
		v = new Vector3d[]{ v0, v1, v2 };
		
		//compute normal
		Vector3d e1 = v[1].copy(); e1.subtract(v0);
		Vector3d e2 = v[2].copy(); e2.subtract(v0);
		n = e1.cross(e2); n.normalize();
		
		//compute edge normals
		en1 = n.cross(e1);
		en2 = e2.cross(n);
		//compute dot test values
		d1 = v[0].dot(en1); d2 = v[0].dot(en2);
		d0 = v[1].dot(en1) + v[1].dot(en2);
	}
	
	@Override
	public void pickOccurred(Vector3d pickPosition){
		pickListener.pickOccurred(pickPosition);
	}
	
	/**
	 * Determines if the point p is in the region obtained by extruding
	 * this triangle infinitely in the direction perpendicular to the plane 
	 * in which it is contained.
	 * 
	 * @param p The point to check.
	 * @return True if the point is in the region, false otherwise.
	 */
	public boolean pointInExtrudedTriangle(Vector3d p){
		
		//Point in triangle with 2 dots:
		//-----------------------------------
		//to check if a 3d point, which is assumed to already be in the plane containing
		//the triangle, is inside the triangle, we can check it is on the correct side
		//of the 3 planes formed by extruding the triangle's boundary along the normal.
		//
		//Let en1 = nx(v1-v0), en2 = (v2-v0)xn, en0 = nx(v2-v1) = -en1-en2; we must check:
		// (p-v0).en1 > 0 <=> p.en1 > v0.en1 =: d1
		// (p-v0).en2 > 0 <=> p.en2 > v0.en2 =: d2
		// (p-v1).en0 > 0 <=> (p-v1).(-en1-en2) > 0 <=> p.en1+p.en2 < v1.en1+v1.en2 =: d0
		
		float pd1 = p.dot(en1), pd2 = p.dot(en2);
		return ((pd1 > d1) && (pd2 > d2) && ((pd1+pd2) < d0));
	}

	//==================================================
	// PICKING
	//==================================================
	
	@Override
	public boolean pick(PickData<Vector3d> pickData,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		return pick(pickData, null, false, p, normalizedRay, maxPickDistance, rayCuller);
	}
	
	@Override
	public boolean pick(PickData<Vector3d> pickData, ObjectTransformation3d objTransformation,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		return pick(pickData, objTransformation, true, p, normalizedRay, maxPickDistance, rayCuller);
	}
	
	/**
	 * Encompasses both transformed and non-transformed versions of {@link Collider#pick}
	 */
	private boolean pick(PickData<Vector3d> pickData, 
		ObjectTransformation3d objTransformation, boolean isTransformed,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		//if isTransformed, first apply reverse transformation to p and normalizedRay
		Vector3d pRT = isTransformed ? objTransformation.unApplyToPoint(p) : p;
		Vector3d rayRT = isTransformed ? objTransformation.unApplyToVector(normalizedRay) : normalizedRay;
		
		//get oriented distance from p to plane
		Vector3d pMinusV0 = pRT.copy(); pMinusV0.subtract(v[0]);
		float dPlane = pMinusV0.dot(n);
		
		//if point on wrong side of plane, do nothing
		if(dPlane < 0){ return false; }
		//if ray is pointing away from, or tangent to, plane, do nothing
		float rayDotN = rayRT.dot(n);
		if(rayDotN >= 0){ return false; }
		
		//intersect ray with plane:
		//p+d*ray = v0+t; d = -(p-v0).n / ray.n = -dPlane / rayDotN
		float d = -dPlane / rayDotN;
		
		//check if intersection is in triangle
		Vector3d xRT = rayRT.copy(); xRT.scale(d); xRT.add(pRT);
		if(pointInExtrudedTriangle(xRT)){
			
			//report intersection point (adjusting for transformation if isTransformed)
			Vector3d x = xRT;
			if(isTransformed){ x = normalizedRay.copy(); x.scale(d); x.add(p); }
			return pickData.reportPick(d, x, this);
		}
		
		//to be precise, should check whether ray passes within some PICK_TRIANGLE_EPS 
		//of an edge, but in practice it doesn't seem to be necessary at all
		return false;
	}
	
	//==================================================
	// COLLISION
	//==================================================
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
	
		return collide(collisionData, null, false, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
	}
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData, ObjectTransformation3d objTransformation,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
	
		return collide(collisionData, objTransformation, true, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
	}
	
	/**
	 * Encompasses both transformed and non-transformed versions of {@link Collider#collide}
	 */
	private boolean collide(CollisionData<Vector3d> collisionData, 
		ObjectTransformation3d objTransformation, boolean isTransformed,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		//PLANE COLLISION
		Vector3d p0T = isTransformed ? objTransformation.applyToPoint(v[0]) : v[0];
		Vector3d nT = isTransformed ? objTransformation.applyToVector(n) : n;
		EllipsoidPlaneCollisionData planeCollisionData = 
			Ellipsoid.collideMovingEllipsoidWithPlane(ce, movement, p0T, nT);
		
		//if no collision or d > 1, no chance of colliding with the interior or boundary of triangle
		if(	!planeCollisionData.collisionOccurred || 
			(planeCollisionData.movementMultiple > 1)){ return false; }
		
		//if d < 0, may still hit the boundary, but not the triangle interior
		if(planeCollisionData.movementMultiple > minimumMovementMultiple){ 
			
			//get collision point (reverse-transformed if isTransformed)
			Vector3d x = planeCollisionData.collisionPosition;
			Vector3d xRT = isTransformed ? objTransformation.unApplyToPoint(x) : x;
			
			//check if collision point is in triangle interior
			if(pointInExtrudedTriangle(xRT)){
				//report collision and quit (tri boundary can be safely ignored)
				return collisionData.reportCollision(
					planeCollisionData.movementMultiple, x);
			}
		}
		
		//at this point, either the ellipsoid is intersecting the plane, or it is on the
		//positive side of the plane, and can reach the plane before running out of movement,
		//but does not intersect the triangle interior. 
		
		//copy of (if isTransformed) transformed triangle vertices [not to be modified]
		Vector3d vT[] = new Vector3d[]{ p0T,
			isTransformed ? objTransformation.applyToPoint(v[1]) : v[1],
			isTransformed ? objTransformation.applyToPoint(v[2]) : v[2],
		};
		//triangle vertices divided componentwise by radius, adjusted by objTransformation if isTransformed
		Vector3d wr[] = new Vector3d[]{ vT[0].copy(), vT[1].copy(), vT[2].copy() };
		for(int i=0; i<3; i++){ wr[i].divideComponentWise(ce.getRadii()); }
		
		//store closest hit for boundary components
		boolean hitBoundary = false;
		float dBoundaryMin = 0.0f;
		Vector3d xBoundaryMin = null;
		
		//VERTEX COLLISIONS
		for(int i=0; i<3; i++){
			SpherePointCollisionData vertexCollisionData = 
				Ellipsoid.collideMovingUnitSphereWithPoint(ce.getCenterDividedByRadii(), movementr, wr[i]);
			
			if(!vertexCollisionData.collisionOccurred){ continue; }
			if(	(vertexCollisionData.movementMultiple < minimumMovementMultiple) || 
				(vertexCollisionData.movementMultiple > 1) ){ continue; } 
			
			//store closest hit
			if(!hitBoundary || (vertexCollisionData.movementMultiple < dBoundaryMin)){
				hitBoundary = true;
				dBoundaryMin = vertexCollisionData.movementMultiple;
				xBoundaryMin = vT[i];
			}
		}
		
		//EDGE COLLISIONS
		for(int i=0; i<3; i++){
			SphereLineCollisionData edgeCollisionData = 
				Ellipsoid.collideMovingUnitSphereWithLine(ce.getCenterDividedByRadii(), movementr, wr[i], wr[(i+1)%3]);
			
			if(!edgeCollisionData.collisionOccurred){ continue; }
			if(	(edgeCollisionData.movementMultiple < minimumMovementMultiple) || 
				(edgeCollisionData.movementMultiple > 1) ){ continue; } 
			if(	(edgeCollisionData.lineLerp < 0) || 
				(edgeCollisionData.lineLerp > 1) ){ continue; } 
			
			//store closest hit
			if(!hitBoundary || (edgeCollisionData.movementMultiple < dBoundaryMin)){
				hitBoundary = true;
				dBoundaryMin = edgeCollisionData.movementMultiple;
				xBoundaryMin = vT[i].lerp(vT[(i+1)%3], edgeCollisionData.lineLerp);
			}
		}
		
		return hitBoundary ? collisionData.reportCollision(dBoundaryMin, xBoundaryMin) : false;
	}
}
