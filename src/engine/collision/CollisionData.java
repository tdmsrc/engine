package engine.collision;

import geometry.math.FixedDimensionalVector;


public class CollisionData<Vector extends FixedDimensionalVector<Vector>>{
	
	private boolean collisionOccurred;
	private float movementMultiple; //multiple of CollisionEllipsoid movement vector to travel along
	private Vector collisionPoint; //the point at which the ellipsoid collided with the collider
	
	public CollisionData(){
		reset();
	}
	
	public void reset(){
		collisionOccurred = false;
		movementMultiple = 1.0f;
		collisionPoint = null;
	}
	
	//returns true if collision is better than previous best
	public boolean reportCollision(float d, Vector x){
		if(!collisionOccurred || (d < movementMultiple)){
			collisionOccurred = true;
			movementMultiple = d;
			collisionPoint = x;
			return true;
		}
		return false;
	}
	
	public boolean checkCollisionOccurred(){ return collisionOccurred; }
	public float getCollisionMovementMultiple(){ return movementMultiple; }
	public Vector getCollisionPoint(){ return collisionPoint; }
}