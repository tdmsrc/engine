package engine.collision;

import java.util.Collection;
import java.util.LinkedList;

import geometry.math.ObjectTransformation3d;
import geometry.math.Vector3d;
import geometry.polyhedron.components.PolyhedronEdge;
import geometry.rawgeometry.ObjData;
import geometry.rawgeometry.ObjData.VertexData;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;
import geometry.spacepartition.Culler.BoxContainment;


//this is an axis-aligned bounding box containing some number of ColliderTriangles

/**
 * A ColliderGroup is a {@link BoundingBox} and a collection of 
 * {@link Collider}<{@link Vector3d}> which are contained in the BoundingBox.<br>
 * When adding a component to the ColliderGroup, the BoundingBox is 
 * automatically expanded to encompass the new object.<br>
 * Collision and picking are performed by first checking against the BoundingBox,
 * then checking every Collider in the collection.
 */
public class ColliderGroup implements Collider<Vector3d>{

	private static final float BOX_EPS = 0.01f; //min dims of box
	
	private BoundingBox boundingBox;
	private LinkedList<Collider<Vector3d>> colliders;
	private PickListener<Vector3d> pickListener;
	
	
	public ColliderGroup(PickListener<Vector3d> pickListener){
		this.pickListener = pickListener;
		
		boundingBox = null;
		colliders = new LinkedList<Collider<Vector3d>>();
	}
	
	public BoundingBox getBoundingBox(){ return boundingBox; }
	public int getColliderCount(){ return colliders.size(); }
	
	
	//==================================================
	// ADDING COMPONENTS
	//==================================================
	
	/**
	 * Add a {@link ColliderEdge} to this Collider box.<br>
	 * The default {@link PickListener} (i.e., the one passed in the constructor) 
	 * will be used for the edge.<br>
	 */
	public void addEdge(Vector3d p, Vector3d q, float collisionRadius){
		//add edge using PickListener of parent box
		addEdge(p, q, collisionRadius, pickListener);
	}
	
	/**
	 * Add a {@link ColliderEdge} to this Collider box.<br>
	 * The specified {@link PickListener} will be used for the new edge.
	 */
	public void addEdge(Vector3d p, Vector3d q, float collisionRadius, PickListener<Vector3d> pickListener){
		
		//bound edge
		Vector3d edgeMin = p.copy(), edgeMax = p.copy();
		
		for(int i=0; i<3; i++){
			if(q.get(i) < edgeMin.get(i)){ edgeMin.set(i, q.get(i)); }
			if(q.get(i) > edgeMax.get(i)){ edgeMax.set(i, q.get(i)); }
		}
		
		//adjust bounds to account for BOX_EPS and collisionRadius
		float adj = BOX_EPS;
		if(collisionRadius > BOX_EPS){ adj = collisionRadius; }
		for(int i=0; i<3; i++){
			edgeMin.set(i, edgeMin.get(i) - adj);
			edgeMax.set(i, edgeMax.get(i) + adj);
		}
		
		if(colliders.isEmpty()){
			//set bounding box to bound triangle
			//since boundingBox is never directly reported as picked, may set pickListener null 
			boundingBox = new BoundingBox(edgeMin, edgeMax, null);
		}else{
			//expand boundingBox to encompass triangle bounds
			for(int i=0; i<3; i++){
				if(edgeMin.get(i) < boundingBox.getMin().get(i)){ boundingBox.getMin().set(i, edgeMin.get(i)); }
				if(edgeMax.get(i) > boundingBox.getMax().get(i)){ boundingBox.getMax().set(i, edgeMax.get(i)); }
			}
		}
		
		colliders.add(new ColliderEdge(p, q, collisionRadius, pickListener));
	}
	
	/**
	 * Add a {@link ColliderTriangle} to this Collider box.<br>
	 * The default {@link PickListener} (i.e., the one passed in the constructor) 
	 * will be used for the triangle.<br>
	 * The triangle vertices should be specified in CCW order (i.e., with outward-pointing
	 * normal pointing toward you).
	 */
	public void addTriangle(Vector3d v0, Vector3d v1, Vector3d v2){
		//add triangle using PickListener of parent box 
		addTriangle(v0, v1, v2, pickListener);
	}
	
	/**
	 * Add a {@link ColliderTriangle} to this Collider box.<br>
	 * The specified {@link PickListener} will be used for the new triangle.<br>
	 * The triangle vertices should be specified in CCW order (i.e., with outward-pointing
	 * normal pointing toward you).
	 */
	public void addTriangle(Vector3d v0, Vector3d v1, Vector3d v2, PickListener<Vector3d> pickListener){
		
		//bound triangle
		Vector3d triMin = v0.copy(), triMax = v0.copy();
		
		for(int i=0; i<3; i++){
			if(v1.get(i) < triMin.get(i)){ triMin.set(i, v1.get(i)); }
			if(v1.get(i) > triMax.get(i)){ triMax.set(i, v1.get(i)); }
			
			if(v2.get(i) < triMin.get(i)){ triMin.set(i, v2.get(i)); }
			if(v2.get(i) > triMax.get(i)){ triMax.set(i, v2.get(i)); }
		}
		
		//adjust bounds to account for BOX_EPS
		float adj = BOX_EPS;
		for(int i=0; i<3; i++){
			triMin.set(i, triMin.get(i) - adj);
			triMax.set(i, triMax.get(i) + adj);
		}
		
		if(colliders.isEmpty()){
			//set bounding box to bound triangle
			//since boundingBox is never directly reported as picked, may set pickListener null 
			boundingBox = new BoundingBox(triMin, triMax, null);
		}else{
			//expand boundingBox to encompass triangle bounds
			for(int i=0; i<3; i++){
				if(triMin.get(i) < boundingBox.getMin().get(i)){ boundingBox.getMin().set(i, triMin.get(i)); }
				if(triMax.get(i) > boundingBox.getMax().get(i)){ boundingBox.getMax().set(i, triMax.get(i)); }
			}
		}
		
		colliders.add(new ColliderTriangle(v0, v1, v2, pickListener));
	}
	
	
	//==================================================
	// CREATING FROM OBJ DATA
	//==================================================
	
	/**
	 * Returns a {@link ColliderGroup} containing all of the faces in the specified {@link ObjData}.<br>
	 * Assumes that the ObjData is not empty, and that all of its faces are convex.
	 * 
	 * @param objData The geometry specifying the triangles comprising the ColliderGroup.
	 * @param pickListener The {@link PickListener} that will be used for every triangle.
	 * @return A {@link ColliderGroup} containing all of the faces in the specified {@link ObjData}.
	 */
	public static ColliderGroup createTriangleBoxFromObjData(ObjData objData, PickListener<Vector3d> pickListener){
		
		ColliderGroup collider = new ColliderGroup(pickListener);
		
		for(VertexData[] face : objData.faces){
			int n = face.length;
			for(int i = 1; i <= n-2; i++){
				collider.addTriangle(
					objData.position[face[0].posIndex], 
					objData.position[face[i].posIndex], 
					objData.position[face[i+1].posIndex]);
			}
		}
		
		return collider;
	}
	
	/**
	 * Returns a {@link ColliderGroup} containing each {@link PolyhedronEdge} in the specified collection.<br>
	 * 
	 * @param edges The collection of edges comprising the ColliderGroup.
	 * @param collisionRadius The maximum distance from an edge that registers a collision.
	 * @param pickListener The {@link PickListener} that will be used for every edge.
	 * @return A {@link ColliderGroup} containing each {@link PolyhedronEdge} in the specified collection.
	 */
	public static ColliderGroup createEdgeBoxFromPolyhedronEdges(
		Collection<? extends PolyhedronEdge<?,?,?>> edges, float collisionRadius,
		PickListener<Vector3d> pickListener){
		
		ColliderGroup collider = new ColliderGroup(pickListener);
		
		for(PolyhedronEdge<?,?,?> edge : edges){
			collider.addEdge(
				edge.getInitialVertex().getPosition(),
				edge.getFinalVertex().getPosition(), collisionRadius);
		}
		
		return collider;
	}
	
	
	//==================================================
	// PICKING
	//==================================================
	
	@Override
	public void pickOccurred(Vector3d pickPosition){ 
		//never reports itself as the picked object, so this will never be called
	}
	
	@Override
	public boolean pick(PickData<Vector3d> pickData,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		if(colliders.isEmpty()){ return false; }
		
		//check if rayCuller intersects box
		if(rayCuller.getBoxContainment(boundingBox) == BoxContainment.CONTAINMENT_NONE){ return false; }
		
		boolean result = false;
		for(Collider<Vector3d> c : colliders){
			//the order of operands of || here is important due to short-circuit evalution
			result = c.pick(pickData, p, normalizedRay, maxPickDistance, rayCuller) || result;
		}
		return result;
	}
	
	@Override
	public boolean pick(PickData<Vector3d> pickData, ObjectTransformation3d objTrans,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		if(colliders.isEmpty()){ return false; }
		
		//check if rayCuller intersects transformed box
		if(boundingBox.getReverseContainment(rayCuller, objTrans) == BoxContainment.CONTAINMENT_NONE){ return false; } 
		
		boolean result = false;
		for(Collider<Vector3d> c : colliders){
			//the order of operands of || here is important due to short-circuit evalution
			result = c.pick(pickData, objTrans, p, normalizedRay, maxPickDistance, rayCuller) || result;
		}
		return result;
	}
	
	
	//==================================================
	// COLLISION
	//==================================================
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		if(colliders.isEmpty()){ return false; }
		
		//check if CollisionEllipsoid intersects box
		if(movingEllipsoidBounds.getBoxContainment(boundingBox) == BoxContainment.CONTAINMENT_NONE){ return false; }
		
		boolean result = false;
		for(Collider<Vector3d> c : colliders){
			//the order of operands of || here is important due to short-circuit evalution
			result = c.collide(collisionData, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds) || result;
		}
		return result;
	}
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData, ObjectTransformation3d objTrans,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		if(colliders.isEmpty()){ return false; }
		
		//check if CollisionEllipsoid intersects transformed box
		if(boundingBox.getReverseContainment(movingEllipsoidBounds, objTrans) == BoxContainment.CONTAINMENT_NONE){ return false; }
		
		boolean result = false;
		for(Collider<Vector3d> c : colliders){
			//the order of operands of || here is important due to short-circuit evalution
			result = c.collide(collisionData, objTrans, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds) || result;
		}
		return result;
	}
}
