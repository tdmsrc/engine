package engine.collision;

import geometry.math.FixedDimensionalVector;
import geometry.math.ObjectTransformation3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;


public class ColliderNoClip<Vector extends FixedDimensionalVector<Vector>>
	implements Collider<Vector>{

	public ColliderNoClip(){ }
	
	@Override
	public void pickOccurred(Vector pickPosition){ 
		//never reports itself as the picked object, so this will never be called
	}

	@Override
	public boolean pick(PickData<Vector> pickData, 
		Vector p, Vector normalizedRay, float maxPickDistance, CullerEdge<Vector> rayCuller){

		return false;
	}
	
	@Override
	public boolean pick(PickData<Vector> pickData, ObjectTransformation3d objTransformation,
		Vector p, Vector normalizedRay, float maxPickDistance, CullerEdge<Vector> rayCuller){

		return false;
	}
	
	@Override
	public boolean collide(CollisionData<Vector> collisionData,
		Ellipsoid<Vector> ce, Vector movement, Vector movementr, float minimumMovementMultiple, Box<Vector> movingEllipsoidBounds){
		
		return false;
	}
	
	@Override
	public boolean collide(CollisionData<Vector> collisionData, ObjectTransformation3d objTransformation,
		Ellipsoid<Vector> ce, Vector movement, Vector movementr, float minimumMovementMultiple, Box<Vector> movingEllipsoidBounds){
		
		return false;
	}
}
