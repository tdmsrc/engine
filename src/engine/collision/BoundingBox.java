package engine.collision;

import java.util.EnumMap;

import engine.collision.Ellipsoid.EllipsoidPlaneCollisionData;
import engine.collision.Ellipsoid.SphereLineCollisionData;
import engine.collision.Ellipsoid.SpherePointCollisionData;
import geometry.math.ObjectTransformation3d;
import geometry.math.Vector3d;
import geometry.rawgeometry.ObjData;
import geometry.spacepartition.Box;
import geometry.spacepartition.Culler;
import geometry.spacepartition.CullerEdge;

/**
 * This is an extension of {@link Box} which implements the {@link Collider}
 * interface. That is, it allows collisions and picking, possibly with a transformation 
 * applied to the box.
 * <br><br>
 * Additionally, as {@link Culler}s do not have an option to apply a transformation to the 
 * box whose containment in the Culler's region is to be checked, this class
 * provides such methods for the {@link Box} culler and the {@link CullerEdge} culler.
 * 
 * @see {@link #getReverseContainment(Box, SceneObjectTransformation)}
 * @see {@link #getReverseContainment(CullerEdge, SceneObjectTransformation)}
 */
public class BoundingBox extends Box<Vector3d> implements Collider<Vector3d>{

	private static final float BOX_EPS = 0.01f; //min dims of box
	
	//standard basis vectors
	private static final Vector3d[] E = new Vector3d[]{
		new Vector3d(1,0,0), new Vector3d(0,1,0), new Vector3d(0,0,1)
	};
	
	//action to use when pick occurs
	private PickListener<Vector3d> pickListener;
	
	
	public BoundingBox(Vector3d min, Vector3d max, PickListener<Vector3d> pickListener){
		super(min, max);
		
		this.pickListener = pickListener;
	}
	
	public BoundingBox(Box<Vector3d> box, PickListener<Vector3d> pickListener){
		super(box.getMin(), box.getMax());
		
		this.pickListener = pickListener;
	}
	
	@Override
	public void pickOccurred(Vector3d pickPosition){
		pickListener.pickOccurred(pickPosition);
	}
	
	//==================================================
	// CREATING FROM OBJ DATA
	//==================================================

	/**
	 * Returns a BoundingBox which contains all the vertices in the specified ObjData. <br>
	 * Assumes that the ObjData is not empty.
	 */
	public static BoundingBox createFromObjData(ObjData objData, PickListener<Vector3d> pickListener){
		if((objData.position == null) || (objData.position.length == 0)){
			return new BoundingBox(new Vector3d(-BOX_EPS,-BOX_EPS,-BOX_EPS), new Vector3d(BOX_EPS,BOX_EPS,BOX_EPS), pickListener);
		}
		
		//initial bounds
		Vector3d v0 = objData.position[0];
		Vector3d boxMin = new Vector3d(v0.getX()-BOX_EPS, v0.getY()-BOX_EPS, v0.getZ()-BOX_EPS);
		Vector3d boxMax = new Vector3d(v0.getX()+BOX_EPS, v0.getY()+BOX_EPS, v0.getZ()+BOX_EPS);
		
		//expand to fit additional points
		for(int i=1; i<objData.position.length; i++){
			Vector3d v = objData.position[i];
			
			for(int k=0; k<3; k++){
				if(v.get(k) < boxMin.get(k)){ boxMin.set(k, v.get(k)); }
				if(v.get(k) > boxMax.get(k)){ boxMax.set(k, v.get(k)); }
			}
		}
		
		//return box
		return new BoundingBox(boxMin, boxMax, pickListener);
	}
	
	//====================================================
	// REVERSE CULLER IMPLEMENTATIONS WITH TRANSFORMATION
	//====================================================
	
	/**
	 * Determine whether this BoundingBox, with the transformation objTransformation applied,
	 * is contained completely, partially, or not at all in the {@link Box} "cullerBox".
	 * <br>
	 * If no transformation is to be applied to this BoundingBox, use
	 * cullerBox.getBoxContainment(this).
	 * <br>
	 * This method may return a greater degree of containment than is actually true, 
	 * but it may not return a lesser degree of containment.
	 *  
	 * @param cullerBox The Box whose containment of this transformed BoundingBox is to be checked.
	 * @param objTransformation The transformation to be applied to this BoundingBox.
	 * 
	 * @return A BoxContainment indicating the degree of containment.
	 * @see {@link Box#getBoxContainment(Box)}
	 */
	public BoxContainment getReverseContainment(Box<Vector3d> cullerBox, ObjectTransformation3d objTransformation){
		
		//METHOD:
		//BRIEFLY: reverse-transform each pair of axis-aligned planes of cullerBox;
		// check how this box is sandwiched between them.
		//IN DETAIL: let R be the rotation and T the translation for the transformation.
		// let m0 = invObjTrans(cullerBox.min); Pi = plane through m0, normal ni = R^{-1}*e_i
		// the oriented distance from a point p to that plane is (p-m0).n;
		// for box verts: \sum_k (M_k e_k - m0_k e_k).ni = \sum_k (M_k-m0_k)(e_k.n), M = min or max
		// max value occurs when M = max for e_k.n > 0, M = min else; min value occurs choosing reverse
		// let min_i, max_i = min and max oriented dists of points in this (untransformed) box
		// finally, return NONE if for ANY i, [min_i,max_i] \cap [0,cullerBox.dims_i] = empty
		// and return COMPLETE if for ALL i, [min_i,max_i] \subset [0,cullerBox.dims_i]; PARTIAL else
		
		Vector3d cullerBoxDims = cullerBox.getDimensions();
		Vector3d m0 = objTransformation.unApplyToPoint(cullerBox.getMin());

		boolean completelyContained = true;
		
		//check reverse-transformed pairs of faces of cullerBox 
		for(int i=0; i<3; i++){
			Vector3d n = objTransformation.unApplyToVector(E[i]);
			
			//compute max and min values, over all this box's verts, of dot(n[i], vert - m0)
			float maxDot = 0, minDot = 0;
			for(int k=0; k<3; k++){
				if(n.get(k) > 0){
					maxDot += n.get(k) * (getMax().get(k) - m0.get(k));
					minDot += n.get(k) * (getMin().get(k) - m0.get(k));
				}else{ 
					maxDot += n.get(k) * (getMin().get(k) - m0.get(k));
					minDot += n.get(k) * (getMax().get(k) - m0.get(k));
				}
			}
			
			//reject as NONE if possible; update completelyContained
			if((maxDot < 0) || (minDot > cullerBoxDims.get(i))){ return BoxContainment.CONTAINMENT_NONE; }
			completelyContained = completelyContained && ((minDot > 0) && (maxDot < cullerBoxDims.get(i)));
		}
		
		return completelyContained ? BoxContainment.CONTAINMENT_COMPLETE : BoxContainment.CONTAINMENT_PARTIAL;
	}
	
	/**
	 * Determine whether this BoundingBox, with the transformation objTransformation applied,
	 * is contained completely, partially, or not at all in the {@link CullerEdge} "cullerEdge".
	 * <br>
	 * If no transformation is to be applied to this BoundingBox, use 
	 * cullerEdge.getBoxContainment(this).
	 * <br>
	 * This method may return a greater degree of containment than is actually true, 
	 * but it may not return a lesser degree of containment.
	 * 
	 * @param cullerEdge The CullerEdge whose containment of this transformed BoundingBox is to be checked.
	 * @param objTransformation The transformation to be applied to this BoundingBox.
	 * 
	 * @return A BoxContainment indicating the degree of containment.
	 * @see {@link CullerEdge#getBoxContainment(Box)}
	 */
	public BoxContainment getReverseContainment(CullerEdge<Vector3d> cullerEdge, ObjectTransformation3d objTransformation){
		
		//get edge endpoints, with reverse transformation applied
		Vector3d pRT = objTransformation.unApplyToPoint(cullerEdge.getInitialPoint());
		Vector3d qRT = objTransformation.unApplyToPoint(cullerEdge.getFinalPoint());
		
		//containment determined by clipping reverse-transformed edge
		return clipEdge(pRT, qRT) ? 
			BoxContainment.CONTAINMENT_PARTIAL : BoxContainment.CONTAINMENT_NONE;
	}
	
	//==================================================
	// PICKING
	//==================================================

	@Override
	public boolean pick(PickData<Vector3d> pickData, 
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){

		return this.pick(pickData, null, false, p, normalizedRay, maxPickDistance, rayCuller);
	}
	
	@Override
	public boolean pick(PickData<Vector3d> pickData, ObjectTransformation3d objTransformation,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		return pick(pickData, objTransformation, true, p, normalizedRay, maxPickDistance, rayCuller);
	}
	
	/**
	 * Encompasses both transformed and non-transformed versions of {@link Collider#pick}
	 */
	private boolean pick(PickData<Vector3d> pickData, 
			ObjectTransformation3d objTransformation, boolean isTransformed,
		Vector3d p, Vector3d normalizedRay, float maxPickDistance, CullerEdge<Vector3d> rayCuller){
		
		//get edge final point
		Vector3d q = rayCuller.getFinalPoint();
		
		//edge endpoints (reverse-transformed if isTransformed)
		Vector3d pRT = isTransformed ? objTransformation.unApplyToPoint(p) : p;
		Vector3d qRT = isTransformed ? objTransformation.unApplyToPoint(q) : q;

		//vectors to hold clipped edge's endpoints
		Vector3d pClipRT = new Vector3d(), qClipRT = new Vector3d();
		
		//clip reverse-transformed edge
		if(clipEdge(pRT, qRT, pClipRT, qClipRT)){
			//get initial point of clipped edge, re-transformed if isTransformed
			Vector3d pClip = isTransformed ? p.lerp(q, pRT.unLerp(qRT, pClipRT)) : pClipRT;
			return pickData.reportPick(pClip.distance(p), pClip, this);
		}
		
		//no pick if reverse-transformed edge doesn't intersect
		return false;
	}
	
	
	//==================================================
	// COLLISION
	//==================================================
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData, 
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){

		return collide(collisionData, null, false, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
	}
	
	@Override
	public boolean collide(CollisionData<Vector3d> collisionData, ObjectTransformation3d objTransformation,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){

		return collide(collisionData, objTransformation, true, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
	}
	
	/**
	 * An enumeration of the vertices that occur in a flower of faces of a box about a central vertex.
	 * The vertex position is given by its offset values from the central vertex.
	 * For example, if only offsetX is true, this represents the vertex with y and z coordinates
	 * agreeing with the central vertex, but with x coordinate agreeing with that of the side
	 * of the box opposite the central vertex.
	 */
	private static enum SkeletonVertex{
		VERTEX_000{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return true; 
			}
			@Override public boolean offsetX(){ return false; }
			@Override public boolean offsetY(){ return false; }
			@Override public boolean offsetZ(){ return false; }
		}, 
		VERTEX_100{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[1] || boundaryPossible[2]; 
			}
			@Override public boolean offsetX(){ return true; }
			@Override public boolean offsetY(){ return false; }
			@Override public boolean offsetZ(){ return false; }
		}, 
		VERTEX_010{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[0] || boundaryPossible[2]; 
			}
			@Override public boolean offsetX(){ return false; }
			@Override public boolean offsetY(){ return true; }
			@Override public boolean offsetZ(){ return false; }
		}, 
		VERTEX_001{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[0] || boundaryPossible[1]; 
			}
			@Override public boolean offsetX(){ return false; }
			@Override public boolean offsetY(){ return false; }
			@Override public boolean offsetZ(){ return true; }
		}, 
		VERTEX_110{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[2]; 
			}
			@Override public boolean offsetX(){ return true; }
			@Override public boolean offsetY(){ return true; }
			@Override public boolean offsetZ(){ return false; }
		}, 
		VERTEX_101{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[1]; 
			}
			@Override public boolean offsetX(){ return true; }
			@Override public boolean offsetY(){ return false; }
			@Override public boolean offsetZ(){ return true; }
		}, 
		VERTEX_011{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[0]; 
			}
			@Override public boolean offsetX(){ return false; }
			@Override public boolean offsetY(){ return true; }
			@Override public boolean offsetZ(){ return true; }
		};
		
		/**
		 * Determine whether this vertex needs to be considered.
		 * 
		 * @param boundaryPossible Indicate which faces in the flower are relevant.
		 * @return True if the vertex should be considered, false otherwise.
		 */
		public abstract boolean checkRelevant(boolean[] boundaryPossible);
		
		/**
		 * True if the x value of this vertex is opposite that of the central vertex, false if it agrees.
		 */
		public abstract boolean offsetX();
		/**
		 * True if the y value of this vertex is opposite that of the central vertex, false if it agrees.
		 */
		public abstract boolean offsetY();
		/**
		 * True if the z value of this vertex is opposite that of the central vertex, false if it agrees.
		 */
		public abstract boolean offsetZ();
	}
	
	/**
	 * An enumeration of the edges that occur in a flower of faces of a box about a central vertex.
	 */
	private static enum SkeletonEdge{
		EDGE_110{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[0] || boundaryPossible[1];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_000; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_001; }
		},
		EDGE_101{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[0] || boundaryPossible[2];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_000; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_010; }
		},
		EDGE_011{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[1] || boundaryPossible[2];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_000; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_100; }
		},
		EDGE_100_A{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[0];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_011; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_010; }
		},
		EDGE_100_B{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[0];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_011; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_001; }
		},
		EDGE_010_A{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[1];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_101; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_100; }
		},
		EDGE_010_B{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[1];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_101; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_001; }
		},
		EDGE_001_A{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[2];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_110; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_100; }
		},
		EDGE_001_B{
			@Override public boolean checkRelevant(boolean[] boundaryPossible){
				return boundaryPossible[2];
			}
			@Override public SkeletonVertex getFinalPoint(){ return SkeletonVertex.VERTEX_110; }
			@Override public SkeletonVertex getInitialPoint(){ return SkeletonVertex.VERTEX_010; }
		};
		
		/**
		 * Determine whether this edge needs to be considered.
		 * 
		 * @param boundaryPossible Indicate which faces in the flower are relevant.
		 * @return True if the edge should be considered, false otherwise.
		 */
		public abstract boolean checkRelevant(boolean[] boundaryPossible);
		
		/**
		 * Get the initial point of this edge, as a {@link SkeletonVertex}
		 */
		public abstract SkeletonVertex getInitialPoint();
		/**
		 * Get the final point of this edge, as a {@link SkeletonVertex}.
		 */
		public abstract SkeletonVertex getFinalPoint();
	}
	
	/**
	 * This class holds a transformed vertex ("central vertex") and 3 transformed offset vectors.
	 * The offset vectors are the outward-pointing normals to each face occurring 
	 * in the flower at the central vertex.
	 * (i.e., offset[0] is the normal to the x-axis-aligned face containing the central vertex, etc).
	 * <br>
	 * This class can then compute each vertex of the transformed box by specifying its offset
	 * from the central vertex.  For example, if only offsetX is false, it will return the transformed
	 * vertex which originally had y and z components agreeing with the central vertex, but with 
	 * opposite x component.
	 * <br>
	 * The offsets are specified by passing a {@link SkeletonVertex}.
	 * <br>
	 * This class will also compute the vertices transformed and divided componentwise by the
	 * radii of the specified ellipsoid.
	 */
	private static class BoxTransformData{
		
		private Vector3d x;  private Vector3d[] offset;
		private Vector3d xr; private Vector3d[] offsetr;
		
		/**
		 * Create a new BoxTransformData.
		 * 
		 * @param x The transformed "central vertex".
		 * @param offset The transformed offset vectors.
		 * @param ellipsoid The ellipsoid specifying the radii to divide the transformed 
		 * vertices' components by for the {@link #getTRVertex} method. 
		 */
		public BoxTransformData(Vector3d x, Vector3d[] offset, Ellipsoid<Vector3d> ellipsoid){
			this.x = x;
			this.offset = offset;
			
			//divide p0 and fNScaled vectors componentwise by ellipsoid radii
			xr = x.copy(); xr.divideComponentWise(ellipsoid.getRadii());
			offsetr = new Vector3d[3];
			for(int i=0; i<3; i++){
				offsetr[i] = offset[i].copy();
				offsetr[i].divideComponentWise(ellipsoid.getRadii());
			}
		}
		
		/**
		 * Get the vertex of the transformed box specified by skeletonVertex.
		 */
		public Vector3d getTVertex(SkeletonVertex skeletonVertex){
			Vector3d ret = x.copy();
			if(skeletonVertex.offsetX()){ ret.subtract(offset[0]); }
			if(skeletonVertex.offsetY()){ ret.subtract(offset[1]); }
			if(skeletonVertex.offsetZ()){ ret.subtract(offset[2]); }
			return ret;
		}
		
		/**
		 * Get the vertex of the transformed box specified by skeletonVertex, with its
		 * components divided by the radii of the ellipsoid specified in the constructor.
		 */
		public Vector3d getTRVertex(SkeletonVertex skeletonVertex){
			Vector3d ret = xr.copy();
			if(skeletonVertex.offsetX()){ ret.subtract(offsetr[0]); }
			if(skeletonVertex.offsetY()){ ret.subtract(offsetr[1]); }
			if(skeletonVertex.offsetZ()){ ret.subtract(offsetr[2]); }
			return ret;
		}
	}
	
	/**
	 * Encompasses both transformed and non-transformed versions of {@link Collider#collide}
	 */
	private boolean collide(CollisionData<Vector3d> collisionData, 
		ObjectTransformation3d objTransformation, boolean isTransformed,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		//METHOD:
		// Find 3 faces such that their outward-pointing normals have <= 0 dot 
		// product with "movement"; get EllipsoidPlaneCollisionData for each one.
		// If the intersection lands in the interior of the face, return that and quit
		// (there is at most one such intersection, and this is the closest).
		// If for any face d > 1, impossible to hit box anywhere.
		// Now do "boundary phase": return nearest collision with vertex or edge incident
		// to a face for which there was a plane collision, but no face interior hit.
		
		//determine the 3 faces the ellipsoid is moving toward
		//e.g. the face in the plane x = box.(faceMax[0] ? max : min).get(0) is one such face 
		Vector3d vRT = isTransformed ? objTransformation.unApplyToVector(movement) : movement;
		boolean faceMax[] = new boolean[]{ vRT.getX() < 0, vRT.getY() < 0, vRT.getZ() < 0 };
				
		//point on intersection of all the faces, transformed if necessary
		Vector3d p0 = new Vector3d(
			(faceMax[0] ? max : min).get(0),
			(faceMax[1] ? max : min).get(1), 
			(faceMax[2] ? max : min).get(2)
		);
		if(isTransformed){ p0 = objTransformation.applyToPoint(p0); }
		
		//data to keep track of which faces need their boundaries checked and which don't
		boolean checkSkeleton = false;
		boolean boundaryPossible[] = new boolean[]{ false, false, false };
		
		//outward-pointing normals to the 3 faces; compute during loop below as needed
		//but store here, since if loop completes, they will be used later
		Vector3d[] faceNormal = new Vector3d[3];
		
		//get EllipsoidPlaneCollisionData for the faces and check for easy-outs
		for(int i=0; i<3; i++){
			//transform the necessary basis vector to obtain the normal; store it for later
			if(isTransformed){
				faceNormal[i] = objTransformation.applyToVector(E[i]);
				if(!faceMax[i]){ faceNormal[i].flip(); }
			}else{
				if(faceMax[i]){ faceNormal[i] = E[i]; } 
				else{ faceNormal[i] = E[i].copy(); faceNormal[i].flip(); }
			}
			
			//get EllipsoidPlaneCollisionData
			EllipsoidPlaneCollisionData planeCollisionData = 
				Ellipsoid.collideMovingEllipsoidWithPlane(ce, movement, p0, faceNormal[i]);
			
			//keep track of whether boundary should be checked: true if at least a plane collision
			//(see the conditions in collideMovingEllipsoidWithPlane for returning no collision)
			if(planeCollisionData.collisionOccurred){ 
				checkSkeleton = true; boundaryPossible[i] = true; 
			}
			else{ continue; }
			
			//if d > 1, no chance of hitting the box anywhere at all
			if(planeCollisionData.movementMultiple > 1){ return false; }
			
			//if d < 0, may still hit the boundary or another face, but not the face interior
			if(planeCollisionData.movementMultiple > minimumMovementMultiple){

				//get collision point (reverse-transformed if isTransformed)
				Vector3d x = planeCollisionData.collisionPosition;
				Vector3d xRT = isTransformed ? objTransformation.unApplyToPoint(x) : x;
				
				//check if collision point is in face interior
				//(check if reverse-transformed point is within box bounds for dimensions other than i)
				boolean inFace = true;
				for(int k=0; k<3; k++){
					if(k == i){ continue; }
					inFace = inFace && ((xRT.get(k) > min.get(k)) && (xRT.get(k) < max.get(k))); 
				}
				if(inFace){
					//report collision and quit
					return collisionData.reportCollision(planeCollisionData.movementMultiple, x);
				}
			}
		}
		
		//BOUNDARY PHASE
		//----------------------------------------------------------------------------
		//need not do anything here if none of the faces have boundaryPossible = true
		if(!checkSkeleton){ return false; }

		//scale the faceNormals by dimension, so vertices can be obtained by subtracting from p0
		Vector3d dims = getDimensions();
		Vector3d[] fNScaled = faceNormal; //(ok since faceNormal won't be used again)
		for(int i=0; i<3; i++){ fNScaled[i].scale(dims.get(i)); }
		
		//create the BoxTransformData so transformed and ellipsoid-radii-scaled vertices
		//can be obtained from a SkeletonVertex.
		BoxTransformData boxTransformData = new BoxTransformData(p0, fNScaled, ce);
		
		//add relevant skeleton verts
		EnumMap<SkeletonVertex,Vector3d> vertexT = new EnumMap<SkeletonVertex,Vector3d>(SkeletonVertex.class);
		EnumMap<SkeletonVertex,Vector3d> vertexTR = new EnumMap<SkeletonVertex,Vector3d>(SkeletonVertex.class);
		
		for(SkeletonVertex sv : SkeletonVertex.values()){
			if(!sv.checkRelevant(boundaryPossible)){ continue; }
			
			vertexT.put(sv, boxTransformData.getTVertex(sv));
			vertexTR.put(sv, boxTransformData.getTRVertex(sv));
		}

		//store best collision while looping through the vertices and edges that are 
		//on the boundary of a face marked as boundaryPossible = true (as computed above)
		//----------------------------------------------------------------------------
		boolean hitSkeleton = false;
		float dSkeletonMin = 0.0f;
		Vector3d xSkeletonMin = null;
		
		//VERTEX INTERSECTIONS
		for(SkeletonVertex sv : SkeletonVertex.values()){
			Vector3d wr = vertexTR.get(sv);
			if(wr == null){ continue; }
			
			SpherePointCollisionData vertexCollisionData = 
				Ellipsoid.collideMovingUnitSphereWithPoint(ce.getCenterDividedByRadii(), movementr, wr);
			
			if(!vertexCollisionData.collisionOccurred){ continue; }
			if(	(vertexCollisionData.movementMultiple < minimumMovementMultiple) || 
				(vertexCollisionData.movementMultiple > 1) ){ continue; } 
			
			//store closest hit
			if(!hitSkeleton || (vertexCollisionData.movementMultiple < dSkeletonMin)){
				hitSkeleton = true;
				dSkeletonMin = vertexCollisionData.movementMultiple;
				xSkeletonMin = vertexT.get(sv);
			}
		}
		
		//EDGE INTERSECTIONS
		for(SkeletonEdge se : SkeletonEdge.values()){
			if(!se.checkRelevant(boundaryPossible)){ continue; }
			
			SkeletonVertex sva = se.getInitialPoint(), svb = se.getFinalPoint();
			
			SphereLineCollisionData edgeCollisionData = 
				Ellipsoid.collideMovingUnitSphereWithLine(ce.getCenterDividedByRadii(), movementr, vertexTR.get(sva), vertexTR.get(svb));
			
			if(!edgeCollisionData.collisionOccurred){ continue; }
			if(	(edgeCollisionData.movementMultiple < minimumMovementMultiple) || 
				(edgeCollisionData.movementMultiple > 1) ){ continue; } 
			if(	(edgeCollisionData.lineLerp < 0) || 
				(edgeCollisionData.lineLerp > 1) ){ continue; } 
			
			//store closest hit
			if(!hitSkeleton || (edgeCollisionData.movementMultiple < dSkeletonMin)){
				hitSkeleton = true;
				dSkeletonMin = edgeCollisionData.movementMultiple;
				xSkeletonMin = vertexT.get(sva).lerp(vertexT.get(svb), edgeCollisionData.lineLerp);
			}
		}
		
		return hitSkeleton ? collisionData.reportCollision(dSkeletonMin, xSkeletonMin) : false;
	}
}
