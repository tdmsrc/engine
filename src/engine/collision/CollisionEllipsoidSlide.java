package engine.collision;

import geometry.math.Vector3d;

public class CollisionEllipsoidSlide<GL> extends CollisionEllipsoid<GL>{

	public CollisionEllipsoidSlide(Vector3d radii, float mass, Vector3d center, Vector3d velocity){
		super(radii, mass, center, velocity);
	}
	
	@Override
	protected Vector3d collisionResponse(CollisionData<Vector3d> collisionData, Vector3d movement){
		
		//get new center
		float d = collisionData.getCollisionMovementMultiple();
		Vector3d newCenter = movement.copy(); newCenter.scale(d); newCenter.add(getCenter());
		
		//get normal to tangent plane at intersection point
		Vector3d intRay = collisionData.getCollisionPoint().copy(); intRay.subtract(newCenter);
		Vector3d n = getTangentPlaneNormal(intRay);
		
		//project remaining movement onto tangent plane
		Vector3d tangentMovement = Vector3d.linearCombination(1.0f, movement, -movement.dot(n), n);
		tangentMovement.scale(1.0f-d);
		
		//project velocity onto tangent plane
		Vector3d tangentVelocity = Vector3d.linearCombination(1.0f, velocity, -velocity.dot(n), n);
		setVelocity(tangentVelocity);
		
		//float NEW_CENTER_EPS away from contact point
		Vector3d newCenterAdjust = n.copy(); newCenterAdjust.flip();
		newCenterAdjust.scale(NEW_CENTER_OFFSET);
		newCenter.add(newCenterAdjust);

		//set new state and recurse
		setCenter(newCenter);
		return tangentMovement;
	}
}
