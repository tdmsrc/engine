package engine.collision;

import geometry.math.FixedDimensionalVector;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;

//this represents an ellipsoid bounding region.

//In particular, represents the ellipsoid given by the set of x
//such that \sum (x_i / r_i)^2 = 1, offset by "center"

public class Ellipsoid<Vector extends FixedDimensionalVector<Vector>>{
	
	private Vector center;
	private Vector radii;
	private Vector cr; //center, divided componentwise by radii (often needed in "collide" methods)
	

	public Ellipsoid(Vector radii, Vector center){
		
		this.radii = radii;
		this.center = center;
		updateCR();
	}
	
	public Vector getRadii(){ return radii; }
	public Vector getCenter(){ return center; }
	
	public Vector getCenterDividedByRadii(){ return cr; }
	
	public void setRadii(Vector radii){ this.radii = radii; updateCR(); }
	public void setCenter(Vector center){ this.center = center; updateCR(); }
	
	public void move(Vector movement){ center.add(movement); updateCR(); }
	
	private void updateCR(){
		cr = center.copy(); cr.divideComponentWise(radii);
	}

	
	//======================================
	// BASIC ELLIPSOID METHODS
	//======================================
	
	/**
	 * Return a box which contains every point of the form x+t*movement,
	 * where x is a point on the ellipsoid, and t \in [0,1].
	 */
	protected Box<Vector> getBoundingBox(Vector movement){
		
		Vector min = center.copy();
		Vector max = center.copy();
		
		int n = min.getDimension();
		for(int i=0; i<n; i++){
			Vector mod = (movement.get(i) > 0) ? max : min;
			mod.set(i, mod.get(i) + movement.get(i));
		}
		
		min.subtract(radii); max.add(radii);
		return new Box<Vector>(min, max);
	}

	/**
	 * Given a ray based at the center of the ellipsoid, return the
	 * normal to the tangent plane at the intersection of the ray and ellipsoid
	 */
	public Vector getTangentPlaneNormal(Vector ray){
		
		Vector n = ray.copy();
		n.divideComponentWise(radii);
		n.divideComponentWise(radii);
		n.normalize();
		return n;
	}
	
	/**
	 * Returns the offset (i.e., from "center") of the point on the ellipsoid 
	 * which minimizes the function sending x to (x-o).n, for any o.  
	 * 
	 * In other words, if the ellipsoid is on the positive side of a plane with
	 * normal n, this returns the offset of the closest point to the plane on the ellipsoid.
	 */
	public Vector getClosestPointToPlane(Vector n){
		
		//first, find the critical points of f(x) = (x-o).n restricted to the ellipse,
		//which is a level set of g(x) = \sum x_i^2/r_i^2; using lagrange multipliers
		//the points are lambda*<...n_i*r_i^2...>, where lambda = +-1/|<...n_i r_i...>|
		//the negative lambda will yield the smallest value of f.
		
		Vector x = n.copy(); 
		x.multiplyComponentWise(radii);
		float lambda = -1.0f/x.length();
		x.multiplyComponentWise(radii);
		x.scale(lambda);
		return x;
	}
	
	
	//=========================================
	// MOVING SPHERE/ELLIPSOID INTERSECTIONS 
	//=========================================
	
	//the reason some are spheres and not ellipsoids is that the ellipsoid check
	//requires division componentwise by the ellipsoid radii; in e.g. ColliderTriangle
	//this would mean repeating such divisions on the triangle vertices multiple times
	//when instead it can be precomputed and passed to the sphere methods
	
	/**
	 * A data structure holding the result of 
	 * {@link Ellipsoid#collideMovingUnitSphereWithPoint}
	 * 
	 * @see {@link Ellipsoid#collideMovingUnitSphereWithPoint}
	 */
	public static class SpherePointCollisionData{
		
		public boolean collisionOccurred;
		public float movementMultiple;
		
		private SpherePointCollisionData(boolean collisionOccurred, float movementMultiple){
			this.collisionOccurred = collisionOccurred;
			this.movementMultiple = movementMultiple;
		}
		
		public static SpherePointCollisionData noCollision(){
			return new SpherePointCollisionData(false, 0);
		}
		
		public static SpherePointCollisionData collision(float movementMultiple){
			return new SpherePointCollisionData(true, movementMultiple);
		}
	}
	
	/**
	 * Determine the least value of d, if any, such that the unit sphere
	 * centered at center+d*movement intersects the point x.
	 * Assumes movement has length > 0.
	 * <br><br>
	 * To figure out the corresponding problem for a moving ellipsoid,
	 * pass to this method the center of the ellipsoid, movement vector, and point,
	 * each divided componentwise by the radii of the ellipsoid.
	 * 
	 * @param center The center of the unit sphere in its initial position (i.e., at d=0).
	 * @param movement The vector along which the unit sphere is moving.
	 * @param x The point with which the unit sphere is to collide.
	 * @return Returns a {@link SpherePointCollisionData} indicating whether a collision
	 * occurred, and if so, the value of d (movementMultiple) described above.
	 * 
	 * @see {@link Ellipsoid#getCenterDividedByRadii()}
	 */
	public static SpherePointCollisionData collideMovingUnitSphereWithPoint(
		Vector3d center, Vector3d movement, Vector3d x){
		
		//METHOD:
		// to check when a unit sphere at c+d*v hits the point q, let u = q-c, v = movement;
		// then find where |u-d*v|^2 = 1; this is a quadratic in d;
		// d = (u.v +- sqrt(D))/|v|^2, D = |v|^2-|uxv|^2  [|uxv|^2 = |u|^2|v|^2-(u.v)^2]
		
		Vector3d u = x.copy(); u.subtract(center);
		float uDotv = u.dot(movement);
		float ulen2 = u.lengthSquared();
		float vlen2 = movement.lengthSquared();
		
		//compute d if it exists
		float discr = vlen2*(1.0f - ulen2) + uDotv*uDotv;
		if(discr < 0){ return SpherePointCollisionData.noCollision(); }
		
		float d = (uDotv - (float)Math.sqrt(discr)) / vlen2;
		return SpherePointCollisionData.collision(d);
	}
	
	/**
	 * A data structure holding the result of 
	 * {@link Ellipsoid#collideMovingUnitSphereWithLine}
	 * 
	 * @see {@link Ellipsoid#collideMovingUnitSphereWithLine}
	 */
	public static class SphereLineCollisionData{
		
		public boolean collisionOccurred;
		public float movementMultiple, lineLerp;
		
		private SphereLineCollisionData(boolean collisionOccurred, 
			float movementMultiple, float lineLerp){
			
			this.collisionOccurred = collisionOccurred;
			this.movementMultiple = movementMultiple;
			this.lineLerp = lineLerp;
		}
		
		public static SphereLineCollisionData noCollision(){
			return new SphereLineCollisionData(false, 0, 0);
		}
		
		public static SphereLineCollisionData collision(float movementMultiple, float lineLerp){
			return new SphereLineCollisionData(true, movementMultiple, lineLerp);
		}
	}
	
	/**
	 * Determine the least value of d, if any, such that the unit sphere
	 * centered at center+d*movement intersects the line containing a and b.
	 * Also determines the line lerp value of that intersection point; i.e.,
	 * the value t such that t*b+(1-t)*a intersects the offset sphere.
	 * Assumes movement has length > 0.
	 * <br><br>
	 * To figure out the corresponding problem for a moving ellipsoid,
	 * pass to this method the center of the ellipsoid, movement vector, and points
	 * on the line, each divided componentwise by the radii of the ellipsoid.
	 * 
	 * @param center The center of the unit sphere in its initial position (i.e., at d=0).
	 * @param movement The vector along which the unit sphere is moving.
	 * @param a A point on the line.
	 * @param b Another point on the line.
	 * @return Returns a {@link SphereLineCollisionData} indicating whether a collision
	 * occurred, and if so, the values of d (movementMultiple) and t (lineLerp)
	 * described above.
	 * 
	 * @see {@link Ellipsoid#getCenterDividedByRadii()}
	 */
	public static SphereLineCollisionData collideMovingUnitSphereWithLine(
		Vector3d center, Vector3d movement, Vector3d a, Vector3d b){
		
		//METHOD:
		// to check when a unit sphere at c+d*v hits the line, let u = tb+(1-t)a-c, v = movement;
		// then find where |d*v-u(t)|^2 = 1; this is quadratic in t (viewing d as fixed);
		// we want the line to be tangent to ellipse, i.e. exactly one solution,
		// i.e., set discriminant D (a function of d) equal to zero:
		// end up with nastyish formulas below for d; then t = (dv+(c-a)).(b-a)/|b-a|^2
		
		Vector3d ca = center.copy(); ca.subtract(a); 
		Vector3d e = b.copy(); e.subtract(a);
		
		float vlen2 = movement.lengthSquared();
		float calen2 = ca.lengthSquared(); //<-[TODO] check if 0; may have degenerate edge
		float elen2 = e.lengthSquared();
		
		float vca = movement.dot(ca), ve = movement.dot(e), eca = ca.dot(e);

		float c0 = elen2*(calen2 - 1.0f) - eca*eca; //|(b-a) x (c-a)|^2 - |b-a|^2
		float c1 = vca*elen2 - ve*eca; //det([ v ][ c-a ] * [ c-a ][ b-a ]^T)
		float c2 = vlen2*elen2 - ve*ve; //|v x (b-a)|^2
		
		//"no collision" if c2 == 0, i.e., v and b-a are multiples (often, since both often multiples of up)
		if(c2 == 0.0f){ return SphereLineCollisionData.noCollision(); }
		
		float discr = c1*c1 - c0*c2;
		if(discr < 0){ return SphereLineCollisionData.noCollision(); }
		
		float dNumer = -c1 - (float)Math.sqrt(discr);
		float d = dNumer / c2;
		float t = (d*ve + eca) / elen2;
		return SphereLineCollisionData.collision(d, t);
	}
	
	/**
	 * A data structure holding the result of 
	 * {@link Ellipsoid#collideMovingEllipsoidWithPlane}
	 * 
	 * @see {@link Ellipsoid#collideMovingEllipsoidWithPlane}
	 */
	public static class EllipsoidPlaneCollisionData{
		
		public boolean collisionOccurred;
		public float movementMultiple;
		public Vector3d collisionPosition;
		
		private EllipsoidPlaneCollisionData(boolean collisionOccurred, 
			float movementMultiple, Vector3d collisionPosition){
			
			this.collisionOccurred = collisionOccurred;
			this.movementMultiple = movementMultiple;
			this.collisionPosition = collisionPosition;
		}
		
		public static EllipsoidPlaneCollisionData noCollision(){
			return new EllipsoidPlaneCollisionData(false, 0, null);
		}
		
		public static EllipsoidPlaneCollisionData collision(float movementMultiple, Vector3d collisionPosition){
			return new EllipsoidPlaneCollisionData(true, movementMultiple, collisionPosition);
		}
	}
	
	/**
	 * Determine the least value of d, if any, such that the specified ellipsoid,
	 * offset by d*movement, intersects the oriented plane with normal n passing 
	 * through p0.  Also determines the intersection point.
	 * Assumes movement has length > 0.
	 * <br>
	 * Returns "no collision" if: 
	 * <ul>
	 * <li>Movement is in the same direction as the plane's normal,
	 * <li>The ellipsoid is contained entirely on the negative side of the plane.
	 * </ul>
	 * 
	 * @param ellipsoid The ellipsoid in its initial position (i.e., at d=0).
	 * @param movement The vector along which the ellipsoid is moving.
	 * @param p0 A point on the plane.
	 * @param n The normal to the plane.
	 * @return Returns a {@link EllipsoidPlaneCollisionData} indicating whether a collision
	 * occurred, and if so, the value of d (movementMultiple) described above, and
	 * the intersection point (collisionPosition).
	 */
	public static EllipsoidPlaneCollisionData collideMovingEllipsoidWithPlane(
		Ellipsoid<Vector3d> ellipsoid, Vector3d movement, Vector3d p0, Vector3d n){
		
		//METHOD
		// do nothing if movement is in the same direction as n
		// otherwise, let v = movement, xMin = ellipsoid.center + getClosestPointToPlane;
		// find d such that xMin+d*v = p0+t; d = (p0-xMin).n / v.n
		
		//first of all, if ellipsoid is not moving "into" the plane, no collision
		float vDotN = movement.dot(n);
		if(vDotN >= 0){ return EllipsoidPlaneCollisionData.noCollision(); }
		
		//offset of point x on ellipsoid minimizing (x-p0).n; 
		//the points on ellipsoid minimizing and maximizing are c+x and c-x, respectively
		Vector3d x = ellipsoid.getClosestPointToPlane(n);
		
		//first get max point and max value
		Vector3d xMaxMinusP0 = ellipsoid.getCenter().copy(); xMaxMinusP0.subtract(x); xMaxMinusP0.subtract(p0);
		float dPlaneMax = xMaxMinusP0.dot(n);
		//if dPlaneMax < 0, the ellipsoid is entirely on negative side of plane; return "no collision"
		if(dPlaneMax < 0){ return EllipsoidPlaneCollisionData.noCollision(); }
		
		//now get min point offset, min point, and min value
		Vector3d xMin = ellipsoid.getCenter().copy(); xMin.add(x);
		Vector3d xMinMinusP0 = xMin.copy(); xMinMinusP0.subtract(p0);
		float dPlaneMin = xMinMinusP0.dot(n);
		
		//return data
		float d = -dPlaneMin / vDotN;
		Vector3d collisionPosition = movement.copy(); collisionPosition.scale(d); collisionPosition.add(xMin);
		return EllipsoidPlaneCollisionData.collision(d, collisionPosition);
	}
}
