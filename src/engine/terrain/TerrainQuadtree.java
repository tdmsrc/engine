package engine.terrain;

import java.util.EnumMap;

import engine.renderer.DrawableIBO;
import engine.renderer.GenericBuffer;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.QuadtreeChild;
import geometry.spacepartition.QuadtreeNodeCullable;
import geometry.spacepartition.QuadtreeNodeTraversal;


public class TerrainQuadtree extends QuadtreeNodeCullable<TerrainQuadtree, Vector3d>{

	private Box<Vector2d> tileLerp;
	private Box<Vector3d> tileBounds;

	//parent
	private TerrainQuadtree parent;
	private QuadtreeChild childType;
	
	//neighbors (for appropriate LOD stitches)
	private EnumMap<QuadtreeNeighbor,TerrainQuadtree> neighbors;
	
	//children
	private boolean isLeaf;
	private EnumMap<QuadtreeChild,TerrainQuadtree> children;

	
	//==========================================
	// CONSTRUCTOR
	//==========================================
	
	public TerrainQuadtree(int depth, TerrainMetrics terrainMetrics, Box<Vector2d> tileLerp){
		//initialize everything except neighbors, and recursively create children
		this(depth, null, null, terrainMetrics, tileLerp);
		
		//find neighbor links (children must all be created first)
		findNeighbors();
	}

	private TerrainQuadtree(
		int depth, TerrainQuadtree parent, QuadtreeChild childType, 
		TerrainMetrics terrainMetrics, Box<Vector2d> tileLerp){
		
		this.parent = parent;
		this.childType = childType;
		
		//compute tile bounds
		this.tileLerp = tileLerp;
		
		float hmin = 0.0f, hmax = terrainMetrics.getHeightScale() * 256.0f;
		Vector2d tilePosMin = terrainMetrics.getTerrainPosBounds().lerp(tileLerp.getMin());
		Vector2d tilePosMax = terrainMetrics.getTerrainPosBounds().lerp(tileLerp.getMax());
		tileBounds = new Box<Vector3d>(
			new Vector3d(tilePosMin.getX(),hmin,-tilePosMax.getY()), 
			new Vector3d(tilePosMax.getX(),hmax,-tilePosMin.getY()));
		
		//make children, if necessary
		isLeaf = (depth == 0);
		
		if(depth > 0){
			children = new EnumMap<QuadtreeChild,TerrainQuadtree>(QuadtreeChild.class);
			for(QuadtreeChild child : QuadtreeChild.values()){
				children.put(child, 
					new TerrainQuadtree(depth-1, this, child, terrainMetrics, child.getSubBox(tileLerp))
				);
			}
		}
		else{ children = null; }
	}
	
	private void findNeighbors(){
		
		neighbors = this.getNeighbors();
		
		//recurse
		if(isLeaf){ return; }
		for(QuadtreeChild child : QuadtreeChild.values()){
			children.get(child).findNeighbors();
		}
	}

	//==========================================
	// BASIC METHODS, QUADTREE NODE INTERFACE
	//==========================================
	
	@Override
	protected TerrainQuadtree getThisNode(){ return this; }
	
	@Override
	protected TerrainQuadtree getParent(){ return parent; }
	
	@Override
	protected QuadtreeChild getChildType(){ return childType; }
	
	@Override
	protected boolean isLeaf(){ return isLeaf; }
	
	@Override
	protected TerrainQuadtree getChild(QuadtreeChild child){ return children.get(child); }

	@Override
	protected Box<Vector3d> getCullBounds(){ return tileBounds; }

	
	public Box<Vector2d> getTileLerp(){ return tileLerp; }
	
	public Box<Vector3d> getTileBounds(){ return tileBounds; }
	
	private static final int maxLOD = 6;
	private static final int minLOD = 2;
	private static final float minLODDistance = 1024.0f;
	private static final float slopeLOD = (maxLOD - minLOD)/minLODDistance;
	
	private int getLODFromPoint(Vector3d p){
		
		float d = tileBounds.getCenter().distance(p);
		int lod = maxLOD - (int)(d*slopeLOD);
		if(lod < minLOD){ lod = minLOD; }
		return lod;
	}
	
	public <GL, Buffer extends GenericBuffer<GL>> DrawableIBO<GL,Buffer> 
		getIBO(Vector3d lodPosition, TerrainTileData<GL,Buffer> tileData){
		
		int lod = getLODFromPoint(lodPosition);
		
		if(lod == 0){ return tileData.getIBOLevel0(); }
		
		TerrainQuadtree neighbor;
		neighbor = neighbors.get(QuadtreeNeighbor.NEIGHBOR_XMAX);
		boolean coarse_XMAX = ((neighbor != null) && (neighbor.getLODFromPoint(lodPosition) < lod));
		neighbor = neighbors.get(QuadtreeNeighbor.NEIGHBOR_XMIN);
		boolean coarse_XMIN = ((neighbor != null) && (neighbor.getLODFromPoint(lodPosition) < lod));
		neighbor = neighbors.get(QuadtreeNeighbor.NEIGHBOR_YMAX);
		boolean coarse_YMAX = ((neighbor != null) && (neighbor.getLODFromPoint(lodPosition) < lod));
		neighbor = neighbors.get(QuadtreeNeighbor.NEIGHBOR_YMIN);
		boolean coarse_YMIN = ((neighbor != null) && (neighbor.getLODFromPoint(lodPosition) < lod));
		
		return tileData.getIBO(lod, coarse_XMAX, coarse_XMIN, coarse_YMAX, coarse_YMIN);
	}
	
	//==========================================
	// INTERFACE TO UPDATE NODES' HEIGHT BOUNDS
	//==========================================
	
	public static interface SetHeightBoundsMethod{
		/**
		 * Return min and max height for the Terrain over the tile with the specified lerp bounds.
		 * The height should be scaled according to TerrainMetrics.heightScale.
		 */
		public Vector2d getHeightBounds(Box<Vector2d> tileLerp);
	}
	
	public void setHeightBounds(final SetHeightBoundsMethod method, final float heightScale){
		
		//use method.getHeightBounds to set height bounds for leaves
		this.traverse(new QuadtreeNodeTraversal<TerrainQuadtree>(){
			@Override
			public void handleQuadtreeNode(TerrainQuadtree node){
				Vector2d heightBounds = method.getHeightBounds(node.tileLerp);
				node.tileBounds.getMin().setY(heightBounds.getX());
				node.tileBounds.getMax().setY(heightBounds.getY());
			}
		}, true, QuadtreeChild.values());
		
		//set height bounds for every node using min/max among children
		setHeightBoundsFromLeavesRecurse();
	}
	
	private Vector2d setHeightBoundsFromLeavesRecurse(){

		//if leaf, return the height bounds
		if(isLeaf){
			return new Vector2d(tileBounds.getMin().getY(), tileBounds.getMax().getY());
		}
		
		//else, get the height bounds of children, and find overall min and max; set and return
		boolean hadFirst = false;
		float maxChildHeight = 0.0f, minChildHeight = 0.0f;
		
		for(QuadtreeChild child : QuadtreeChild.values()){
			Vector2d childHeightBounds = children.get(child).setHeightBoundsFromLeavesRecurse();
			
			if(!hadFirst || (childHeightBounds.getX() < minChildHeight)){
				minChildHeight = childHeightBounds.getX();
			}
			if(!hadFirst || (childHeightBounds.getY() > maxChildHeight)){
				maxChildHeight = childHeightBounds.getY();
			}
			
			hadFirst = true;
		}
		
		tileBounds.getMin().setY(minChildHeight);
		tileBounds.getMax().setY(maxChildHeight);
		return new Vector2d(minChildHeight, maxChildHeight);
	}
	
}
