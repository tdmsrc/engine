package engine.terrain;

import engine.collision.Collider;
import engine.collision.ColliderGroup;
import engine.collision.PickData;
import engine.collision.PickListener;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericTexture;
import engine.renderer.GenericBuffer.BufferConstructor;
import engine.scene.DrawOptions;
import engine.scene.SceneEntity;
import engine.terrain.TerrainQuadtree.SetHeightBoundsMethod;
import engine.toolsAWT.BufferTexture;
import geometry.common.MessageOutput;
import geometry.math.FixedDimensionalVector;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.CullerEdge;
import geometry.spacepartition.QuadtreeChild;
import geometry.spacepartition.QuadtreeNodeTraversal;

//Basic algorithm:

//Start with a quadtree.  In each node of the quadtree, have a min/max height, forming a box.

//When culling, use the box intersections; false positives are fine.
//When doing collision detection, can cull the object first using box intersections.
//Then, just have to check the collision on single terrain tiles with the object.
//
//Need to be able to collide a bounding box mesh + ray of movement,
//and a sphere + ray of movement.
//

//When rendering, first cull, then compute LOD for each *vertex* of each tile.
//(The LOD should be some function dist -> [0,maxLOD], with 0->maxLOD, maxDist->0LOD)
//
//If a square/node is LOD 0, then simply don't split it up anymore.
//This way, even with small leaves, very big LOD 0 squares can still occur.
//
//Also, if it ends up that every leaf under a square is visible
//and would have the same LOD, then simply draw a single tile at that square at the appropriate LOD.



//It's clear what the problem is with very big leaves--not enough will get culled, LOD will be off, etc.
//What downside is there to having very small leaves?
//It is ideal to have tiny leaves, but the recursion will take too long, and it will take too much memory.
//
//If the target is for e.g. 2kmx2km with metrics as they currently are (1 px data map = 0.5 m)
//How deep should the quadtree be?
//
//if W is tile width, tree depth = log_2 (2048/W), (2048/W)x(1024/W) leaves, (4*tiles-1)/3 nodes+leaves 
//so if W = 64 meters, tree depth 5, 1024 leaves, 1365 nodes+leaves, fine
//
//right now tri leg has length 8
//that would be LOD2 for a 128x128 tile
//
//


/*2 data textures

Height+auxilliary: RGB with *no* filtering (packed values should not be interpolated)
- R,G:	packed 16 bit height value (for R,G \in [0,1], height = R*255+G)
- B:	auxilliary byte

Normal+blend: RGBA with bilinear filtering
- R,G:	normal.x and normal.z
- B,A:	blend factors for 2 textures (coeff of 3rd texture is 1-b-a or something)*/


public class Terrain<GL,
	Buffer extends GenericBuffer<GL>, 
	Texture extends GenericTexture<GL>>

	extends SceneEntity{
	
	private static final int MAX_LOD = 6;
	private static final int TREE_DEPTH = 4;
	
	private Buffer dataPositionBuffer;
	private TerrainTileData<GL,Buffer> tileData;
	
	private TerrainQuadtree tree;
	private TerrainMetrics terrainMetrics;
	
	//a re-usable quadtree that covers a leaf of this terrain, the leaves of which are the
	//subtiles of this terrain (i.e., the 8-triangle meshes that occur at the highest LOD)
	//[TODO] need to recreate this if MAX_LOD changes
	private TerrainLeafQuadtree subtileTree;
	
	//a few precomputed values depending on MAX_LOD, TREE_DEPTH, and terrainMetrics
	private int subtileCount; //terrain is (subtileCount X subtileCount) grid of subtiles
	private int terrainPixelMinX, terrainPixelMinY;
	private float subtilePixelWidth, subtilePixelHeight;
	
	//textures and drawoptions
	private DrawOptions drawOptions;
	private TerrainMaterial<Texture> material;
	private Texture dataHeightAndAux, dataNormalAndBlend;
	
	private PickListener<Vector3d> pickListener;
	
	
	public Terrain(GL gl, String name,
		DrawOptions drawOptions, TerrainMaterial<Texture> material, boolean constructWireframeData,
		TerrainMetrics terrainMetrics, Texture dataHeightAndAux, Texture dataNormalAndBlend,
		BufferConstructor<GL,Buffer> bufferConstructor){

		super(name);
		
		this.drawOptions = drawOptions;
		this.material = material;
		
		this.dataHeightAndAux = dataHeightAndAux;
		this.dataNormalAndBlend = dataNormalAndBlend;
		
		this.terrainMetrics = terrainMetrics;
		updateTerrainMetrics();
		
		tree = new TerrainQuadtree(TREE_DEPTH, terrainMetrics,
			new Box<Vector2d>(new Vector2d(0.0f, 0.0f), new Vector2d(1.0f, 1.0f)));
		
		subtileTree = new TerrainLeafQuadtree(this, MAX_LOD);
		
		dataPositionBuffer = TerrainTileFactory.getDataPositionBuffer(gl, MAX_LOD, bufferConstructor);
		tileData = new TerrainTileData<GL,Buffer>(gl, MAX_LOD, constructWireframeData, bufferConstructor);
	}
	
	/**
	 * Set the PickListener for this Terrain.
	 */
	public void setPickListener(PickListener<Vector3d> pickListener){
		this.pickListener = pickListener;
	}
	
	/**
	 * Update various precomputed values which are used to construct the
	 * Collider and which depend on the Terrain's metrics.
	 */
	private void updateTerrainMetrics(){
		
		//terrain has subtileCount^2 total "sub tiles", each looks like an 8-tri LOD 0 tile 
		int leafGridSize = (1 << TREE_DEPTH);
		subtileCount = leafGridSize << MAX_LOD; //= 2^lgs*2^mlod 
		
		//get minimum texture coordinate, in pixels, for terrain
		Vector2d terrainDataMin = terrainMetrics.getTerrainDataTexBounds().getMin();
		terrainPixelMinX = (int)(dataHeightAndAux.getWidth() * terrainDataMin.getX());
		terrainPixelMinY = (int)(dataHeightAndAux.getHeight() * terrainDataMin.getY());
		
		//get pixel size of one subtile
		Vector2d terrainDataDims = terrainMetrics.getTerrainDataTexBounds().getDimensions();
		subtilePixelWidth = dataHeightAndAux.getWidth() * terrainDataDims.getX() / (float)subtileCount;
		subtilePixelHeight = dataHeightAndAux.getHeight() * terrainDataDims.getY() / (float)subtileCount;
	}
	
	//get metrics and VBO/IBOs
	public TerrainMetrics getMetrics(){ return terrainMetrics; }
	
	public Buffer getPositionBuffer(){ return dataPositionBuffer; }
	public TerrainTileData<GL,Buffer> getTileData(){ return tileData; }
	
	//get draw options
	public DrawOptions getDrawOptions(){ return drawOptions; }
	
	//get material
	public TerrainMaterial<Texture> getMaterial(){ return material; }
	
	//get data textures
	public Texture getDataHeightAndAux(){ return dataHeightAndAux; }
	public Texture getDataNormalAndBlend(){ return dataNormalAndBlend; }
	
	//get quadtree root
	public TerrainQuadtree getQuadtree(){ return tree; }

	
	//=========================================================
	// QUADTREE TILE HEIGHT BOUNDS
	//=========================================================
	
	/**
	 * The Terrain is split into a quadtree of tiles.  This method will read the heightmap
	 * texture and update the height range occurring in each tile (it is necessary for
	 * this to be up to date, for correctness and efficiency for culling Terrain tiles).
	 */
	public void updateTileHeightBounds(final GL gl){
		
		MessageOutput.printDebug("Setting terrain quadtree node height bounds...");
		
		final Terrain<GL,Buffer,Texture> thisTerrain = this;
		final BufferTexture texHeightmapSample = new BufferTexture(1);
		
		tree.setHeightBounds(new SetHeightBoundsMethod(){
			@Override
			public Vector2d getHeightBounds(Box<Vector2d> tileLerp){
				return thisTerrain.getHeightBounds(gl, tileLerp, texHeightmapSample);
			}
		}, terrainMetrics.getHeightScale());
		
		MessageOutput.printDebug("Done.");
	}
	
	private Vector2d getHeightBounds(GL gl, Box<Vector2d> tileLerp, BufferTexture texHeightmapSample){
		
		//compute pixel metrics for tile specified by tileLerp
		Vector2d heightTexMin = terrainMetrics.getTerrainDataTexBounds().lerp(tileLerp.getMin());
		Vector2d heightTexMax = terrainMetrics.getTerrainDataTexBounds().lerp(tileLerp.getMax());
		
		int pixelMinX = (int)(dataHeightAndAux.getWidth()  * heightTexMin.getX());
		int pixelMaxX = (int)(dataHeightAndAux.getWidth()  * heightTexMax.getX());
		int pixelMinY = (int)(dataHeightAndAux.getHeight() * heightTexMin.getY());
		int pixelMaxY = (int)(dataHeightAndAux.getHeight() * heightTexMax.getY());
		
		int pixelWidth  = pixelMaxX - pixelMinX;
		int pixelHeight = pixelMaxY - pixelMinY;
		
		//read tile's portion of heightmap into BufferTexture and find bounds
		dataHeightAndAux.getAsBufferTexture(gl, texHeightmapSample, pixelMinX, pixelMinY, pixelWidth, pixelHeight);
		
		float hMax = -1.0f, hMin = 257.0f; //min and max unscaled height
		for(int i=0; i<pixelWidth; i++){
		for(int j=0; j<pixelHeight; j++){
			
			int r = texHeightmapSample.getR(i,j);
			int g = texHeightmapSample.getG(i,j);
			float h = (float)r + (float)g/255.0f; //unscaled height
			
			if(h > hMax){ hMax = h; } 
			if(h < hMin){ hMin = h; }
		}}
		
		//failsafe to ensure max >= min
		if(hMax < hMin){ 
			hMin = 0.5f * (hMax + hMin);
			hMax = hMin;
		}
		//scale and store
		Vector2d heightBounds = new Vector2d(hMin, hMax);
		heightBounds.scale(terrainMetrics.getHeightScale());
		
		//return
		return heightBounds;
	}
	
	
	//=========================================================
	// PICKING
	//=========================================================

	/**
	 * Compatible with {@link Collider#pick(PickData, FixedDimensionalVector, FixedDimensionalVector, float, CullerEdge)}.
	 */
	public boolean pick(final GL gl, final PickData<Vector3d> pickData, 
		final Vector3d p, final Vector3d normalizedRay, float maxPickDistance, final CullerEdge<Vector3d> rayCuller){
		
		//get ray endpoint at maxPickDistance
		final Vector3d q = normalizedRay.copy(); q.scale(maxPickDistance); q.add(p);
		
		//get endpoints in terrain coords
		Vector2d p2d = new Vector2d(p.getX(), -p.getZ());
		Vector2d q2d = new Vector2d(q.getX(), -q.getZ());
		
		final Vector2d ray2d = q2d.copy(); ray2d.subtract(p2d);
		final CullerEdge<Vector2d> rayCuller2d = new CullerEdge<Vector2d>(p2d, q2d);

		//get quadtree child order from ray and terrain dims
		final QuadtreeChild[] childOrder = new QuadtreeChild[4];
		QuadtreeChild.sort(ray2d, terrainMetrics.getTerrainPosBounds().getDimensions(), childOrder);
		
		//prepare a buffertexture to store subtile sample
		final BufferTexture texHeightmapSample = new BufferTexture(1);
	
		//the real purpose of using the quadtree here is to avoid checking subtiles
		//which are in a terrain leaf that doesn't intersect the pick ray
		tree.traverse(new QuadtreeNodeTraversal<TerrainQuadtree>(){
			@Override
			public void handleQuadtreeNode(TerrainQuadtree node){
				
				//associate the terrain leaf "node" with the tree of subtiles
				subtileTree.setActiveLeaf(node);
				
				subtileTree.traverse(new QuadtreeNodeTraversal<TerrainLeafQuadtree>(){
					@Override
					public void handleQuadtreeNode(TerrainLeafQuadtree subtile){
						
						//get subtile
						Vector2d subtileTerrainLerpCenter = subtile.getTerrainLerpCenter();
						int subtileX = (int)(subtileTerrainLerpCenter.getX() * (float)subtileCount);
						int subtileY = (int)(subtileTerrainLerpCenter.getY() * (float)subtileCount);
						
						//get collider for current subtile
						readHeightData(gl, subtileX, subtileY, 1, 1, texHeightmapSample);
						Vector3d[][] vertexGrid = readLocalColliderVertexGrid(
								subtileX, subtileY, 1, 1, texHeightmapSample);
						ColliderGroup collider = getTrianglesFromVertexGrid(1, 1, vertexGrid, pickListener);

						//check for pick, and if there is one, shorten the culler and stop "rasterizing"
						boolean result = collider.pick(pickData, p, normalizedRay, pickData.getPickDistance(), rayCuller);
						if(result){ 
							Vector3d pickPos = pickData.getPickPosition();
							Vector2d pickPos2d = new Vector2d(pickPos.getX(), -pickPos.getZ());
							
							rayCuller.setFinalPoint(pickPos);
							rayCuller2d.setFinalPoint(pickPos2d);
							return;
						}
					}
				}, true, childOrder, rayCuller2d);
			}
		}, true, childOrder, rayCuller);
		
		return false;
	}
	
	
	//=========================================================
	// METHODS TO PULL HIGHEST-LOD TRIANGLES
	//=========================================================
	
	/**
	 * Read the portion of the height texture covered by the specified subtiles
	 * into the specified BufferTexture.
	 */
	private void readHeightData(GL gl, int subtileMinX, int subtileMinY, int subtileCountX, int subtileCountY, BufferTexture texHeightmapSample){
		
		//get pixel bounds and read heightmap texture
		int pixelMinX = terrainPixelMinX + (int)(subtileMinX*subtilePixelWidth);
		int pixelMinY = terrainPixelMinY + (int)(subtileMinY*subtilePixelHeight);
		
		int pixelWidth = 1 + terrainPixelMinX + (int)(subtileCountX*subtilePixelWidth);
		int pixelHeight = 1 + terrainPixelMinY + (int)(subtileCountY*subtilePixelHeight);
		
		dataHeightAndAux.getAsBufferTexture(gl, texHeightmapSample, pixelMinX, pixelMinY, pixelWidth, pixelHeight);
	}
	
	/**
	 * Read from texHeightmapSample a grid of vertices which are necessary
	 * to construct a local collider encompassing the specified subtiles.
	 */
	private Vector3d[][] readLocalColliderVertexGrid(int subtileMinX, int subtileMinY, int subtileCountX, int subtileCountY, BufferTexture texHeightmapSample){

		//read grid of 3d positions
		int vertCountX = 2*subtileCountX + 1;
		int vertCountY = 2*subtileCountY + 1;
		Vector3d[][] verts = new Vector3d[vertCountX][vertCountY];
		
		Vector2d vertLerp = new Vector2d(0.0f, 0.0f);
		for(int i=0; i<vertCountX; i++){ 
		for(int j=0; j<vertCountY; j++){ 
			
			//terrain lerp position of vertex
			vertLerp.setX((float)subtileMinX + 0.5f*(float)i); 
			vertLerp.setY((float)subtileMinY + 0.5f*(float)j);
			vertLerp.scale(1.0f / (float)subtileCount);	
			
			//2d position
			Vector2d vertPos2d = terrainMetrics.getTerrainPosBounds().lerp(vertLerp);

			//pixel offset of vertex
			int pixelOffsetX = (int)(subtilePixelWidth * 0.5f*(float)i);
			int pixelOffsetY = (int)(subtilePixelHeight * 0.5f*(float)j);
			
			//read height from heightmap
			int r = texHeightmapSample.getR(pixelOffsetX, pixelOffsetY);
			int g = texHeightmapSample.getG(pixelOffsetX, pixelOffsetY);
			float h = terrainMetrics.getHeightScale() * ((float)r + (float)g/255.0f);

			//record 3d position
			verts[i][j] = new Vector3d(vertPos2d.getX(), h, -vertPos2d.getY());
		}}
		
		return verts;
	}
	
	/**
	 * Construct the triangles forming each subtile using a vertex grid
	 * output from readLocalColliderVertexGrid.
	 */
	private static ColliderGroup getTrianglesFromVertexGrid(
		int tileCountX, int tileCountY, Vector3d[][] vertexGrid,
		PickListener<Vector3d> pickListener){
		
		ColliderGroup colliderTiles = new ColliderGroup(pickListener);
		
		for(int i=0; i<tileCountX; i++){
		for(int j=0; j<tileCountY; j++){
			//center vertex for current tile
			int cx = 1 + 2*i, cy = 1 + 2*j;
			
			//v[0] = center vertex, v[1]-v[8] are positioned in CCW order around center
			Vector3d[] v = new Vector3d[]{
				vertexGrid[cx][cy],
				vertexGrid[cx+1][cy], vertexGrid[cx+1][cy+1], vertexGrid[cx][cy+1], vertexGrid[cx-1][cy+1],
				vertexGrid[cx-1][cy], vertexGrid[cx-1][cy-1], vertexGrid[cx][cy-1], vertexGrid[cx+1][cy-1]
			};
			
			//specify triangles
			colliderTiles.addTriangle(v[0], v[1], v[2]);
			colliderTiles.addTriangle(v[0], v[2], v[3]);
			colliderTiles.addTriangle(v[0], v[3], v[4]);
			colliderTiles.addTriangle(v[0], v[4], v[5]);
			colliderTiles.addTriangle(v[0], v[5], v[6]);
			colliderTiles.addTriangle(v[0], v[6], v[7]);
			colliderTiles.addTriangle(v[0], v[7], v[8]);
			colliderTiles.addTriangle(v[0], v[8], v[1]);
		}}
		
		return colliderTiles;
	}
	
	/**
	 * Given a terrain position (i.e., terrain y <-> ambient -z), return the correpsonding
	 * fractional subtile position.  See Terrain comments for an explanation of subtiles.
	 */
	private Vector2d getSubtilePosFromTerrainPos(Vector2d pos){
		
		Vector2d lerpPos = terrainMetrics.getTerrainPosBounds().getLerp(pos);
		lerpPos.scale((float)subtileCount);
		return lerpPos;
	}
	
	/**
	 * Construct a TriangleBox Collider which aligns with the triangles constructed
	 * by the Terrain and its vertex shader at the highest LOD, and which covers a region 
	 * of the terrain encompassing the bounds specified by requestedPosBounds.
	 * 
	 * @param requestedPosBounds The requested region of the terrain that the TriangleBox
	 * should encompass.  This is specified in "terrain coordinates" 
	 * (i.e., terrain y <-> ambient -z).
	 * @param texHeightmapSample A temporary BufferTexture into which the heightmap texture is
	 * read, for the purpose of reading back height values to create the triangles.
	 * @return The TriangleBox collider.
	 */
	public ColliderGroup getTriangles(GL gl, 
		Box<Vector2d> requestedPosBounds, BufferTexture texHeightmapSample){
		
		//clamp to terrain pos bounds
		terrainMetrics.getTerrainPosBounds().clamp(requestedPosBounds);
		
		//get fractional subtile index (i.e., min = 0, max = subtileCount-1) range
		Box<Vector2d> lcTileBounds = new Box<Vector2d>(
			getSubtilePosFromTerrainPos(requestedPosBounds.getMin()),
			getSubtilePosFromTerrainPos(requestedPosBounds.getMax())
		);
		
		//get rounded and clamped tile bounds (inclusive bounds)
		int subtileMinX = (int)lcTileBounds.getMin().getX(), subtileMaxX = (int)lcTileBounds.getMax().getX();
		int subtileMinY = (int)lcTileBounds.getMin().getY(), subtileMaxY = (int)lcTileBounds.getMax().getY();		
		
		if(subtileMinX < 0){ subtileMinX = 0; } if(subtileMaxX >= subtileCount){ subtileMaxX = subtileCount-1; }
		if(subtileMinY < 0){ subtileMinY = 0; } if(subtileMaxY >= subtileCount){ subtileMaxY = subtileCount-1; }
		
		int subtileCountX = 1 + subtileMaxX - subtileMinX;
		int subtileCountY = 1 + subtileMaxY - subtileMinY;
		
		//read heightmap texture 
		readHeightData(gl, subtileMinX, subtileMinY, subtileCountX, subtileCountY, texHeightmapSample);
		
		//read vertex grid from texture
		Vector3d[][] vertexGrid = readLocalColliderVertexGrid(subtileMinX, subtileMinY, subtileCountX, subtileCountY, texHeightmapSample);
		
		//assemble triangles from vertex grid
		return getTrianglesFromVertexGrid(subtileCountX, subtileCountY, vertexGrid, pickListener);
	}
}
