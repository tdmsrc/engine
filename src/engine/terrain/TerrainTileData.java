package engine.terrain;

import java.util.ArrayList;

import engine.renderer.DrawableIBO;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericBuffer.BufferConstructor;


public class TerrainTileData<GL, Buffer extends GenericBuffer<GL>> {

	private int maxLOD;
	
	//terrain pieces at various LODs
	private DrawableIBO<GL,Buffer> tileLevel0;
	private ArrayList<ArrayList<DrawableIBO<GL,Buffer>>> tileLevel;
	
	private static final int 
		MASK_COARSE_XMAX = 1 << 0,
		MASK_COARSE_XMIN = 1 << 1,
		MASK_COARSE_YMAX = 1 << 2,
		MASK_COARSE_YMIN = 1 << 3;
	
	private static int getIndexFromFlags(boolean coarse_XMAX, boolean coarse_XMIN, boolean coarse_YMAX, boolean coarse_YMIN){
		int i = 0;
		if(coarse_XMAX){ i |= MASK_COARSE_XMAX; }
		if(coarse_XMIN){ i |= MASK_COARSE_XMIN; }
		if(coarse_YMAX){ i |= MASK_COARSE_YMAX; }
		if(coarse_YMIN){ i |= MASK_COARSE_YMIN; }
		return i;
	}
	
	public TerrainTileData(GL gl, int maxLOD, boolean constructWireframeData,
		BufferConstructor<GL,Buffer> bufferConstructor){
		
		this.maxLOD = maxLOD;
		
		//construct terrain LOD pieces
		tileLevel0 = TerrainTileFactory.getTileIBOLevel0(gl, maxLOD, constructWireframeData, bufferConstructor);
		tileLevel = new ArrayList<ArrayList<DrawableIBO<GL,Buffer>>>(maxLOD);
		
		for(int lod=1; lod<=maxLOD; lod++){
			ArrayList<DrawableIBO<GL,Buffer>> tileLevelData = new ArrayList<DrawableIBO<GL,Buffer>>(16);
			for(int i=0; i<16; i++){
				tileLevelData.add(TerrainTileFactory.getTileIBO(gl, maxLOD, lod, constructWireframeData,
					(i & MASK_COARSE_XMAX) == MASK_COARSE_XMAX, 
					(i & MASK_COARSE_XMIN) == MASK_COARSE_XMIN, 
					(i & MASK_COARSE_YMAX) == MASK_COARSE_YMAX, 
					(i & MASK_COARSE_YMIN) == MASK_COARSE_YMIN,
					bufferConstructor));
			}
			tileLevel.add(tileLevelData);
		}
	}
	
	public DrawableIBO<GL,Buffer> getIBOLevel0(){
		return tileLevel0;
	}
	
	public DrawableIBO<GL,Buffer> getIBO(int lod,
		boolean coarse_XMAX, boolean coarse_XMIN, boolean coarse_YMAX, boolean coarse_YMIN){

		if(lod > maxLOD){ lod = maxLOD; }
		
		return tileLevel.get(lod-1).get(getIndexFromFlags(coarse_XMAX, coarse_XMIN, coarse_YMAX, coarse_YMIN));
	}
}
