package engine.terrain;

import engine.renderer.GenericTexture;
import engine.renderer.Material;


public class TerrainMaterial<Texture extends GenericTexture<?>>{

	private boolean useMaterial1, useMaterial2, useMaterial3;
	private Material<Texture> material1, material2, material3;
	
	
	public TerrainMaterial(Material<Texture> material1, Material<Texture> material2, Material<Texture> material3){
		super();
		
		useMaterial1 = true;
		this.material1 = material1;
		
		useMaterial2 = true;
		this.material2 = material2;
		
		useMaterial3 = true;
		this.material3 = material3;
	}

	public boolean checkUseMaterial1(){ return useMaterial1; }
	public boolean checkUseMaterial2(){ return useMaterial2; }
	public boolean checkUseMaterial3(){ return useMaterial3; }
	
	//material
	public Material<Texture> getMaterial1(){ return material1; }
	public Material<Texture> getMaterial2(){ return material2; }
	public Material<Texture> getMaterial3(){ return material3; }
}
