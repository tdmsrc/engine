package engine.terrain;

import geometry.math.Vector2d;
import geometry.spacepartition.Box;

public class TerrainMetrics {

	private float heightScale;
	private Box<Vector2d> terrainPos;
	private Box<Vector2d> terrainDataTex;
	private Box<Vector2d> terrainColorTex;
	
	
	public TerrainMetrics(float heightScale, 
		Box<Vector2d> terrainPos, 
		Box<Vector2d> terrainDataTex, 
		Box<Vector2d> terrainColorTex){
		
		this.heightScale = heightScale;
		this.terrainPos = terrainPos;
		this.terrainDataTex = terrainDataTex;
		this.terrainColorTex = terrainColorTex;
	}
	
	public float getHeightScale(){ return heightScale; }
	
	public Box<Vector2d> getTerrainPosBounds(){ return terrainPos; }
	public Box<Vector2d> getTerrainDataTexBounds(){ return terrainDataTex; }
	public Box<Vector2d> getTerrainColorTexBounds(){ return terrainColorTex; }
}
