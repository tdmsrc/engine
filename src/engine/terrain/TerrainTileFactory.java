package engine.terrain;

import engine.renderer.DrawableIBO;
import engine.renderer.GenericBuffer;
import engine.renderer.GenericBuffer.BufferConstructor;

//[TODO]
//describe this class


class TerrainTileFactory{
	
	private static int putPoint(float[] fArray, int k, float x, float y){
		fArray[k  ] = x;
		fArray[k+1] = y;
		return 2;
	}
	
	private static int putEdge(int[] iArrayWireframe, int kw, int p, int q){
		iArrayWireframe[kw  ] = p;
		iArrayWireframe[kw+1] = q;
		return 2;
	}
	
	private static int putTri(int[] iArray, int k, int p, int q, int r){
		iArray[k  ] = p;
		iArray[k+1] = q;
		iArray[k+2] = r;
		return 3;
	}
	
	private static int putEdge(int[] iArrayWireframe, int kw, int maxLOD, int curLOD,
		int px, int py, int qx, int qy){
				
		return putEdge(iArrayWireframe, kw, 
			getIndex(px,py,maxLOD,curLOD), 
			getIndex(qx,qy,maxLOD,curLOD));
	}
	
	private static int putTri(int[] iArray, int k, int maxLOD, int curLOD,
		int px, int py, int qx, int qy, int rx, int ry){
		
		return putTri(iArray, k, 
			getIndex(px,py,maxLOD,curLOD), 
			getIndex(qx,qy,maxLOD,curLOD), 
			getIndex(rx,ry,maxLOD,curLOD));
	}
	
	private static int getIndex(int x, int y, int maxLOD, int curLOD){
		
		//the curLOD grid fits onto the maxLOD grid with matching vertices.
		//given (x,y) \in [0,(curLOD grid size)+1]^2, figure out the corresponding
		//index of the point in the list of points given by getVBO(maxLOD)
		
		int maxLODGridSize = 1 << (maxLOD + 1);
		int step = 1 << (maxLOD - curLOD);
		return (x*step) + (maxLODGridSize+1)*(y*step);
	}
	
	public static <GL, Buffer extends GenericBuffer<GL>> 
		Buffer getDataPositionBuffer(GL gl, int maxLOD,
			BufferConstructor<GL,Buffer> bufferConstructor){
		
		//split [0,1]^2 into a gridSize x gridSize grid, where gridSize = 2^(maxLOD+1)
		//index in such a way that the (x+(gridSize+1)*y)th point is (x*step, y*step). 
		
		int gridSize = 1 << (maxLOD + 1);
		float step = 1.0f / (float)gridSize;
		float[] fArray = new float[2*(gridSize+1)*(gridSize+1)]; 
		
		int k = 0;
		for(int j=0; j<=gridSize; j++){
		for(int i=0; i<=gridSize; i++){
			k += putPoint(fArray, k, i*step, j*step);
		}}

		return bufferConstructor.construct(gl, fArray, 2);
	}
	
	public static <GL, Buffer extends GenericBuffer<GL>> 
		DrawableIBO<GL,Buffer> getTileIBOLevel0(GL gl, int maxLOD, boolean constructWireframeData,
			BufferConstructor<GL,Buffer> bufferConstructor){
		
		int triCount = 8, edgeCount = 16;
		
		int[] iArray = new int[3*triCount];
		int[] iArrayWireframe = (constructWireframeData ? new int[2*edgeCount] : new int[0]);
		int k=0, kw=0;
		
		k += putTri(iArray,k, maxLOD,0, 0,0, 1,0, 1,1);
		k += putTri(iArray,k, maxLOD,0, 1,0, 2,0, 1,1);
		k += putTri(iArray,k, maxLOD,0, 2,0, 2,1, 1,1);
		k += putTri(iArray,k, maxLOD,0, 2,1, 2,2, 1,1);
		k += putTri(iArray,k, maxLOD,0, 2,2, 1,2, 1,1);
		k += putTri(iArray,k, maxLOD,0, 1,2, 0,2, 1,1);
		k += putTri(iArray,k, maxLOD,0, 0,2, 0,1, 1,1);
		k += putTri(iArray,k, maxLOD,0, 0,1, 0,0, 1,1);
		
		if(constructWireframeData){
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 0,0, 1,0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,0, 2,0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 2,0, 2,1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 2,1, 2,2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 2,2, 1,2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,2, 0,2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 0,2, 0,1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 0,1, 0,0);
			
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 0,0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 1,0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 2,0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 2,1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 2,2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 1,2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 0,2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,0, 1,1, 0,1);
		}

		return (constructWireframeData ?
			new DrawableIBO<GL,Buffer>(gl, iArray, triCount, iArrayWireframe, edgeCount, bufferConstructor) :
			new DrawableIBO<GL,Buffer>(gl, iArray, triCount, bufferConstructor));
	}
	
	public static <GL, Buffer extends GenericBuffer<GL>> 
		DrawableIBO<GL,Buffer> getTileIBO(GL gl, 
			int maxLOD, int curLOD, boolean constructWireframeData,
			boolean coarse_XMAX, boolean coarse_XMIN, boolean coarse_YMAX, boolean coarse_YMIN,
			BufferConstructor<GL,Buffer> bufferConstructor){
		
		int triCount = 0, edgeCount = 0;
		
		//grid of center boxes 2^(lod)-1 each side
		int centerBoxCount = (1 << curLOD) - 1;
		//per center box: 8 triangles, 16 edges
		triCount += centerBoxCount*centerBoxCount * 8;
		edgeCount += centerBoxCount*centerBoxCount * 16;
		//centerBoxCount outward-pointing flanges, centerBoxCount+1 inward-pointing flanges
		//out flanges: 2 triangles, 3 edges; in flanges: (coarse?1:2) triangles, (coarse?1:3) edges
		triCount += centerBoxCount*2*4 + (centerBoxCount+1)*
			((coarse_XMAX ? 1:2) + (coarse_XMIN ? 1:2) + (coarse_YMAX ? 1:2) + (coarse_YMIN ? 1:2));
		edgeCount += centerBoxCount*3*4 + (centerBoxCount+1)*
			((coarse_XMAX ? 1:3) + (coarse_XMIN ? 1:3) + (coarse_YMAX ? 1:3) + (coarse_YMIN ? 1:3));
		
		//initialize array
		int[] iArray = new int[3*triCount];
		int[] iArrayWireframe = (constructWireframeData ? new int[2*edgeCount] : new int[0]);
		int k=0, kw=0;
		
		//make center
		for(int j=0; j<centerBoxCount; j++){
		for(int i=0; i<centerBoxCount; i++){
			int ox = 1 + 2*i, oy = 1 + 2*j;
			
			k += putTri(iArray,k, maxLOD,curLOD, ox+0,oy+0, ox+1,oy+0, ox+1,oy+1);
			k += putTri(iArray,k, maxLOD,curLOD, ox+1,oy+0, ox+2,oy+0, ox+1,oy+1);
			k += putTri(iArray,k, maxLOD,curLOD, ox+2,oy+0, ox+2,oy+1, ox+1,oy+1);
			k += putTri(iArray,k, maxLOD,curLOD, ox+2,oy+1, ox+2,oy+2, ox+1,oy+1);
			k += putTri(iArray,k, maxLOD,curLOD, ox+2,oy+2, ox+1,oy+2, ox+1,oy+1);
			k += putTri(iArray,k, maxLOD,curLOD, ox+1,oy+2, ox+0,oy+2, ox+1,oy+1);
			k += putTri(iArray,k, maxLOD,curLOD, ox+0,oy+2, ox+0,oy+1, ox+1,oy+1);
			k += putTri(iArray,k, maxLOD,curLOD, ox+0,oy+1, ox+0,oy+0, ox+1,oy+1);
			
			if(!constructWireframeData){ continue; }
			
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+0,oy+0, ox+1,oy+0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+0, ox+2,oy+0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+2,oy+0, ox+2,oy+1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+2,oy+1, ox+2,oy+2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+2,oy+2, ox+1,oy+2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+2, ox+0,oy+2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+0,oy+2, ox+0,oy+1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+0,oy+1, ox+0,oy+0);
			
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+0,oy+0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+1,oy+0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+2,oy+0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+2,oy+1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+2,oy+2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+1,oy+2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+0,oy+2);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, ox+1,oy+1, ox+0,oy+1);
		}}
		
		//make flanges
		int gridSize = 1 << (curLOD + 1);

		//outward-pointing flanges
		for(int i=0; i<centerBoxCount; i++){
			int off = 1 + 2*i, in, out;
			
			//XMIN
			out = 0; in = out+1;
			k += putTri(iArray,k, maxLOD,curLOD, in ,off+2, out,off+1, in,off+1);
			k += putTri(iArray,k, maxLOD,curLOD, out,off+1, in ,off+0, in,off+1);
			//XMAX
			out = gridSize; in = out-1;
			k += putTri(iArray,k, maxLOD,curLOD, in ,off+0, out,off+1, in,off+1);
			k += putTri(iArray,k, maxLOD,curLOD, out,off+1, in ,off+2, in,off+1);
			//YMIN
			out = 0; in = out+1;
			k += putTri(iArray,k, maxLOD,curLOD, off+0,in , off+1,out, off+1,in);
			k += putTri(iArray,k, maxLOD,curLOD, off+1,out, off+2,in , off+1,in);
			//YMAX
			out = gridSize; in = out-1;
			k += putTri(iArray,k, maxLOD,curLOD, off+2,in , off+1,out, off+1,in);
			k += putTri(iArray,k, maxLOD,curLOD, off+1,out, off+0,in , off+1,in);
			
			if(!constructWireframeData){ continue; }
			
			//XMIN
			out = 0; in = out+1;
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in, off+0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in, off+1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in, off+2);
			
			//XMAX
			out = gridSize; in = out-1;
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in, off+0);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in, off+1);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in, off+2);
			
			//YMIN
			out = 0; in = out+1;
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+0,in);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+1,in);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+2,in);
				
			//YMAX
			out = gridSize; in = out-1;
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+0,in);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+1,in);
			kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+2,in);
		}

		//inward-pointing flanges
		for(int i=0; i<=centerBoxCount; i++){
			int off = 2*i, in, out;
			
			//XMIN
			out = 0; in = out+1;
			if(coarse_XMIN){
				k += putTri(iArray,k, maxLOD,curLOD, out,off+2, out,off+0, in,off+1);
			}else{
				k += putTri(iArray,k, maxLOD,curLOD, out,off+0, in ,off+1, out,off+1);
				k += putTri(iArray,k, maxLOD,curLOD, in ,off+1, out,off+2, out,off+1);
			}
			//XMAX
			out = gridSize; in = out-1;
			if(coarse_XMAX){
				k += putTri(iArray,k, maxLOD,curLOD, in,off+1, out,off+0, out,off+2);
			}else{
				k += putTri(iArray,k, maxLOD,curLOD, out,off+2, in ,off+1, out,off+1);
				k += putTri(iArray,k, maxLOD,curLOD, in ,off+1, out,off+0, out,off+1);
			}
			//YMIN
			out = 0; in = out+1;
			if(coarse_YMIN){
				k += putTri(iArray,k, maxLOD,curLOD, off+0,out, off+2,out, off+1,in);
			}else{
				k += putTri(iArray,k, maxLOD,curLOD, off+2,out, off+1,in , off+1,out);
				k += putTri(iArray,k, maxLOD,curLOD, off+1,in , off+0,out, off+1,out);
			}
			//YMAX
			out = gridSize; in = out-1;
			if(coarse_YMAX){
				k += putTri(iArray,k, maxLOD,curLOD, off+0,out, off+1,in, off+2,out);
			}else{
				k += putTri(iArray,k, maxLOD,curLOD, off+0,out, off+1,in , off+1,out);
				k += putTri(iArray,k, maxLOD,curLOD, off+1,in , off+2,out, off+1,out);
			}
			
			if(!constructWireframeData){ continue; }

			//XMIN
			out = 0; in = out+1;
			if(coarse_XMIN){
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+0, out,off+2);
			}else{
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+0, out,off+1);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, out,off+2);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in ,off+1);
			}
			
			//XMAX
			out = gridSize; in = out-1;
			if(coarse_XMAX){
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+0, out,off+2);
			}else{
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+0, out,off+1);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, out,off+2);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, out,off+1, in ,off+1);
			}
				
			//YMIN
			out = 0; in = out+1;
			if(coarse_YMIN){
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+0,out, off+2,out);
			}else{
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+0,out, off+1,out);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+2,out);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+1,in );
			}
				
			//YMAX
			out = gridSize; in = out-1;
			if(coarse_YMAX){
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+0,out, off+2,out);
			}else{
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+0,out, off+1,out);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+2,out);
				kw += putEdge(iArrayWireframe,kw, maxLOD,curLOD, off+1,out, off+1,in );
			}
		}
		
		return (constructWireframeData ?
			new DrawableIBO<GL,Buffer>(gl, iArray, triCount, iArrayWireframe, edgeCount, bufferConstructor) :
			new DrawableIBO<GL,Buffer>(gl, iArray, triCount, bufferConstructor));
	}
}