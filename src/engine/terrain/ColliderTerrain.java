package engine.terrain;

import engine.collision.Collider;
import engine.collision.ColliderGroup;
import engine.collision.CollisionData;
import engine.collision.Ellipsoid;
import engine.renderer.GenericTexture;
import engine.toolsAWT.BufferTexture;
import geometry.common.MessageOutput;
import geometry.math.FixedDimensionalVector;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;
import geometry.spacepartition.QuadtreeChild;
import geometry.spacepartition.QuadtreeNodeTraversal;


public class ColliderTerrain<GL>{
	
	private static final float MIN_LOCAL_COLLIDER_SIZE_X = 20.0f;
	private static final float MIN_LOCAL_COLLIDER_SIZE_Z = 20.0f;
	
	private Terrain<GL,?,? extends GenericTexture<GL>> terrain;

	private BufferTexture texHeightmapSample;
	
	private boolean hasLocalCollider;
	private Collider<Vector3d> localCollider;
	private Box<Vector2d> localColliderBounds; //<- in terrain units; (terrain Y <=> ambient -Z)
	
	
	public ColliderTerrain(Terrain<GL,?,? extends GenericTexture<GL>> terrain){
		this.terrain = terrain;

		//allocate BufferTexture
		texHeightmapSample = new BufferTexture(1);
		
		//invalidate current collider
		hasLocalCollider = false;
		localCollider = null;
		localColliderBounds = null;
	}

	/**
	 * Does the same thing as Collider.collide, but there is no need to handle
	 * the case with objTransformation, and no need for picking either.
	 * 
	 * @see {@link Collider#collide(CollisionData, Ellipsoid, FixedDimensionalVector, Box)}
	 */
	public boolean collide(CollisionData<Vector3d> collisionData,
		Ellipsoid<Vector3d> ce, Vector3d movement, Vector3d movementr, float minimumMovementMultiple, Box<Vector3d> movingEllipsoidBounds){
		
		//use localCollider (if there is one) to get collision
		if(!hasLocalCollider){ return false; }
		return localCollider.collide(collisionData, ce, movement, movementr, minimumMovementMultiple, movingEllipsoidBounds);
	}
	
	/**
	 * Update the Collider, if necessary, so that it can be used with the 
	 * CollisionEllipsoid with the specified movingEllipsoidBounds.
	 */
	public void update(GL gl, Box<Vector3d> movingEllipsoidBounds){
	
		if(!checkLocalColliderIsValid(movingEllipsoidBounds)){
			updateLocalCollider(gl, movingEllipsoidBounds);
		}
	}
	
	/**
	 * Check if it is necessary to update the Collider.
	 * It is not necessary if the current localColliderBounds encompass
	 * the projection of movingEllipsoidBounds onto the XZ plane, or if
	 * movingEllipsoidBounds intersects no TerrainQuadtree node bounding box.
	 */
	protected boolean checkLocalColliderIsValid(Box<Vector3d> movingEllipsoidBounds){
		
		if(!hasLocalCollider){ return false; }
		
		//check if ellipsoid movement is encompassed by localColliderBounds
		if(	(movingEllipsoidBounds.getMin().getX() > localColliderBounds.getMin().getX()) &&
			(movingEllipsoidBounds.getMax().getX() < localColliderBounds.getMax().getX()) && 
			(movingEllipsoidBounds.getMin().getZ() > -localColliderBounds.getMax().getY()) && 
			(movingEllipsoidBounds.getMax().getZ() < -localColliderBounds.getMin().getY()) ){

			return true; 
		}
		
		//if not, use quadtree traversal to check if movingEllipsoidBounds intersects
		//any terrain quadtree leaf bounding box; if not, nothing to check at all
		int n = terrain.getQuadtree().traverse(new QuadtreeNodeTraversal<TerrainQuadtree>(){
			@Override
			public void handleQuadtreeNode(TerrainQuadtree node){ }
		}, true, QuadtreeChild.values(), movingEllipsoidBounds);
		
		if(n == 0){ return true; }
		
		//if neither condition above is satisfied, need to update local collider
		return false;
	}
	
	
	//==========================================
	// GENERATE TRIANGLE BOX FROM HEIGHTMAP
	//==========================================
	
	/**
	 * Returns 2d rect, in Terrain coordinates (i.e., terrain y <-> ambient -z),
	 * for the region the local collider should cover, given the condition that it 
	 * should encompass movingEllipsoidBounds' projection onto the XZ plane,
	 * and meet MIN_LOCAL_COLLIDER_SIZE requirements.
	 */
	private static Box<Vector2d> getRequestedLocalColliderBounds(Box<Vector3d> movingEllipsoidBounds){
		
		//project movingEllipsoidBounds onto XZ plane and expand so it meets minLC requirements
		Vector3d dims = movingEllipsoidBounds.getDimensions();
		float expandX = (MIN_LOCAL_COLLIDER_SIZE_X - dims.getX()) / 2.0f;
		float expandZ = (MIN_LOCAL_COLLIDER_SIZE_Z - dims.getZ()) / 2.0f;
		expandX = (expandX > 0) ? expandX : 0;
		expandZ = (expandZ > 0) ? expandZ : 0;
		
		//now get 2d position bounds (i.e., using terrain XY coordinates)
		//since -z axis maps to texture +y axis, XY pos min is (x min, max z), etc		
		return new Box<Vector2d>(
			new Vector2d(movingEllipsoidBounds.getMin().getX() - expandX, -movingEllipsoidBounds.getMax().getZ() - expandZ),
			new Vector2d(movingEllipsoidBounds.getMax().getX() + expandX, -movingEllipsoidBounds.getMin().getZ() + expandZ));
	}

	
	/**
	 * Construct a TriangleBox Collider which aligns with the triangles constructed
	 * by the Terrain and its vertex shader at the highest LOD, and which covers a region 
	 * of the terrain encompassing the projection of movingEllipsoidBounds onto the XZ
	 * plane and which meets the MIN_LOCAL_COLLIDER_SIZE requirements.
	 */
	private void updateLocalCollider(GL gl, Box<Vector3d> movingEllipsoidBounds){

		//get requested local collider bounds
		Box<Vector2d> lcPosBounds = getRequestedLocalColliderBounds(movingEllipsoidBounds);
		
		//get collider
		ColliderGroup collider = terrain.getTriangles(gl, lcPosBounds, texHeightmapSample);
		
		localCollider = collider;
		localColliderBounds = lcPosBounds;
		hasLocalCollider = true;
		
		MessageOutput.printDebug("Terrain collider update: " +  collider.getColliderCount() + " triangles");
	}
}
