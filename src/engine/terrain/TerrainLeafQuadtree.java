package engine.terrain;

import java.util.EnumMap;

import geometry.math.Vector2d;
import geometry.spacepartition.Box;
import geometry.spacepartition.QuadtreeChild;
import geometry.spacepartition.QuadtreeNodeCullable;

//arrange the subtiles of a terrain leaf into a quadtree
//for the purpose of picking

//depth of this tree = terrain's max LOD

//there is some data that can be set before traversing that indicates
//which leaf, how deep in the tree the leaf is, and the max LOD of terrain

class TerrainLeafQuadtree extends QuadtreeNodeCullable<TerrainLeafQuadtree, Vector2d>{

	//root info
	private Box<Vector2d> terrainPosBounds;
	private TerrainQuadtree activeLeaf;
	
	//node info
	private Box<Vector2d> subLeafLerp; //lerp bounds for this sub-leaf node, with [0,1]^2 indicating the whole leaf
	
	//parent
	private TerrainLeafQuadtree root;
	private TerrainLeafQuadtree parent;
	private QuadtreeChild childType;
	
	//children
	private boolean isLeaf;
	private EnumMap<QuadtreeChild,TerrainLeafQuadtree> children;
	
	
	//==========================================
	// CONSTRUCTOR
	//==========================================
	
	public TerrainLeafQuadtree(Terrain<?,?,?> terrain, int maxLOD){
		this(maxLOD, null, null, new Box<Vector2d>(new Vector2d(0,0), new Vector2d(1,1)));
		
		terrainPosBounds = terrain.getMetrics().getTerrainPosBounds();
		setRoot(this);
	}
	
	private TerrainLeafQuadtree(
		int depth, TerrainLeafQuadtree parent, QuadtreeChild childType, Box<Vector2d> subLeafLerp){
		
		this.parent = parent;
		this.childType = childType;
		
		this.subLeafLerp = subLeafLerp;
		
		//make children, if necessary
		isLeaf = (depth == 0);
		
		if(depth > 0){
			children = new EnumMap<QuadtreeChild,TerrainLeafQuadtree>(QuadtreeChild.class);
			for(QuadtreeChild child : QuadtreeChild.values()){
				children.put(child,
					new TerrainLeafQuadtree(depth-1, this, child, child.getSubBox(subLeafLerp))
				);
			}
		}
	}
	
	private void setRoot(TerrainLeafQuadtree root){
		this.root = root;
		
		if(isLeaf){ return; }
		for(QuadtreeChild child : QuadtreeChild.values()){
			children.get(child).setRoot(root);
		}
	}
	
	public void setActiveLeaf(TerrainQuadtree activeLeaf){
		
		root.activeLeaf = activeLeaf;
	}
	
	//==========================================
	// BASIC METHODS, QUADTREE NODE INTERFACE
	//==========================================
	
	@Override
	protected TerrainLeafQuadtree getThisNode(){ return this; }
	
	@Override
	protected TerrainLeafQuadtree getParent(){ return parent; }
	
	@Override
	protected QuadtreeChild getChildType(){ return childType; }
	
	@Override
	protected boolean isLeaf(){ return isLeaf; }

	@Override
	protected TerrainLeafQuadtree getChild(QuadtreeChild child){ return children.get(child); }
	
	@Override
	protected Box<Vector2d> getCullBounds(){
		
		Vector2d terrainLerpMin = root.activeLeaf.getTileLerp().lerp(subLeafLerp.getMin());
		Vector2d terrainLerpMax = root.activeLeaf.getTileLerp().lerp(subLeafLerp.getMax());
		
		Vector2d terrainPosMin = root.terrainPosBounds.lerp(terrainLerpMin);
		Vector2d terrainPosMax = root.terrainPosBounds.lerp(terrainLerpMax);
		
		return new Box<Vector2d>(terrainPosMin, terrainPosMax);
	}
	
	public Vector2d getTerrainLerpCenter(){
		
		Vector2d subLeafLerpCenter = subLeafLerp.getCenter();
		return root.activeLeaf.getTileLerp().lerp(subLeafLerpCenter);
	}
}
